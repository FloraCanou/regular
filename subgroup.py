import math
import matfunc
import regutils, parametric, te

class TemperamentClass(parametric.TemperamentClass):
    def __init__(self, translation, plimit, melody):
        """
        translation -- Subgroup matrix defined on prime intervals.
        plimit -- Sizes of prime intervals in octaves.
        melody -- The mapping in terms of the subgroup.
        """
        for vector in translation:
            assert len(vector) == len(plimit)
        for mapping in melody:
            assert len(mapping) == len(translation)
        self.translation = translation
        [self.plimit] = parametric.transmult(translation, [plimit])
        self.melody = melody
        self.metric = makeComplexityMetric(translation, plimit)

    def complexity(self):
        """Subgroup TE complexity"""
        M = self.melody
        G = self.metric
        H = self.plimit
        A = parametric.transmult(parametric.transmult(G, M), M)
        B = parametric.bracket(H, G, H)
        if len(self.melody) == 1:
            # Determinants of a 1 x 1 matrix fail
            squarething = A[0][0]
        else:
            squarething = matfunc.Square(A).det()
        return math.sqrt(abs(squarething) / B ** len(self.melody))

    def optimalBadness(self, ek=0.0):
        """Cangwu badness"""
        # Using equation 17 of badness.pdf
        epsilon = ek / math.sqrt(1 + ek**2)
        M = self.melody
        G = self.metric
        H = self.plimit
        [A] = parametric.transmult(parametric.transmult(G, M), [H])
        B = parametric.bracket(H, G, H)
        multiplier = (1 - epsilon) / B
        C = [[x*y*multiplier for x in H] for y in A]
        Mp = [[x-y for (x, y) in zip(Mrow, Crow)]
                for (Mrow, Crow) in zip(M, C)]
        Ap = parametric.transmult(parametric.transmult(G, Mp), Mp)
        if len(self.melody) == 1:
            # Determinants of a 1 x 1 matrix fail
            squarething = Ap[0][0]
        else:
            squarething = matfunc.Square(Ap).det()
        return math.sqrt(abs(squarething) / B ** len(self.melody))

class RegularTemperament(TemperamentClass, te.TuningMixin):
    def optimize(self):
        # Equation 28 from primerr.pdf
        M = self.melody
        G = self.metric
        H = self.plimit
        MTM = parametric.transmult(parametric.transmult(G, M), M)
        [MTV] = parametric.transmult(parametric.transmult(G, M), [H])
        if len(self.melody) == 1:
            self.basis = MTV[0]/MTM[0][0],
        else:
            self.basis = tuple(matfunc.Square(MTM).solve(matfunc.Vec(MTV)))

    def error(self):
        """
        Copy of the error from te.py.
        Not sure what else to do with this.
        """
        return regutils.rms((x-1) for x in self.weightedPrimes())

class NotRationalSubgroup(Exception):
    pass

def makeLimit(limit):
    """
    Return a translation matrix and a prime limit
    given a list of either integer or pair of integers
    or strings for ratios.
    Raise NotRationalSubgroup for anything unrecognized or unfactorizable.
    """
    ratios = map(ratioFromPartial, limit)
    try:
        vectors = [regutils.factorizeRatio(*ratio) for ratio in ratios]
    except OverflowError:
        raise NotRationalSubgroup("Outside prime limit")

    mask = [False]*len(regutils.primes)
    for vector in vectors:
        for i, n in enumerate(vector):
            if n:
                mask[i] = True

    vectors = [filterByMask(mask, vector) for vector in vectors]
    return vectors, filterByMask(mask, regutils.primes)

def trivialSubgroup(vectors):
    """
    Return true if the subgroup calculation
    would be ideal to the normal TE calculation
    (usually for a standard prime limit, or something like that)
    """
    size = len(vectors[0])
    mask = [False] * size
    for vector in vectors:
        for i, n in enumerate(vector):
            if n < 0:
                return False
            if n:
                if mask[i]:
                    return False
                mask[i] = True
    return True

def makeComplexityMetric(translation, plimit):
    """
    Make the metric (Gram?) matrix for subgroup TE complexity
    as a quadratic form.

    translation -- list of basis intervals in ratio-space vector form
    plimit -- the sizes of the supergroup prime intervals
    """
    weightedTranslation = [[t*p for t, p in zip(row, plimit)]
                           for row in translation]
    dualMatrix = parametric.gramMatrix(weightedTranslation)
    return list(map(list, matfunc.Square(dualMatrix).inverse()))

def ratioFromPartial(partial):
    if isinstance(partial, int):
        return partial, 1
    elif isinstance(partial, str):
        if '/' in partial:
            try:
                n, d = map(int, partial.split('/'))
            except TypeError:
                raise NotRationalSubgroup("Unrecognized partial"
                        + repr(partial))
            except ValueError:
                raise NotRationalSubgroup("Wrong size sequence"
                        + repr(partial))
        else:
            try:
                n = int(partial)
            except TypeError:
                raise NotRationalSubgroup("Unrecognized partial"
                        + repr(partial))
            except ValueError:
                raise NotRationalSubgroup("Unrecognized partial"
                        + repr(partial))
            d = 1
        return n, d
    else:
        try:
            n, d = partial
        except TypeError:
            raise NotRationalSubgroup("Unrecognized partial" + repr(partial))
        except ValueError:
            raise NotRationalSubgroup("Wrong size sequence" + repr(partial))
        if isinstance(n, int) and isinstance(d, int):
            return n, d
    raise NotRationalSubgroup("No integral type[s]" + repr(partial))

def approximately_equal(x, y):
    if y:
        return abs(1 - x/y) < 0.0001
    return abs(x) < 0.0001

def iterables_approximately_equal(*iterables):
    lists = list(map(list, iterables))
    for each in lists:
        if len(each) != len(lists[0]):
            return False
        for x, y in zip(each, lists[0]):
            if not approximately_equal(x, y):
                return False
    return True

def filterByMask(mask, vector):
    return [v for m, v in zip(mask, vector) if m]

if __name__ == '__main__':
    for not_harmonic in [(1.0, 2, 3, 4),
                         (1, 'a', 3, 4),
                         (1, (2, '3'), 4),
                         (1, (2, 3, 4)),
                         (1, 2, 3, 4, 59)]:
        try:
            makeLimit(not_harmonic)
        except NotRationalSubgroup:
            pass
        else:
            raise AssertionError("Should not be harmonic: %r"
                                    % (not_harmonic, ))

    assert makeLimit([2,3,5,7]) == ([[1,0,0,0],
                                     [0,1,0,0],
                                     [0,0,1,0],
                                     [0,0,0,1]], regutils.primes[:4])
    assert trivialSubgroup(makeLimit([2,3,5,7])[0])

    assert makeLimit([10,12,14]) == ([[1,0,1,0],
                                      [2,1,0,0],
                                      [1,0,0,1]], regutils.primes[:4])
    assert not trivialSubgroup(makeLimit([10,12,14])[0])

    assert makeLimit([2, (3,5), (5,7)]) == ([[1,0,0,0],
                                             [0,1,-1,0],
                                             [0,0,1,-1]], regutils.primes[:4])
    assert not trivialSubgroup(makeLimit([2, (3,5), (5,7)])[0])

    no5 = regutils.primes[:6]
    del no5[2]
    assert makeLimit([2,3,7,11,13]) == ([[1,0,0,0,0],
                                         [0,1,0,0,0],
                                         [0,0,1,0,0],
                                         [0,0,0,1,0],
                                         [0,0,0,0,1]], no5)
    assert trivialSubgroup(makeLimit([2,3,7,11,13])[0])
    assert trivialSubgroup(makeLimit([2,3,13,7,11])[0])

    # These limits are special-cased by regutils, but not here.
    # All that matters is that they are recognized as trivial.
    for i in range(1,20):
        assert trivialSubgroup(makeLimit([i])[0])

    meantone = parametric.Temperament(regutils.primes[:4], 12, 19)
    trans7lim = [[1,0,0,0,0,0,0],
                 [0,1,0,0,0,0,0],
                 [0,0,1,0,0,0,0],
                 [0,0,0,1,0,0,0]]
    submeantone = TemperamentClass(trans7lim, regutils.primes[:7],
                                   meantone.melody)
    assert approximately_equal(meantone.complexity(), submeantone.complexity())
    assert approximately_equal(
            meantone.optimalError(),
            submeantone.optimalError())
    assert approximately_equal(
            meantone.optimalBadness(),
            submeantone.optimalBadness())
    assert approximately_equal(
            meantone.optimalBadness(0.1),
            submeantone.optimalBadness(0.1))
    assert approximately_equal(
            meantone.optimalBadness(0.01),
            submeantone.optimalBadness(0.01))

    red7lim = [[1,0,0,0,0,0,0],
               [-1,1,0,0,0,0,0],
               [-2,0,1,0,0,0,0],
               [-2,0,0,1,0,0,0]]
    redmeantone = [[12, 7, 4, 10], [19, 11, 6, 15]]
    submeantone = TemperamentClass(red7lim, regutils.primes[:7], redmeantone)
    assert approximately_equal(meantone.complexity(), submeantone.complexity())
    assert approximately_equal(
            meantone.optimalError(),
            submeantone.optimalError())
    assert approximately_equal(
            meantone.optimalBadness(),
            submeantone.optimalBadness())
    assert approximately_equal(
            meantone.optimalBadness(0.1),
            submeantone.optimalBadness(0.1))
    assert approximately_equal(
            meantone.optimalBadness(0.01),
            submeantone.optimalBadness(0.01))

    h19 = te.Temperament(regutils.primes[:4], 19)
    red19 = TemperamentClass(red7lim, regutils.primes[:7], [redmeantone[1]])
    assert approximately_equal(h19.complexity(), red19.complexity())
    assert approximately_equal(h19.optimalError(), red19.optimalError())
    for ek in 0.0, 0.1, 0.001:
        assert approximately_equal(
                h19.optimalBadness(ek), red19.optimalBadness(ek))

    meantone = te.Temperament(regutils.primes[:4], 12, 19)
    submeantone = RegularTemperament(red7lim, regutils.primes[:7], redmeantone)
    meantone.optimize()
    submeantone.optimize()
    assert iterables_approximately_equal(meantone.basis, submeantone.basis)
    assert approximately_equal(meantone.tuningMap()[0],
                               submeantone.tuningMap()[0])
    meantone.unstretch()
    submeantone.unstretch()
    assert len(submeantone.tuningMap()) == 4
    for x, subx in zip(meantone.tuningMap(), submeantone.tuningMap()):
        if subx == 1:
            assert x == 1
        else:
            assert approximately_equal(x % 1, subx)

    h19 = te.RegularTemperament(regutils.primes[:4], h19.melody)
    red19 = RegularTemperament(red7lim, regutils.primes[:7], [redmeantone[1]])
    h19.optimize()
    red19.optimize()
    assert approximately_equal(h19.basis[0], red19.basis[0])

    marvel = te.Temperament(regutils.primes[:4], 12, 19, 22)
    intredmarvel = te.Temperament(red19.plimit, 12, 19, 22)
    redmarvel = TemperamentClass(
            red7lim, regutils.primes[:7], intredmarvel.melody)
    assert approximately_equal(marvel.complexity(),
                              redmarvel.complexity())
    assert approximately_equal(marvel.optimalError(),
                               redmarvel.optimalError())
    for ek in 0.0, 0.1, 0.001:
        assert approximately_equal(
                marvel.optimalBadness(ek), redmarvel.optimalBadness(ek))

    marvel = te.RegularTemperament(regutils.primes[:4], marvel.melody)
    redmarvel = RegularTemperament(
            red7lim, regutils.primes[:7], intredmarvel.melody)
    assert approximately_equal(marvel.optimalError(),
                               redmarvel.optimalError())
    marvel.optimize()
    redmarvel.optimize()
    assert approximately_equal(marvel.tuningMap()[0],
                            redmarvel.tuningMap()[0])
    assert iterables_approximately_equal(
            [x % marvel.tuningMap()[0] for x in marvel.tuningMap()],
            [x % redmarvel.tuningMap()[0] for x in redmarvel.tuningMap()],
            )
    marvel.unstretch()
    redmarvel.unstretch()
    assert iterables_approximately_equal(
            [x % 1.0 for x in marvel.tuningMap()],
            [x % 1.0 for x in redmarvel.tuningMap()],
            )

    bp = te.Temperament(regutils.primes[1:4], 13, 17)
    bptrans = [[1,0,0,0], [0,1,0,0], [0,0,1,0]]
    subbp = RegularTemperament(bptrans, regutils.primes[1:5], bp.melody)
    assert iterables_approximately_equal(bp.plimit, subbp.plimit)
    assert approximately_equal(bp.complexity(), subbp.complexity())
    assert approximately_equal(bp.optimalError(), subbp.optimalError())
    for ek in 0.0, 0.1, 0.001:
        assert approximately_equal(
                bp.optimalBadness(ek), subbp.optimalBadness(ek))
    bp.optimize()
    subbp.optimize()
    assert approximately_equal(bp.tuningMap()[0],
                            subbp.tuningMap()[0])
    assert iterables_approximately_equal(
            [x % bp.tuningMap()[0] for x in bp.tuningMap()],
            [x % subbp.tuningMap()[0] for x in subbp.tuningMap()],
            )
    bp.unstretch()
    subbp.unstretch()
    assert iterables_approximately_equal(
            [x % 1.0 for x in bp.tuningMap()],
            [x % 1.0 for x in subbp.tuningMap()],
            )
