#  Regular temperament finding library
#  based on temper.py
#  only does weighted prime errors
#  originally simpler but getting bloated

from __future__ import division, print_function
# evil generic import to ease backwards compatibility
from et import *
from math import floor, ceil
from regutils import *

###############################################################################
#
#  Rank 2 temperaments constructed from equal temperaments or mappings
#
###############################################################################


# the old way of calculating the badness of a rank 2 temperament
def defaultBadness(lt, width, error):
    return width*width*error

# the new kind of badness that can be calculated more efficiently
# but for backwards compatibility has to be a function of width and error
def simpleBadness(lt, width, error):
    return width*error

def getLinearTemperaments(ets,
        highestComplexity,
        highestError,
        error=rmsError,
        badness=defaultBadness,
        useRMS=True,
        scalarComplexity=True,
        nResults=100):
    """Returns a list of temperaments as specified,
    ordered by minimax badness.

    ets -- A list of EqualTemperaments that get paired off to produce
           rank 2 temperaments.
           The EqualTemperament class is imported from et.py.
           Any objects with suitable "mapping" and "primes" will do.
    highestComplexity -- weighted as steps per octave, or whatever.
    highestError -- weighted and dimensionless.
    error -- The function of weighted primes used to calculate the error.
             Some of these are imported from et.py
    badness -- a function of a rank 2 temperament, complexity and error.
    useRMS -- A flag to say whether the temperament should be optimized
                for a TOP-RMS error.
              If false, the temperament is optimized for TOP-Max error.
              Note that if useRMS is true and error is a known RMS,
              there's no need to do a separate optimization step.
    scalarComplexity -- A flag to say whether to calculate complexity the
                     new way, as the determinant of a square matrix of
                     products of weighted mapping elements.
                     If false, use the Kees-max complexity instead.
    nResults -- The maximum number of rank 2 temperaments to return.
                If the search fails to find this many, it returns
                as many as it can.
    """
    primes = ets[0].primes # all ETs should use the same prime intervals
    temperaments = TemperamentList(nResults) # container for the results

    # consider all pairs of equal temperaments
    for i in range(len(ets)-1):
        et1 = ets[i]
        for j in range(i+1, len(ets)):
            et2 = ets[j]
            try:
                tuning = LinearTemperament(et1.mapping, et2.mapping, primes)
                if scalarComplexity:
                    width = tuning.scalarComplexity()
                else:
                    width = tuning.complexityRange()
                if width==0.0 or width > highestComplexity:
                    continue
                if useRMS and error in (rmsError, optimalRMSError):
                    optimum = tuning.optimalRMSError()
                else:
                    if useRMS:
                        tuning.optimizeRMS()
                    else:
                        if error in (topError, worstError):
                            # because the TOP-max error is always less than
                            # the TOP-RMS error, we can reject some
                            # temperament classes using the faster calculation
                            lowOptimum = tuning.optimalRMSError()
                            if lowOptimum > highestError:
                                continue
                            lowBadness = lowOptimum*width
                            if not temperaments.goodEnough(lowBadness):
                                continue
                        tuning.optimizeTOP()
                    optimum = error(tuning.weightedPrimes())
                if optimum > highestError:
                    continue
                bad = badness(tuning, width, optimum)
                if (temperaments.goodEnough(bad)
                        and tuning.contorsion()==1):
                    temperaments.add(tuning, bad)
            except DegenerateException:
                # reject temperaments that don't depend on the generator
                continue
            except ValueError:
                # Means the square root of a negative number was attempted.
                # This is an error on x86 chips.
                # It means the temperament has a very low error, maybe zero.
                # Whatever, we can do without it.
                continue

    return temperaments.retrieve()

# convenience function for when using the library interactively
def Temperament(primes, m, n=None, *p):
    """return a regular temperament object
       consistent with the given ETs
    """
    if p:
        ets = [BestET(i, primes).mapping for i in (m,n) + p]
        return RegularTemperament(primes, ets)
    if n:
        et1, et2 = BestET(m, primes).mapping, BestET(n, primes).mapping
        return LinearTemperament(et1, et2, primes) ###
    return BestET(m, primes)

temperament = Temperament # make a common typo legitimate

# named "LinearTemperament" for hysterical raisins
# does not imply an octave period
def LinearTemperament (mapping1, mapping2, primes):
    """Rank 2 temperament from two equal temperaments"""
    m, n = mapping1[0], mapping2[0]

    mGen, nGen, octave = extendedEuclid(m, n)
    if mGen < 0:
        # forget negative generators
        mGen += m//octave
        nGen += n//octave
    if 2*mGen > m//octave:
        # and put the generator between a unison and a period
        # at least as far as we can tell from that one
        m, n = n, m
        melody = transp(mapping2, mapping1)
        mGen, nGen = m//octave - nGen, n//octave - mGen
    else:
        melody = transp(mapping1, mapping2)

    assert m*nGen - n*mGen == octave

    genConv1, genConv2 = -n//octave, m//octave
    generatorMapping = [
        (genConv1*axis1 + genConv2*axis2)
        for axis1, axis2 in melody]

    periodMapping = [
        (nGen*axis1 - mGen*axis2)
        for axis1, axis2 in melody]

    return Rank2Temperament(
            primes, nGen+mGen, periodMapping, generatorMapping, melody)

# These Finders are used do find rank 2 temperaments from a single
# equal temperament mapping.
# They're complicated because they try to handle all possibilities.
# They're based on the Finders from et.py, because finding equal temperaments
# is a similar problem to finding octave-equivalent mappings.

class SquareErrorFinder:
    """Used to find values for the mapping given the
    generator size and the ET mapping
    Returns the full mapping
    """
    def __init__(self, primes, etMapping, generator, mapping=None):
        self.etMapping = etMapping
        self.generator = generator
        if mapping is None:
            octave = hcf((generator, etMapping[0]))
            mapping = [(octave, 0)]
        self.mapping = mapping
        self.primes = primes
        self.sw0 = mapping[0][0]/primes[0]
        self.sw02 = self.sw0**2
        self.sw1 = self.sw12 = self.sw01 = 0.0

    def addEntry(self, newMap):
        newPerMap = self.getPeriodMap(newMap)
        newMapping = self.mapping + [(newPerMap, newMap)]
        newFinder = self.__class__(
                self.primes, self.etMapping, self.generator, newMapping)
        prime = self.primes[len(self.mapping)]
        w0 = newPerMap/prime
        w1 = newMap/prime
        newFinder.sw0 = self.sw0 + w0
        newFinder.sw1 = self.sw1 + w1
        newFinder.sw02 = self.sw02 + w0**2
        newFinder.sw12 = self.sw12 + w1**2
        newFinder.sw01 = self.sw01 + w0*w1
        assert len(dir(newFinder))==len(dir(self))
        return newFinder

    def value(self):
        sw0, sw1 = self.sw0, self.sw1
        sw02, sw12, sw01 = self.sw02, self.sw12, self.sw01
        d = sw02*sw12 - sw01**2 # the determinant of the wNN matrix
        g0, g1 = sw0*sw12 - sw1*sw01, sw1*sw02 - sw0*sw01
        try:
            return len(self.mapping) - (g0*sw0 + g1*sw1)/d
        except ZeroDivisionError:
            return 0.0

    def convert(self, value):
        return value*value*len(self.etMapping)

    def getPeriodMap(self, generatorMap):
        steps = self.etMapping[len(self.mapping)]
        period = self.etMapping[0]//self.mapping[0][0]
        generator = self.generator//self.mapping[0][0]
        assert (steps - generatorMap*generator)%period == 0
        return (steps - generatorMap*generator)//period

class VarianceErrorFinder(SquareErrorFinder):
    """Search by the square of the STD error
    instead of the square of the TOP-RMS error
    """
    def value(self):
        sm0, sm1 = self.sw0, self.sw1
        sm02, sm12, sm01 = self.sw02, self.sw12, self.sw01
        octaveDivision = self.mapping[0][0]/self.primes[0]
        # is that right for arbitrary weights?
        i = len(self.mapping)
        try:
            vari0 = sm02 - sm0**2/i
            vari1 = sm12 - sm1**2/i
            covi = sm01 - sm0*sm1/i
            return (vari0*vari1 - covi**2)/octaveDivision**2/vari1
        except ZeroDivisionError:
            return 0.0

class ComplexityRangeFinder:
    """Used to find values for the generator mapping given the
    generator size and the ET mapping
    """
    def __init__(self, primes, etMapping, generator, mapping=[0], octave=None):
        self.initData()
        self.mapping = mapping
        self.primes = primes
        self.etMapping = etMapping
        self.generator = generator
        if octave is None:
            self.octaveDivision = hcf((etMapping[0], generator))
        else:
            self.octaveDivision = octave

    def initData(self):
        self.lo = self.hi = 0.0

    def addEntry(self, newMap):
        newMapping = self.mapping + [newMap]
        newFinder = ComplexityRangeFinder(
                self.primes, self.etMapping, self.generator,
                newMapping, self.octaveDivision)
        weightedMap = newMap/self.primes[len(self.mapping)]
        newFinder.lo = min(self.lo, weightedMap)
        newFinder.hi = max(self.hi, weightedMap)
        return newFinder

    def possibleRange(self, limit):
        """Return an iterable for all values of
        the next element of the mapping
        that might give a result within the given limit.
        """
        fmin, fmax = self.floatLimits(limit)
        nmin, nmax = int(ceil(fmin)), int(floor(fmax))

        # For this search, we know the mapping of one equal tuning.
        # We also chose a specific number of steps for the generator
        # according to that mapping.
        # These constrain the possible values for the generator mapping.
        octave = self.etMapping[0]
        period = octave // self.octaveDivision
        generator = self.generator // self.octaveDivision
        target = self.etMapping[len(self.mapping)] % period
        while (generator*nmin)%period != target:
            nmin += 1

        return list(range(nmin, nmax+1, period))

    def floatLimits(self, limit):
        """The lowest and highest values for
        the next element of the mapping
        as floating point variables to be truncated
        """
        oelimit = float(limit)/self.octaveDivision
        buoyancy = self.primes[len(self.mapping)]
        return buoyancy*(self.hi - oelimit), buoyancy*(self.lo + oelimit)

    def convert(self, value):
        return value

class HalfRangeComplexityFinder(ComplexityRangeFinder):
    def convert(self, value):
        return value*2

class STDComplexityFinder(ComplexityRangeFinder):
    """Used to find values for the generator mapping given the
    generator size and the ET mapping
    """

    def initData(self):
        self.tot = self.tot2 = 0.0

    def addEntry(self, newMap):
        newMapping = self.mapping + [newMap]
        newFinder = STDComplexityFinder(
                self.primes, self.etMapping, self.generator,
                newMapping, self.octaveDivision)
        weightedMap = newMap/self.primes[len(self.mapping)]
        newFinder.tot = self.tot + weightedMap
        newFinder.tot2 = self.tot2 + weightedMap**2
        return newFinder

    def floatLimits(self, limit):
        i = len(self.mapping)
        tot = self.tot
        oelimit = float(limit)/self.octaveDivision**2
        error = self.tot2 - tot**2/i
        assert error <= oelimit, "%g > %g"%(error, oelimit)
        target = self.primes[i]
        deficit = sqrt((i+1)*(oelimit - error)/i)
        return target*(tot/i - deficit), target*(tot/i + deficit)

    def convert(self, std):
        return len(self.primes)*std**2

# See http://x31eq.com/complete.pdf
# If the criteria are too weak the function may hog system resources
# and never return a result.
# To prevent this you can send it a LimitedResource instead of the
# default InfiniteResource
def rank2Survey(limit,
        maxComplexity, maxError, nResults=100,
        minETBadness=None,
        complexityFinder=STDComplexityFinder,
        errorFinder=VarianceErrorFinder,
        badness=defaultBadness,
        resource=InfiniteResource()):
    """
    Do a complete survey of Rank 2 temperaments within the complexity
    and error limits.
    minETBadness is a parameter for the ET search
    """
    if complexityFinder is ComplexityRangeFinder:
        etComplexity = maxComplexity * 0.5
    else:
        etComplexity = maxComplexity
    if minETBadness is None:
        # smart default
        minETBadness = sqrt(etComplexity*maxError)
    ets = seedETs(limit, etComplexity, maxError, minETBadness,
            resource)
    return allRank2Temperaments(ets, maxComplexity, maxError, nResults,
            complexityFinder, errorFinder, badness, resource)

def allRank2Temperaments(ets,
        maxComplexity, maxError, nResults=100,
        complexityFinder=STDComplexityFinder,
        errorFinder=VarianceErrorFinder,
        badness=simpleBadness,
        resource=InfiniteResource()):
    """Turns a list of equal temperaments into
    a list of rank 2 temperaments
    """
    temperaments = TemperamentList(nResults)
    for et in ets:
        mappings = allMappings(
                et.primes, et.mapping, maxComplexity, maxError,
                complexityFinder, errorFinder, resource)
        for gen, genmap, permap in mappings:
            resource.consume()
            lt = Rank2Temperament(et.primes, gen, genmap, permap,
                                  zip(et.mapping))
            bad = badness(lt, lt.stdComplexity(), lt.optimalSTDError())
            if temperaments.goodEnough(bad):
                temperaments.add(lt, bad)
    return temperaments.retrieve()

def allMappings(primes, et, maxComplexity, maxError,
        complexityFinderClass=STDComplexityFinder,
        errorFinderClass=VarianceErrorFinder,
        resource=InfiniteResource()):
    """Return all different mappings for the Equal Temperament
    with the given mapping and prime intervals
    and the error and complexity within the given range
    as a list of generator, period mapping, generator mapping tuples
    """
    octave = et[0]
    results = []
    for generator in range(octave//2 + 1):
        octaveDivision = hcf((octave, generator))
        gen = generator//octaveDivision
        complexityFinder = complexityFinderClass(primes, et, generator)
        errorFinder = errorFinderClass(primes, et, generator)
        convComplexity = complexityFinder.convert(maxComplexity)
        convError = errorFinder.convert(maxError)
        for mapping in moreMappings(complexityFinder, errorFinder,
                    convComplexity, convError, resource):
            periodMapping, generatorMapping = zip(*mapping)
            if any(generatorMapping):
                results.append((gen, periodMapping, generatorMapping))
    return results

# This is similar to the EqualTemperament Finders in et.py.
# The main difference is that both error and complexity are used
# to prune the search.
# So this is a function taking two Finders,
# rather than a method of a Finder.
def moreMappings(complexityFinder, errorFinder,
        complexityLimit, errorLimit, resource):
    """recursive helper function for allMappings"""
    resource.consume()
    results = []
    # assume the complexity's already been checked for
    if errorFinder.value() <= errorLimit:
        if len(errorFinder.mapping) == len(errorFinder.etMapping):
            # recursion stops here
            return [errorFinder.mapping] ###

        # now try the next prime along
        guesses = complexityFinder.possibleRange(complexityLimit)
        for guess in guesses:
            newComplexityFinder = complexityFinder.addEntry(guess)
            results += moreMappings(
                    newComplexityFinder,
                    errorFinder.addEntry(guess),
                    complexityLimit, errorLimit, resource)

    return results


# This is the class used to represent Rank 2 Temperaments.
# Usually you'll use a factory function instead of calling its constructor.
# It always stores the canonical mapping (involving an octave-equivalent
# generator) which means it isn't the fastest thing to use for paired-ET
# searches.
# However, it is consistent with the single ET search which always produces
# an octave-equivalent generator mapping.
class Rank2Temperament(PrimeWeighted):
    cents = 1200.0
    def __init__(self, primes, generator,
            periodMapping, generatorMapping, melody):
        """Rank2Temperament constructor.
        primes --  A list of the sizes of prime intervals in octaves.
        generator -- The number of steps to the octave-equivalent generator.
                     If the temperament is based on two ETs, this is in terms
                     of their sum.
        periodMapping -- The mapping of prime intervals in terms of the period.
        generatorMapping -- The mapping by the octave equivalent generator.
        melody -- The mapping of prime intervals to scale steps.
                  If the temperament is based on two ETs, this is a list of
                  pairs.
                  If only one ET, it's a list of one-tuples.
        """
        assert len(primes) >= len(periodMapping)
        assert len(periodMapping)==len(generatorMapping)
        self.primes = primes[:len(periodMapping)]
        self.periodMapping = periodMapping
        self.generatorMapping = generatorMapping
        self.octaveDivision = self.periodMapping[0]
        self.generator = generator
        self.primesByMelody = list(melody)

    # Originally this was an arbitrary object unique to the
    # temperament class.
    # Now it has to return the same result to be consistent
    # with different implementations.
    # It must be immutable so it can be used as a hash key.
    # You could even make __hash__ return it to use a set of
    # these objects.
    def invariant(self):
        """The octave-equivalent generator mapping
        multiplied by the number of periods to the octave.
        Identical to the octave-equivalent wedgie.
        Normalized so that the first non-zero entry is always positive.
        """
        mapping = normalize_sign(self.generatorMapping[1:])
        return tuple([x*self.octaveDivision for x in mapping])

    def complexity(self):
        """Whichever complexity measure's in favor this week"""
        return self.complexityRange()

    def complexityRange(self):
        """the range of complexities, a TOP-like measure
        Apparently the same as the maximum Kees-weighted complexity
        """
        return errorRange(self.weightedMapping())*self.octaveDivision

    def optimalSTDError(self):
        """STD error"""
        wmap0 = self.weightedPeriodMapping()
        wmap1 = self.weightedMapping()
        var0, var1 = var(wmap0), var(wmap1)
        cov01 = cov(wmap0, wmap1)
        return sqrt((var0*var1 - cov01**2)/var1)/self.octaveDivision

    def stdComplexity(self):
        """The RMS equivalent of the complexity range"""
        return stdError(self.weightedMapping())*self.octaveDivision

    def weightedMapping(self):
        """The mapping of octave-equivalent generators to prime intervals
        multiplied by the weight of the prime interval.
        Normalized to be mostly independent of the equivalence interval"""
        return [steps/prime/self.primes[0]
            for steps, prime in zip(self.generatorMapping, self.primes)]

    def weightedPeriodMapping(self):
        """The mapping of octave-equivalent generators to prime intervals
        multiplied by the weight of the prime interval.
        Normalized to be mostly independent of the equivalence interval"""
        return [steps/prime/self.primes[0]
            for steps, prime in zip(self.periodMapping, self.primes)]

    def unstretch(self):
        """Stretch the tuning to give pure octaves"""
        period = 1.0/self.octaveDivision
        stretch = self.basis[0]/period
        self.basis = (period, self.basis[1]/stretch)

    def optimalRMSError(self):
        """Return the optimal prime, RMS, weighted error"""
        g0, g1, sw0, sw1 = self.magicSums()
        try:
            return sqrt(1.0 - (g0*sw0 + g1*sw1)/len(self.primes))
        except OverflowError:
            # negative square root, must be pretty small...
            return 0.0

    def optimizeRMS(self):
        """Set the prime, RMS, weighted errors optimum"""
        self.basis = self.magicSums()[:2]

    def magicSums(self):
        """Return values useful for RMS calulations"""
        sw0 = sw1 = sw02 = sw12 = sw01 = 0.0

        for m0, m1, p in zip(self.periodMapping, self.generatorMapping, self.primes):
            w0, w1 = m0/p, m1/p
            sw0 += w0
            sw1 += w1
            sw02 += w0**2
            sw12 += w1**2
            sw01 += w0*w1

        d = sw02*sw12 - sw01**2 # the determinant of the wNN matrix
        if d==0.0:
            raise DegenerateException("null generator mapping")
        g0, g1 = (sw0*sw12 - sw1*sw01)/d, (sw1*sw02 - sw0*sw01)/d
        return g0, g1, sw0, sw1

    def scalarComplexity(self):
        """A complexity measure that doesn't depend on the choice of
        equivalence interval.
        It's usually very close to the standard deviation complexity.
        """
        sM02 = sM12 = sM01 = 0.0

        for m0, m1, p in zip(
                self.periodMapping, self.generatorMapping,
                self.primes):
            M0, M1 = m0/p, m1/p
            sM02 += M0**2
            sM12 += M1**2
            sM01 += M0*M1

        d = sM02*sM12 - sM01**2 # the determinant of the sMNN matrix
        return sqrt(d)/len(self.primes)

    def tuningMap(self):
        """The size of each tempered prime interval in octaves."""
        assert hasattr(self, "basis"), "Must have a tuning"
        period, generator = self.basis
        return [
            (period*perMap + generator*genMap)
            for perMap, genMap
            in zip(self.periodMapping, self.generatorMapping)]

    def optimizeTOP(self):
        """Set the TOP generators

        Requires PuLP and GLPK
        """
        import pulp
        prob = pulp.LpProblem("top", pulp.LpMinimize)

        # set three variables: the generators and the thing to minimize
        # all of them have to be positive
        period = pulp.LpVariable("period", 0, None)
        generator = pulp.LpVariable("generator", 0, None)
        error = pulp.LpVariable("error", 0, None)

        # specify that it's the error we want to minimize
        prob += error, "obj"

        # now set the errors of the temperament as constraints
        primes = self.primes
        for i in range(len(primes)):
            weightedPrime = (period*self.periodMapping[i] +
                    generator*self.generatorMapping[i])/primes[i] 
            # set two error constraints for an overall absolute error
            prob += error >= weightedPrime - 1
            prob += error >= 1 - weightedPrime

        prob.solve(pulp.GLPK(msg=0))

        self.basis = period.varValue, generator.varValue

    def contorsion(self):
        """A regular temperament has a contorsion of 1 if each tempered
        interval represents at least one interval from the set being
        approximated.
        Higher values refer to how complex the temperament is relative
        to an equivalent temperament with contorsion of 1.
        """
        # the simple wedgie formula would be simpler than this...
        gencon = hcf(self.generatorMapping)
        octave = self.octaveDivision
        if octave != 1:
            factors = [n for n in primeNumbers if not octave%n]
            mapping = transp(self.periodMapping, self.generatorMapping)[1:]
            percon = 1
            for factor in factors:
                for i in range(octave):
                    for p, g in mapping:
                        if (p + i*g)%factor:
                            break
                    else:
                        return gencon*hcf([p+i*g for p, g in mapping]) ###
        return gencon ###

    def getScale(self, *rangeArgs, **kwargs):
        """Return a list of cents (by default) values for a generated scale.
        Un-named arguments are as for range().
        The keyword argument "units" is the number of steps to the octave.
        """
        units = kwargs.get("units", self.cents)
        scale = []
        period, generator = self.basis
        for nSteps in range(*rangeArgs):
            for repeat in range(self.octaveDivision):
                root = (nSteps*generator)%period
                scale.append((period*repeat + root)*units)
        scale.sort()
        if scale[0]==0.0:
            return scale[1:] + [period*units*self.octaveDivision]
        return scale

    def __repr__(self):
        """The standard way of converting the temperament to a string.
        This is supposed to be readable, but still brief.
        If the temperament doesn't already have a tuning it's optimized
        for the TOP-RMS.
        """
        if not hasattr(self, 'basis'):
            self.optimizeRMS()
        try:
            m, n = self.primesByMelody[0]
            octave = m+n
        except ValueError:
            octave = self.primesByMelody[0][0]
        w = self.weightedPrimes()
        params = {
            'periodMapping' : formatMapping(self.periodMapping),
            'generatorMapping' : formatMapping(self.generatorMapping),
            'melody' : formatMappingMatrix(transp(*self.primesByMelody)),
            'complexity' : self.scalarComplexity(),
            'rmsError' : rmsError(w)*1200,
            'maxError' : worstError(w)*1200,
            'periodCents' : self.basis[0]*self.cents,
            'generatorCents' : self.basis[1]*self.cents,
            'tuningMap'  : formatIntervals(self.tuningMap()),
            'period' : (octave)//self.octaveDivision,
            'generator' : self.generator,
        }
        return """%(generator)i/%(period)i

%(periodCents)8.3f cents period
%(generatorCents)8.3f cents generator

mapping by period and generator:
[%(periodMapping)s,
 %(generatorMapping)s>

mapping by steps:
%(melody)s

tuning map:
[%(tuningMap)s> cents

scalar complexity: %(complexity).3f
RMS weighted error: %(rmsError).3f cents/octave
max weighted error: %(maxError).3f cents/octave""" % params


def extendedEuclid(f, g):
    """Return two generator sizes and the octave division
    from the sizes of two equal temperaments
    """
    # Uses the extended Euclidian algorithm.
    # From von zur Gathen & Gerhard, p.47

    assert f>0 and g>0
    lastR, nextR = f, g
    lastS, lastT, nextS, nextT = 1, 0, 0, -1

    while nextR:
        q, r = divmod(lastR, nextR)
        nextR, lastR = r, nextR
        nextS, lastS = lastS - q*nextS, nextS
        nextT, lastT = lastT - q*nextT, nextT

    return lastT, lastS, lastR

def var(x):
    """The statistical variance of a sequence"""
    size = len(x)
    tot2 = 0.0
    for each in x:
        tot2 += each*each
    return tot2/size - mean(x)**2

def cov(x, y):
    """The statistical covariance of two sequences"""
    size = len(x)
    assert size==len(y)
    totab = 0.0
    for a, b in zip(x, y):
        totab += a*b
    return totab/size - mean(x)*mean(y)

###############################################################################
#
#  Higher ranks
#
###############################################################################

class RegularTemperament(PrimeWeighted):
    """Counterpart of Rank2Temperament for arbitrary rank"""

    def __init__(self, primes, melody):
        """RegularTemperament constructor.
        primes --  A list of the sizes of prime intervals in octaves.
        melody -- The mapping of prime intervals to scale steps.
                  A list of n tuples for a rank n temperament.
        """
        for mapping in melody:
            assert len(mapping) == len(primes)
        self.primes = primes
        self.melody = melody

    def tuningMap(self):
        """The size of each tempered prime interval in octaves."""
        assert hasattr(self, "basis"), "Must have a tuning"
        map_tuning = transp(self.melody, self.basis)
        result = []
        for prime in range(len(self.primes)):
            # when was sum introduced?
            # at least it won't break compilation
            size = sum([map[prime]*tuning for map, tuning in map_tuning])
            result.append(size)
        return result

    def invariant(self):
        """A tuple to uniquely identify the temperament class"""
        reduced = lattice_reduction(self.melody)
        for i in range(len(reduced)):
            reduced[i] = reduced[i][i:]
        reduced.reverse()
        result = []
        for entry in reduced:
            result.extend(entry)
        return tuple(result)

    def complexity(self):
        """Scalar complexity"""
        return size_of_matrix(self.weightedMapping())

    def optimalBadness(self, Ek=0.0):
        """Scalar badness"""
        scaling = 1 - Ek/sqrt(1 + Ek**2)
        M = []
        for row in self.weightedMapping():
            rowmean = scaling*mean(row)
            M.append([m - rowmean for m in row])
        return size_of_matrix(M)

    def degenerate(self):
        """Check if the ETs are linearly dependent"""
        return lattice_reduction(self.melody)[-1] == [0]*len(self.primes)

    def optimalError(self):
        """TOP-RMS error"""
        return self.optimalBadness()/self.complexity()

    def optimize(self):
        """TOP-RMS"""
        if len(self.melody)==1:
            # special case for equal temperaments:
            m = self.weightedMapping()[0]
            self.basis = mean(m)/rms(m)**2,
        else:
            import matfunc # http://users.rcn.com/python/download/python.htm
            M = matfunc.Matrix(self.weightedMapping()).tr()
            V = matfunc.Vec([1.0]*M.rows)
            self.basis = tuple(M.solve(V))

    def weightedMapping(self):
        result = []
        for map in self.melody:
            result.append([m/p for m, p in zip(map, self.primes)])
        return result

    def tunedBlock(self, units=12e2):
        octaves = [m[0] for m in self.melody]
        return list(self.tuneScale(fokkerBlock(*octaves), units))

    def tuneScale(self, notes, units=12e2):
        for note in notes:
            pitch = 0.0
            for i, tuning in zip(note, self.basis):
                pitch += i*tuning *units
            yield pitch

    def getscale(self, d, units=12e2):
        """Return a list of cents (by default) values for a generated scale
        with d notes to the octave.
        """
        evenScales = [[maximallyEven(d, self.melody[0][0],)]]
        for et in self.melody[1:]:
            # try to minimize the number of distinct scale steps
            candidates = []
            for rot in range(d):
                scale = maximallyEven(d, et[0], rot)
                for oldScale in evenScales:
                    newScale = oldScale + [scale]
                    candidates += [(nStepSizes(newScale), newScale)]
            candidates.sort()
            nDistinctSteps = candidates[0][0]
            evenScales = []
            for dist, scale in candidates:
                if dist > nDistinctSteps:
                    break
                evenScales.append(scale)

        tunings = self.basis
        result = []
        for degrees in zip(*evenScales[0]):
            result.append(sum([d*t for d, t in zip(degrees, tunings)])*units)
        return result

    def name(self, warts=consecutiveWarts):
        """Returns the name of the temperament class,
        and the number of prime dimensions below that it's
        properly defined in.
        """
        try:
            name, extras = nameThatMapping(self.melody)
            return name, extras
        except KeyError:
            mapping = list(self.melody)
            mapping.sort()
            ets = [EqualTemperament(mel, self.primes) for mel in mapping]
            return " & ".join([each.name(warts) for each in ets]), 0

    def unstretch(self):
        stretch = self.tuningMap()[0]
        self.basis = tuple([x/stretch for x in self.basis])

    def __repr__(self):
        """basic stringification"""
        name, extras = self.name()
        return name.replace(u"\xfc", "ue") + '+'*extras

    def __str__(self):
        """The standard way of converting the temperament to a string.
        This is supposed to be readable, but still brief.
        """
        name, extras = self.name()
        if " & " in name:
            # no friendly name
            name = ""
        else:
            name += "+"*extras + "\n\n"
        name = name.replace(u"\xfc", "ue")
        if not hasattr(self, 'basis'):
            self.optimize()
        params = {
            'name'       : name,
            'melody'     : formatMappingMatrix(self.melody),
            'reduced'    : formatMappingMatrix(lattice_reduction(self.melody)),
            'complexity' : self.complexity(),
            'rmsError'   : self.rmsError()*1200,
            'maxError'   : self.worstError()*1200,
            'tuning'     : formatIntervals(self.basis),
            'tuningMap'  : formatIntervals(self.tuningMap()),
        }
        return """%(name)smapping by steps:
%(melody)s

step tunings:
<%(tuning)s] cents

reduced mapping:
%(reduced)s

tuning map:
[%(tuningMap)s> cents

scalar complexity: %(complexity).3f
RMS weighted error: %(rmsError).3f cents/octave
max weighted error: %(maxError).3f cents/octave""" % params


def formatIntervals(intervals):
    return ', '.join(['%.3f'%(x*1200) for x in intervals])

def fokkerBlock(*octaves):
    return transp(*[maximallyEven(octaves[0], j, -1) for j in octaves])

def maximallyEven(d, n, rotation=0):
    """A maximally even d from n scale.
    Rotations from 1 to d should cover all possibilities.
    """
    return [(i*n + n + rotation%d)//d for i in range(d)]

def checkFokker(*octaves):
    N = octaves[0]
    for row in fokkerBlock(*octaves):
        for j, octave in enumerate(octaves):
            assert 0 <= N*row[j] - row[0]*octave < N

def intervals(scale):
    lower = [0]+scale
    return [note - lower[i] for i, note in enumerate(scale)]

def nStepSizes(tunings):
    """The number of distinct step sizes in a scale
    defined by 'tunings' as a list of lists of degrees
    of different equal temperaments,
    assuming an arbitrary regular tuning.
    """
    steps = [intervals(tuning) for tuning in tunings]
    distinct = set(zip(*steps))
    return len(distinct)

###############################################################################
#
#  Test code
#
###############################################################################

# The tests output the numbers of notes to the equal temperaments
# and the invariants for various searches.
# I meant them to be useful searches but the main point is to preserve
# the same output and ensure that different versions of the library
# produce the same results.
# If I find a bug the output may have to change but I try to minimize
# the difference.

# One day, this function will have a standard interface,
# so that I can share the test code between different libraries...
def write_lts(primes, etCutoff=0.17,
        nETs=20, highestComplexity=2, worstError=0.002,
        useRMS=True, scalarComplexity=True, error=rmsError):
    """
    Searches for equal temperaments,
    then rank 2 temperaments by pairing the equal temperaments,
    and writes the results to standard output.

    The output format is supposed to be compact and simple,
    so that it can be duplicated by scripts written in different
    programming languages.
    The equal temperaments are written as a space separated list of
    numbers of notes per octave, all on one line.
    Where there are a lot of them, the middle ones are elided.
    The rank 2 temperaments are written as a space separated list of
    invariants (octave-equivalent generator mappings in a standard form),
    themselves written the normal way for Python tuples.
    The output should always be identical, so even details of whitespace
    are important.

    The parameters are:
    primes -- The sizes of the prime intervals.
    etCutoff -- The worst complexity*error badness for equal temperaments.
    nETs -- The number of equal temperaments to use in the search.
    highestComplexity -- An arbitrary number relating to the complexity
                         of rank 2 temperaments.
                         It gets converted to make sense for the given
                         prime limit.
    worstError -- The maximum error for rank 2 temperaments.
    useRMS -- Whether to optimize rank 2 temperaments for TOP-RMS or
              TOP-max error.
    scalarComplexity -- Whether to use the new complexity measure
                     (close to the standard deviation of the weighted mapping)
                     or the old one (range of the weighted mapping).
    error -- The error function for both equal and rank 2 temperaments.
    """
    # make adjustments
    if scalarComplexity:
        highestComplexity *= len(primes)**2 * 0.125 / primes[0]
    else:
        highestComplexity *= len(primes)**2 * 0.3 / primes[0]
    etCutoff /= sqrt(len(primes))

    ets = getEqualTemperaments(primes, nETs=nETs, cutoff=etCutoff, error=error)
    if len(ets)>30:
        for temp in ets[:10]:
            print(temp[0], end=' ')
        print('...', end=' ')
        for temp in ets[-10:]:
            print(temp[0], end=' ')
        print()
    else:
        print(' '.join([str(temp[0]) for temp in ets]))

    if scalarComplexity:
        badness = simpleBadness
    else:
        badness = defaultBadness

    for lt in getLinearTemperaments(ets, highestComplexity, worstError,
            useRMS=useRMS, scalarComplexity=scalarComplexity,
            error=error, badness=badness):
        print(lt.invariant(), end=' ')
    print("\n")

def transp(*s):
    """Transpose of a sequence as a list"""
    return list(zip(*s))

# From some formula for an ideal tubulong (hollow bar?).
# Used as an example of an inharmonic timbre.
tubulongFrequencies = [
    float(s*(s*s - 1)) / sqrt((s*s + 1) * 7.2)
    for s in range(3,9)]

tubulongPrimes = [log2(f) for f in tubulongFrequencies]

def test(scalarComplexity=True):
    """
    The standard test function.
    Does as close as possible to what it always did.
    Flag to work with different kinds of complexity.
    """
    limits = list(range(2, 11)) # indexes primes (or primeNumbers)
    print("RMS Optima\n")
    for limit in limits:
        print("%2i-limit" % primeNumbers[limit])
        write_lts(primes[:limit+1], scalarComplexity=scalarComplexity)

    print("Less good ETs\n")
    for limit in limits:
        print("%2i-limit" % primeNumbers[limit])
        write_lts(primes[:limit+1], etCutoff=0.22, worstError=0.005,
                scalarComplexity=scalarComplexity)

    print("Microtemperaments\n")
    for limit in limits:
        print("%2i-limit" % primeNumbers[limit])
        write_lts(primes[:limit+1], nETs=30, highestComplexity=5, worstError=5e-4,
                scalarComplexity=scalarComplexity)

    for f in tubulongFrequencies:
      print("%.5f"%f, end=' ')
    print('')

    # Note that the lowest partial for the tubulong timbre is
    # exactly 1.5 octaves.
    # This means the prime intervals are linearly dependent,
    # and so don't form a true bases.
    # It also means that, for the simplest searches here,
    # and exact solution is possible!
    # This is still something the library should be able to handle,
    # but it's a pain to keep the results consistent.
    print("Octave-Equivalent Tubulongs\n")
    for limit in range(2, len(tubulongPrimes)+1):
        write_lts(([1.0]+tubulongPrimes)[:limit+1],  worstError = 0.002,
                scalarComplexity=scalarComplexity)

    print("Straight Tubulongs\n")
    for limit in range(2, len(tubulongPrimes)):
        write_lts(tubulongPrimes[:limit+1], worstError = 0.002,
                scalarComplexity=scalarComplexity)

# Currently this isn't consistent with the other libraries with
# new complexity.
# That's probably because this one is incorrect, and because
# there's a loss of precision.
def bigTest(nETs=300, scalarComplexity=True):
    """Like test() but geared towards more equal temperaments,
    and with an option of how many
    """
    limits = list(range(2, 11)) # indexes primes (or primeNumbers)
    print("RMS Optima\n")
    for limit in limits:
        print("%2i-limit" % primeNumbers[limit])
        write_lts(primes[:limit+1], nETs=nETs, etCutoff=0.2,
                highestComplexity=4, worstError=0.002,
                scalarComplexity=scalarComplexity)

    print("Inconsistent ETs\n")
    for limit in limits:
        print("%2i-limit" % primeNumbers[limit])
        write_lts(primes[:limit+1], nETs=nETs, etCutoff=0.3,
                highestComplexity=4, worstError=0.002,
                scalarComplexity=scalarComplexity)

    print("Microtemperaments\n")
    for limit in limits:
        print("%2i-limit" % primeNumbers[limit])
        write_lts(primes[:limit+1], nETs=nETs, etCutoff=0.2,
                highestComplexity=7, worstError=5e-4,
                scalarComplexity=scalarComplexity)

def surveyTest():
    """A quick test of the guaranteed search"""
    try:
        rank2Survey(limit11, 4, 5e-4, resource=LimitedResource(500))
        print("resource not exhausted")
    except ResourceExhausted:
        miraculous = rank2Survey(limit11, 4, 5e-4,
                resource=LimitedResource(10000))
        assert len(miraculous)==1
        assert len(
                rank2Survey(limit11, 8, 5e-4,
                    complexityFinder = ComplexityRangeFinder,
                    errorFinder = SquareErrorFinder,
                    resource=LimitedResource(10000))
                ) == 1
        assert len(
                rank2Survey(limit11, 4, 5e-4,
                    complexityFinder = HalfRangeComplexityFinder,
                    errorFinder = SquareErrorFinder,
                    resource=LimitedResource(10000))
                ) == 1
        assert rank2Survey(limit11, 2.788, 0.484/1200,
                resource=LimitedResource(10000))==[]
        assert rank2Survey(limit11, 2.7881, 0.484/1200,
                resource=LimitedResource(10000))==[]
        assert rank2Survey(limit11, 2.788, 0.4841/1200,
                resource=LimitedResource(10000))==[]
        assert len(rank2Survey(limit11, 2.7881, 0.4841/1200,
            resource=LimitedResource(10000)))==1
        assert len(rank2Survey(limit13, 4.9, 0.52/1200,
            resource=LimitedResource(10000)))==3
        assert len(rank2Survey(limit13, 12.5, 0.52/1200,
            complexityFinder = ComplexityRangeFinder,
            errorFinder = SquareErrorFinder,
            resource=LimitedResource(10000))) == 1
    # this erroneously gave ennealimmmal:
    assert rank2Survey(limit7, 4.7, 0.0298/1200)==[]
    # this gave an assertion failure:
    assert len(rank2Survey(limit11, 4.5, 0.484/1200))==2
    # test non-consecutive primes
    limit = [log2(f) for f in (2, 5, 11, 13)]
    assert allMappings(limit, [87, 202, 301, 322], 1.47, 0.101/1200)==[]
    assert allMappings(limit, [87, 202, 301, 322], 1.475, 0.101/1200)==[
            (40, (1, 6, 3, 6), (0, -8, 1, -5))]

# If you run the library as a script, it does a test.
# Different options give different tests.
if __name__=='__main__':
    import sys
    if len(sys.argv)>1 and sys.argv[1].startswith('bignew'):
        bigTest(scalarComplexity=True)
    elif len(sys.argv)>1 and sys.argv[1].startswith('big'):
        bigTest(scalarComplexity=False)
    elif len(sys.argv)>1 and sys.argv[1].startswith('sur'):
        surveyTest()
    elif len(sys.argv)>1 and sys.argv[1].startswith('new'):
        test(scalarComplexity=True)
    else:
        test(scalarComplexity=False)

