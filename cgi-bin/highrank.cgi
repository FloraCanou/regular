#!/usr/local/bin/python3 -O

from __future__ import print_function

print("Content-Type: text/html; charset=utf-8\r")
print("Cache-Control: public, max-age=14400\r")
print("\r")

import cgi, math, traceback, re
import parametric, regutils, regular_html, te, kernel, et

cents = 1200.0
nResults = 10
multiplier_for_page = [0.03, 0.1, 0.3, 0.7, 1.0, 1.3, 1.5]

failString="""
<p>
didn\'t work
</p>
<pre>
%s
</pre>
"""

title = """<html>
<head><title>%s-limit Super-Temperaments</title></head>
<body>
"""

form = """
<form action="highrank.cgi" method="get">
    <input type="hidden" name="ets" value="%s">
    <input type="submit" value="%s">
    <input type="hidden" name="page" value="%i">
    <input type="hidden" name="limit" value="%s">
</form>
"""

def search(rt, plimit, numbers, multiplier):
    print("<p>%s-prime limit</p>" % ".".join(map(str, numbers)))
    ek = rt.optimalError()*multiplier
    rt.optimize()
    lists = list(parametric.superSurvey(rt, ek, nResults, safety=40))
    printResults([[rt]] + lists, numbers, plimit, len(rt.melody))

def printResults(lists, numbers, plimit, initial_rank):
    assert len(numbers)==len(plimit)
    rank = initial_rank
    for rts in lists:
        if len(rts)==1:
            print("<h2>Rank %i Temperament</h2>" % rank)
            print(regular_html.formatList(rts, numbers))
        elif len(rts) > 1:
            print("<h2>Rank %i Temperaments</h2>" % rank)
            print(regular_html.formatList(rts, numbers))
        rank += 1

try:
    data = cgi.FieldStorage()
    limitstr = data["limit"].value
    numbers, plimit = regutils.textToLimit(limitstr)
    etNameMatch = r'[a-z]?\d+(?:[a-z]+|[.][0-9]*)?'
    etNames = re.findall(etNameMatch, data["ets"].value)
    if "page" in data:
        page = int(data["page"].value)
    else:
        page = 4

    print(title % regutils.formatPrimeLabels(numbers, '.'), end='')
except Exception as e:
    print("<head><title>Failed Temperament Search</title></head><body><pre>")
    print(cgi.escape(str(e)))
    print("</pre>")
else:
    try:
        warts = regutils.wartsFromPrimeLabels(numbers)
        ets = [et.NamedET(name, plimit, warts) for name in etNames]
        rt = te.RegularTemperament(plimit, ets)
        search(rt, plimit, numbers, multiplier_for_page[page])

        exits = []
        if page < len(multiplier_for_page) - 1:
            exits.append(('Simpler', page+1, numbers))
        if page > 0:
            exits.append(('More accurate', page-1, numbers))

        print("<h2>More Searches</h2>")
        for description, newPage, newLimit in exits:
            newLabels = regutils.formatPrimeLabels(newLimit, '_')
            print(form % (cgi.escape(data["ets"].value),
                    description, newPage, newLabels))
    except Exception as e:
        print(failString % cgi.escape(traceback.format_exc()))

print(regular_html.searchCaveat)
print("""
    <p><a href="/temper/">do something else</a></p>
  </body>
</html>
""")
