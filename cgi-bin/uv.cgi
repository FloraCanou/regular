#!/usr/local/bin/python3 -O

from __future__ import print_function

print("Content-Type: text/html; charset=utf-8\r")
print("Cache-Control: public, max-age=14400\r")
print("\r")

import cgi, math, traceback
try:
    from urllib import urlencode
except ImportError:
    from urllib.parse import urlencode
import parametric, regutils, regular_html, te, kernel

cents = 1200.0
nResults = 10
simpleResults = 5
multiplier_for_page = 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 6, 7, 8, 9, 10

failString="""
<p>
didn\'t work
</p>
<pre>
%s
</pre>
"""

title = """<html>
<head><title>%s-limit Regular Temperaments from Unison Vectors</title></head>
<body>
"""

form = """
<form action="uv.cgi" method="%s">
    <input type="hidden" name="uvs" value="%s">
    <input type="submit" value="%s">
    <input type="hidden" name="page" value="%i">
    <input type="hidden" name="limit" value="%s">
</form>
"""

def search(uvs, plimit, numbers, multiplier):
    print("<p>%s-prime limit</p>" % ".".join(map(str, numbers)))
    uvrank = regutils.rank(uvs)
    ek = parametric.ek_for_search(uvs, plimit)*multiplier
    if uvrank >= len(plimit):
        lists = [[]]
    else:
        rt = te.RegularTemperament(plimit, kernel.temper_out(uvs))
        rt.optimize()
        lists = parametric.subSurvey(rt, ek, nResults)
        printResults(lists, uvs, numbers, plimit)
    if len(lists[0]) < 2 and uvrank > 1:
        print("<p>Trying subsets</p>")
        for i in range(len(uvs)):
            subuvs = uvs[:i] + uvs[i+1:]
            rt = te.RegularTemperament(plimit, kernel.temper_out(subuvs))
            rt.optimize()
            lists = parametric.subSurvey(rt, ek, nResults)
            printResults(lists, subuvs, numbers, plimit)

def printResults(lists, uvs, numbers, plimit):
    assert len(numbers)==len(plimit)
    if len(uvs) > 1:
        print("<h2>Unison Vectors</h2>")
        print(", ".join(
            [regular_html.formatUnisonVector(uv, numbers) for uv in uvs]))
    ets = lists[0]
    if len(ets)==1:
        print("<h2>Equal Temperament</h2>")
    else:
        print("<h2>Equal Temperaments</h2>")
    etstrs = [regular_html.etLink(et, numbers, plimit) for et in ets]
    print("<p>%s</p>" %", ".join(etstrs))

    rank = 1
    for rts in lists[1:]:
        rank += 1
        to_keep = nResults + 2 - rank
        keepers = rts[:to_keep]
        if len(keepers)==1:
            print("<h2>Rank %i Temperament</h2>" % rank)
            print(regular_html.formatList(keepers, numbers))
        elif len(keepers) > 1:
            print("<h2>Rank %i Temperaments</h2>" % rank)
            print(regular_html.formatList(keepers, numbers))
        else:
            # That's your lot
            break

try:
    data = cgi.FieldStorage()
    if "limit" in data:
        numbers, plimit = regutils.textToLimit(data["limit"].value)
        uvs = regutils.textToIntervals(data['uvs'].value, numbers)
    else:
        vectors = regutils.textToIntervals(data['uvs'].value)
        uvs, plimit = regutils.setBestLimit(vectors)
        numbers = regutils.primeNumbers[:len(plimit)]
    if "page" in data:
        page = int(data["page"].value)
    else:
        page = 2
    print(title % regutils.formatPrimeLabels(numbers, '.'), end='')
except Exception as e:
    print("<head><title>Failed Temperament Search</title></head><body><pre>")
    print(cgi.escape(str(e)))
    print("</pre>")
else:
    try:
        # Avoid an assertion failure (if they were enabled)
        uvs = [uv[:len(plimit)] for uv in uvs]
        search(uvs, plimit, numbers, multiplier_for_page[page])

        exits = []
        if page < len(multiplier_for_page) - 1:
            exits.append(('Simpler', page+1, numbers))
        if numbers[-1] in regutils.primeNumbers:
            i = list(regutils.primeNumbers).index(numbers[-1])
            try:
                newLimit = list(numbers) + [regutils.primeNumbers[i+1]]
                exits.append(('%s-limit'%
                    regutils.formatPrimeLabels(newLimit, '.'),
                    page, newLimit))
            except IndexError:
                pass
        if page > 0:
            exits.append(('More accurate', page-1, numbers))

        print("<h2>More Searches</h2>")
        for description, newPage, newLimit in exits:
            newLabels = regutils.formatPrimeLabels(newLimit, '_')
            uvString = ' '.join(
                    [regular_html.simpleVector(vector, newLimit)
                        for vector in uvs])
            protocol = "get" if len(uvString) < 100 else "post"
            print(form % (protocol, uvString, description, newPage, newLabels))

        uvString = ' '.join(
                    [regular_html.simpleVector(vector, numbers)
                        for vector in uvs])
        if len(uvString) < 50:
            print("<h2>URL for this page</h2>")
            url = "http://x31eq.com/cgi-bin/uv.cgi?" + urlencode(
                    dict(uvs=uvString,
                         page=page,
                         limit=regutils.formatPrimeLabels(numbers, '_')))
            print('<p><a href="%s">%s</a></p>' % (url, url))

    except Exception as e:
        print(failString % cgi.escape(traceback.format_exc()))

print(regular_html.searchCaveat)
print("""
    <p><a href="/temper/uv.html">try another</a> or
        <a href="/temper/">do something else</a></p>
  </body>
</html>
""")

