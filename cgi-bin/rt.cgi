#!/usr/local/bin/python3 -O

print("Content-Type: text/html; charset=utf-8\r")
print("Cache-Control: public, max-age=14400\r")
print("\r")

import cgi, re, traceback
import parametric, regutils, regular_html, et, names

def printAll(ets, p_names, plimit, numbers, warts):
    if p_names:
        newets = et.NamedETs(p_names[0], plimit, warts)
        dec = [(t.optimalRMSError(), t) for t in newets]
        dec.sort()
        for newet in [t.mapping for err, t in dec]:
            printAll(ets + [newet], p_names[1:], plimit, numbers, warts)
    else:
        mapping = regutils.lattice_reduction(ets)
        if regutils.rank(mapping)==len(ets):
            try:
                name, adds = regutils.nameThatMapping(mapping, numbers)
                if adds:
                    name += " extension"
                printer.writeTitle("%s-limit %s" % (
                                   regutils.formatPrimeLabels(numbers),
                                   name))
            except KeyError:
                printer.writeTitle("%s-limit Regular Temperament" %
                                   regutils.formatPrimeLabels(numbers))
            printer.write(regular_html.formatRT(plimit, numbers,
                                                mapping, ets,
                                                unstretch, subgroup_te))
            labels = regutils.formatPrimeLabels(numbers, '_')
            etnames = "_".join(
                    et.EqualTemperament(mapping, plimit).name(warts)
                    for mapping in ets)
            printer.write(sub_form % (cgi.escape(etnames), labels))
            printer.write(super_form % (cgi.escape(etnames), labels))

sub_form = """
<form action="lowrank.cgi" method="get">
    <input type="hidden" name="ets" value="%s">
    <input type="submit" value="Subsets">
    (lower rank temperaments including this one)
    <input type="hidden" name="limit" value="%s">
</form>
"""
super_form = """
<form action="highrank.cgi" method="get">
    <input type="hidden" name="ets" value="%s">
    <input type="submit" value="Supersets">
    (higher rank temperaments that include this one)
    <input type="hidden" name="limit" value="%s">
</form>
"""

class Printer(object):
    def __init__(self):
        self.wrote_title = False
    def writeTitle(self, text):
        if self.wrote_title:
            return
        self.write("<head><title>%s</title></head></body>" % text)
        self.wrote_title = True
    def write(self, text):
        print(text)
    def writeError(self, text):
        if not self.wrote_title:
            self.writeTitle("Failed Temperament Search")
        self.write("<pre>%s</pre>" % text)

printer = Printer()

def addET(etName, mapping, etset, plimit, warts):
    # note: etset is really a list
    mapping = list(mapping)
    for result in et.NamedETs(etName, plimit, warts):
        result = tuple(result.mapping)
        if regutils.rank(mapping + [result]) == len(mapping):
            if result not in etset:
                etset.append(result)
                if regutils.rank(etset)==len(etset):
                    return etset
                del etset[-1]
    while etName and not etName[-1].isdigit():
        # allow for a wart being incorrect
        etName = etName[:-1]
    octave = int(etName)
    for nResults in range(1, len(etset)+2):
        for result in parametric.etsForMapping(
            mapping, plimit, nResults=nResults, octave=octave):
            if result not in etset:
                etset.append(result)
                if regutils.rank(etset)==len(etset):
                    return etset
                del etset[-1]
    raise ValueError("suitable ET not found")

try:
    data = cgi.FieldStorage()
    limitstr = data["limit"].value
    numbers, plimit = regutils.textToLimit(limitstr)
    if "invariant" in data:
        invariant = list(map(int, data["invariant"].value.split('_')))
    elif "key" in data:
        invariant = list(map(int, data["key"].value.split('_')))
    else:
        invariant = None
    unstretch = "tuning" in data and data["tuning"].value == "po"
    subgroup_te = "subgroup" in data and data["subgroup"].value == "on"
    etNameMatch = r'[a-z]?\d+(?:[a-z]+|[.][0-9]*)?'
    etNames = re.findall(etNameMatch, data["ets"].value)
    warts = regutils.wartsFromPrimeLabels(numbers)

    if invariant is None:
        # get the best ETs with the given octave sizes
        printAll([], etNames, plimit, numbers, warts)
    else:
        rank = len(etNames)
        mapping = regutils.mappingFromInvariant(invariant, rank)
        if rank != len(mapping):
            raise IndexError("Inconsistent ranks")
        if len(numbers) != len(mapping[0]):
            raise IndexError("Inconsistent prime intervals")
        rt = parametric.TemperamentClass(plimit, mapping)
        etset = []
        for etName in etNames:
            addET(etName, mapping, etset, plimit, warts)
        printer.writeTitle("%s-limit %s" % (
                           regutils.formatPrimeLabels(numbers),
                           names.namesByLimit.get(tuple(numbers), {})
                                .get(tuple(invariant), "regular temperament")))

        print(str(regular_html.formatRT(plimit, numbers,
                                        mapping, etset,
                                        unstretch)))
        labels = regutils.formatPrimeLabels(numbers, '_')
        # Don't trust the original etNames
        etnames = "_".join([et.EqualTemperament(etmap, plimit).name(warts)
                            for etmap in etset])
        printer.write(super_form % (cgi.escape(etnames), labels))
except Exception as e:
    printer.writeError(cgi.escape(traceback.format_exc()))
    printer.writeError(cgi.escape(str(e)))

print("""
    <p><a href="/temper/">temperament finding scripts</a></p>
  </body>
</html>
""")
