#!/usr/local/bin/python3 -O

print("Content-Type: text/html; charset=utf-8\r")
print("Cache-Control: public, max-age=14400\r")
print("\r")

import cgi, math, traceback
try:
    from urllib import urlencode
except ImportError:
    from urllib.parse import urlencode
import parametric, regutils, regular_html
# For native code
import json, subprocess

cents = 1200.0
nResults = 10
safety = 40

failString="""
<p>
didn\'t work
</p>
<pre>
%s
</pre>
"""

title = """<html>
<head><title>%s-limit Regular Temperaments</title></head>
<body>
"""

morelink = '<a href="more.cgi?%s">show more of these</a>'

def moreResults(rank, limit, error):
    limitstr = regutils.formatPrimeLabels(limit, '_')
    params = {'r': rank, 'limit': limitstr, 'error': error}
    return morelink % urlencode(params)

def show_ets(ets, numbers, error):
    print("<h2>Equal Temperaments</h2>")
    etss= [regular_html.etLink(et, numbers, plimit)
            for et in ets[:nResults]]
    print("<p>%s</p>" %", ".join(etss))
    print(moreResults(1, numbers, error))

def show_r2(rts, numbers, ek, error):
    print("<h2>Rank 2 Temperaments</h2>")
    print(regular_html.formatList(rts, numbers, ek))
    print(moreResults(2, numbers, error))

def show_rts(rts, numbers, ek, error):
    rank = len(rts[0].melody)
    if len(rts) == 1:
        print("<h2>Rank %i Temperament</h2>" % rank)
    else:
        print("<h2>Rank %i Temperaments</h2>" % rank)
    print(regular_html.formatList(rts, numbers, ek))
    print(moreResults(rank, numbers, error))

try:
    data = cgi.FieldStorage()
    limitstr = data["limit"].value
    numbers, plimit = regutils.textToLimit(limitstr)
    print((title % regutils.formatPrimeLabels(numbers)))
    error = float(data["error"].value)
    if error < 0.01:
        raise ValueError("Error must be at least 10 millicents")
    if error > 600:
        raise ValueError("Error must be less than a tritone")
except Exception as e:
    print("<head><title>Failed Temperament Search</title></head><body><pre>")
    print(cgi.escape(str(e)))
    print("</pre>")
else:
    try:
        print("<p>%s-prime limit</p>" % regutils.formatPrimeLabels(numbers))
        ek = error/max(plimit)/1200

        try:
            command = ['../../protected/regular', str(nResults), str(ek * 1200)]
            if all(isinstance(number, int) for number in numbers):
                command += list(map(str, numbers))
                proc = subprocess.Popen(command, stdout=subprocess.PIPE)
            else:
                command.append('cents')
                proc = subprocess.Popen(command,
                        stdout=subprocess.PIPE,
                        stdin=subprocess.PIPE,
                        )
                proc.stdout.readline()  # instructions
                proc.stdin.write(''.join(str(x*1200) + '\n' for x in plimit))
                proc.stdin.close()
            rank = 1
            for line in proc.stdout:
                if not line.strip():
                    continue
                mappings = json.loads(line)
                if rank == 1:
                    show_ets([et for [et] in mappings], numbers, error)
                else:
                    rts = [parametric.TemperamentClass(plimit, mapping)
                            for mapping in mappings]
                    if rank == 2:
                        show_r2(rts, numbers, ek, error)
                    else:
                        show_rts(rts, numbers, ek, error)
                rank += 1

            print("<!-- Rust made me -->")

        except Exception:
            ets = parametric.getEqualTemperaments(plimit, ek, nResults + safety)
            show_ets(ets, numbers, error)

            if len(plimit) > 2:
                rts = parametric.getR2Mappings(plimit, ets, ek, nResults)
                show_r2(rts, numbers, ek, error)

            for rank in range(3, len(plimit)):
                try:
                    res = nResults + 2 - rank
                    if res < 2:
                        # there's a bug where nResults = 1
                        # look into it
                        break
                    rts = parametric.higherRankSearch(plimit, ets, rts, ek, res)
                    show_rts(rts, numbers, ek, error)
                except Exception as e:
                    print(failString % cgi.escape(traceback.format_exc()))

        exits = []
        exits.append(('Simpler',
            {'limit':data['limit'].value, 'error':(error*1.1)}))

        if regutils.arePrimeNumbers(numbers):
            newLimit = regutils.primeNumbers[len(numbers)-2]
            exits.append(("%i-limit"%newLimit,
                {'limit':newLimit, 'error':error}))

            if len(numbers) < len(regutils.primeNumbers):
                newLimit = regutils.primeNumbers[len(numbers)]
                exits.append(("%i-limit"%newLimit,
                    {'limit':newLimit, 'error':error}))

        exits.append(('More accurate',
            {'limit':data['limit'].value, 'error':(error*0.9)}))

        print("<h2>More Searches</h2>")
        messages = ['<a href="pregular.cgi?%s">%s</a>' %
                    (urlencode(dict), description)
                    for description, dict in exits]
        print(('&nbsp;'*3).join(messages))

    except Exception as e:
        print(failString % cgi.escape(traceback.format_exc()))

print(regular_html.searchCaveat)
print("""
    <p><a href="/temper/pregular.html">try another</a> or
        <a href="/temper/">do something else</a></p>
  </body>
</html>
""")
