#!/usr/local/bin/python3 -O

print("Content-Type: text/html; charset=utf-8\r")
print("Cache-Control: public, max-age=14400\r")
print("\r")

import cgi, math, traceback
import parametric, regutils, regular_html
# For native code
import json, subprocess

safety = 10

failString="""
<p>
didn\'t work
</p>
<pre>
%s
</pre>
"""

title = """<html>
<head><title>%s-limit Rank %i Temperaments</title></head>
<body>
"""

try:
    data = cgi.FieldStorage()
    limitstr = data["limit"].value
    numbers, plimit = regutils.textToLimit(limitstr)
    if len(numbers) < 4:
        nResults = 50
    elif len(numbers) < 5:
        nResults = 40
    else:
        nResults = 30
    error = float(data["error"].value)
    if error < 0.01:
        raise ValueError("Error must be at least 10 millicents")
    if error > 600:
        raise ValueError("Error must be less than a tritone")
    rank = int(data["r"].value)
    print(title % (regutils.formatPrimeLabels(numbers), rank))
except Exception as e:
    print("<head><title>Failed Temperament Search</title></head><body><pre>")
    print(cgi.escape(str(e)))
    print("</pre>")
else:
    try:
        print("<p>%s-prime limit</p>" % regutils.formatPrimeLabels(numbers))
        ek = error/max(plimit)/1200

        try:
            command = ['../../protected/regular', str(nResults), str(ek * 1200)]
            if all(isinstance(number, int) for number in numbers):
                command += list(map(str, numbers))
                proc = subprocess.Popen(command, stdout=subprocess.PIPE)
            else:
                command.append('cents')
                proc = subprocess.Popen(command,
                        stdout=subprocess.PIPE,
                        stdin=subprocess.PIPE,
                        )
                proc.stdout.readline()  # instructions
                proc.stdin.write(''.join(str(x*1200) + '\n' for x in plimit))
                proc.stdin.close()
            input_rank = 1
            for line in proc.stdout:
                if not line.strip():
                    continue
                if input_rank == rank:
                    mappings = json.loads(line)
                    if rank == 1:
                        ets = [et for [et] in mappings]
                    else:
                        rts = [parametric.TemperamentClass(plimit, mapping)
                                for mapping in mappings]
                input_rank += 1

            print("<!-- Rust made me -->")

        except Exception:
            if rank == 1:
                ets = parametric.getEqualTemperaments(plimit, ek, nResults)
            elif rank == 2:
                rts = parametric.survey2(plimit, ek, nResults, safety)
            else:
                rts = parametric.survey(rank, plimit, ek, nResults, safety)

        if rank == 1:
            etss= [regular_html.etLink(et, numbers, plimit)
                    for et in ets[:nResults]]
            print("<p>%s</p>" %",\n".join(etss))
        else:
            print(regular_html.formatList(rts, numbers, ek))

    except Exception as e:
        print(failString % cgi.escape(traceback.format_exc()))

print(regular_html.searchCaveat)
print("""
    <p>
        <a href="/temper/">do something else</a></p>
  </body>
</html>
""")
