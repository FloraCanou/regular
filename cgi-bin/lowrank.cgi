#!/usr/local/bin/python3 -O

from __future__ import print_function

print("Content-Type: text/html; charset=utf-8\r")
print("Cache-Control: public, max-age=14400\r")
print("\r")

import cgi, math, traceback, re
import parametric, regutils, regular_html, te, kernel, et

cents = 1200.0
nResults = 10
simpleResults = 5
multiplier_for_page = [1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 6, 7, 8, 9, 10]

failString="""
<p>
didn\'t work
</p>
<pre>
%s
</pre>
"""

title = """<html>
<head><title>%s-limit Sub-Temperaments</title></head>
<body>
"""

form = """
<form action="lowrank.cgi" method="get">
    <input type="hidden" name="ets" value="%s">
    <input type="submit" value="%s">
    <input type="hidden" name="page" value="%i">
    <input type="hidden" name="limit" value="%s">
</form>
"""

def search(rt, plimit, numbers, multiplier):
    print("<p>%s-prime limit</p>" % ".".join(map(str, numbers)))
    ek = rt.optimalError()*multiplier
    rt.optimize()
    lists = parametric.subSurvey(rt, ek, nResults)
    printResults(lists, numbers, plimit)

def printResults(lists, numbers, plimit):
    assert len(numbers)==len(plimit)
    ets = lists[0]
    if len(ets)==1:
        print("<h2>Equal Temperament</h2>")
    else:
        print("<h2>Equal Temperaments</h2>")
    etstrs = [regular_html.etLink(et, numbers, plimit) for et in ets]
    print("<p>%s</p>" %", ".join(etstrs))

    rank = 1
    for rts in lists[1:]:
        rank += 1
        keepers = rts[:nResults+2-rank]
        if len(keepers)==1:
            print("<h2>Rank %i Temperament</h2>" % rank)
            print(regular_html.formatList(keepers, numbers))
        elif len(keepers) > 1:
            print("<h2>Rank %i Temperaments</h2>" % rank)
            print(regular_html.formatList(keepers, numbers))

try:
    data = cgi.FieldStorage()
    limitstr = data["limit"].value
    numbers, plimit = regutils.textToLimit(limitstr)
    etNameMatch = r'[a-z]?\d+(?:[a-z]+|[.][0-9]*)?'
    etNames = re.findall(etNameMatch, data["ets"].value)
    if "page" in data:
        page = int(data["page"].value)
    else:
        page = 2

    print(title % regutils.formatPrimeLabels(numbers, '.'), end='')
except Exception as e:
    print("<head><title>Failed Temperament Search</title></head><body><pre>")
    print(cgi.escape(str(e)))
    print("</pre>")
else:
    try:
        warts = regutils.wartsFromPrimeLabels(numbers)
        ets = [et.NamedET(name, plimit, warts) for name in etNames]
        reduced_mapping = regutils.lattice_reduction(ets)
        # The search is more efficient with a reduced mapping
        rt = te.RegularTemperament(plimit, reduced_mapping)
        search(rt, plimit, numbers, multiplier_for_page[page])

        exits = []
        if page < len(multiplier_for_page) - 1:
            exits.append(('Simpler', page+1, numbers))
        if page > 0:
            exits.append(('More accurate', page-1, numbers))

        print("<h2>More Searches</h2>")
        for description, newPage, newLimit in exits:
            newLabels = regutils.formatPrimeLabels(newLimit, '_')
            print(form % (cgi.escape(data["ets"].value),
                    description, newPage, newLabels))
    except Exception as e:
        print(failString % cgi.escape(traceback.format_exc()))

print(regular_html.searchCaveat)
print("""
    <p><a href="/temper/">do something else</a></p>
  </body>
</html>
""")

