#!/usr/bin/env python3

from __future__ import print_function

print("Cache-Control: public, max-age=14400\r")
print("Content-Type: text/plain; charset=utf-8\r\n\r")

import sys, cgi, re, traceback
import te, subgroup, parametric, regutils, regular_html, et

failString="""
didn\'t work

%s
"""

def addET(etName, mapping, etset, plimit, warts):
    # note: etset is really a list
    mapping = list(mapping)
    for result in et.NamedETs(etName, plimit, warts):
        result = tuple(result.mapping)
        if regutils.rank(mapping + [result]) == len(mapping):
            if result not in etset:
                etset.append(result)
                if regutils.rank(etset)==len(etset):
                    return etset
                del etset[-1]
    while etName and not etName[-1].isdigit():
        # allow for a wart being incorrect
        etName = etName[:-1]
    octave = int(etName)
    for nResults in range(1, len(etset)+2):
        for result in parametric.etsForMapping(
            mapping, plimit, nResults=nResults, octave=octave):
            if result not in etset:
                etset.append(result)
                if regutils.rank(etset)==len(etset):
                    return etset
                del etset[-1]
    raise ValueError("suitable ET not found")

try:
    data = cgi.FieldStorage()
    limitstr = data["limit"].value
    numbers, plimit = regutils.textToLimit(limitstr)
    if "invariant" in data:
        invariant = map(int, data["invariant"].value.split('_'))
    elif "key" in data:
        invariant = map(int, data["key"].value.split('_'))
    else:
        invariant = None
    etNameMatch = r'[a-z]?\d+(?:[a-z]+|[.][0-9]*)?'
    etNames = re.findall(etNameMatch, data["ets"].value)
    if "tuning" in data and data["tuning"].value=="po":
        po = True
    else:
        po = False
    if "subgroup" in data and data["subgroup"].value == "on":
        subgroup_te = True
    else:
        subgroup_te = False
    if "error" in data:
        error = float(data["error"].value)
    else:
        error = None

except Exception as e:
    print("Failed Temperament Search")
    print(cgi.escape(str(e)))
    print()
else:
    try:
        if invariant is None:
            # get the best ETs with the given octave sizes
            warts = regutils.wartsFromPrimeLabels(numbers)
            etset = [et.NamedET(name, plimit, warts) for name in etNames]
            mapping = regutils.lattice_reduction(etset)
        else:
            rank = len(etNames)
            mapping = regutils.mappingFromInvariant(invariant, rank)
            if rank != len(mapping):
                raise IndexError("Inconsistent ranks")
            if len(numbers) != len(mapping[0]):
                raise IndexError("Inconsistent prime intervals")
            if error is None:
                rt = parametric.TemperamentClass(plimit, mapping)
            etset = []
            for etName in etNames:
                addET(etName, mapping, etset, plimit, numbers)
        rt = te.RegularTemperament(plimit, etset)
        if subgroup_te:
            try:
                translation, supergroup = subgroup.makeLimit(numbers)
                rt = subgroup.RegularTemperament(
                        translation, supergroup, etset)
            except Exception as e:
                pass
        rt.optimize()
        if po:
            rt.unstretch()
        text = regular_html.scalaFile(rt, numbers)
        # Make the page recognized as a Windows text file
        sys.stdout.write(text.replace('\n', '\r\n'))
    except Exception as e:
        print(failString % cgi.escape(traceback.format_exc()))
