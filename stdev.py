"""
Tunings based on standard deviation of weighted errors.
"""

from __future__ import division
from math import sqrt
import te, regutils, parametric

class RegularTemperament(te.RegularTemperament):
    def error(self):
        return stdev([(x-1) for x in self.weightedPrimes()])

    def optimize(self):
        if len(self.melody) == 2:
            var1, var2, cov12 = self.varvarcov()
            tuning = (var1 - cov12) / (var1 + var2 - 2*cov12)
            tuning0 = (1 - tuning) * self.plimit[0] / self.melody[0][0]
            tuning1 = tuning * self.plimit[0] / self.melody[1][0]
            self.basis = tuning0, tuning1
        elif len(self.melody) == 1:
            self.basis = self.plimit[0]/self.melody[0][0],
        else:
            raise NotImplementedError("Only defined for ranks 1 and 2")

    def optimalError(self):
        if len(self.melody) == 2:
            var1, var2, cov12 = self.varvarcov()
            return sqrt((var1*var2 - cov12**2) / (var1+var2 - 2*cov12))
        if len(self.melody) == 1:
            xm, = self.weightedMapping()
            return stdev([(m / xm[0] - self.plimit[0]) for m in xm])
        raise NotImplementedError("Only defined for ranks 1 and 2")

    def complexity(self):
        if len(self.melody) == 2:
            m0, m1 = regutils.echelon_form(self.weightedMapping())
            return m0[0] * stdev(m1)
        if len(self.melody) == 1:
            return self.melody[0][0] / self.plimit[0]
        raise NotImplementedError("Only defined for ranks 1 and 2")

    def varvarcov(self):
        """Useful things for other calculations"""
        xm, ym = self.weightedMapping()
        x = [(m/xm[0] - 1) for m in xm]
        y = [(m/ym[0] - 1) for m in ym]
        return cov(x,x), cov(y,y), cov(x,y)

def stdev(w):
    """Standard deviation of w"""
    return sqrt(cov(w,w))

def cov(x, y):
    """Covariance of x and y"""
    meansq = regutils.mean([a*b for (a, b) in zip(x, y)])
    meanprod = regutils.mean(x) * regutils.mean(y)
    return meansq - meanprod

def Temperament(plimit, *octaves):
    rt = RegularTemperament(plimit, [parametric.bestET(plimit, n)
                                     for n in octaves])
    rt.optimize()
    return rt
temperament = Temperament
