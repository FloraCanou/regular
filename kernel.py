"""Code for converting between mappings and unison vectors"""

from __future__ import division

import regutils, matfunc

def add_more_uvs(uvs, plimit, nResults=None):
    """
    Returns a selection of more unison vectors
    (not always a superset of the originals)
    """
    if len(uvs) == 1:
        return uvs
    others = secondary_uvs(uvs, plimit)
    unison = (0,) * len(plimit)
    uvset = set(map(tuple, others))
    uvset.add(unison)
    for depth in range(4):
        for uva in uvs:
            for uvb in others:
                zipped = list(zip(uva, uvb))
                sum_vectors = [x + y for (x, y) in zipped]
                diff_vectors = [x - y for (x, y) in zipped]
                newuvs = [uv for uv in (sum_vectors, diff_vectors)
                          if contorsion([uv]) == 1]
                normalize_intervals(newuvs, plimit)
                uvset.update(map(tuple, newuvs))
        others = uvset.copy()
    uvset.remove(unison)
    return tenney_sort(uvset, plimit)[:nResults or len(uvs) * 2]

def unison_vectors(mapping, plimit):
    """Returns a basis of unison vectors"""
    basis = saturated_basis(mapping)
    reduced = TLLL(basis, plimit, tc=4)
    normalize_intervals(reduced, plimit)
    return reduced

def temper_out(uvs):
    basis = saturated_basis(uvs)
    return regutils.lattice_reduction(basis)

def saturated_basis(vectors):
    """Kernel basis without contorsion"""
    return saturate(kernel_basis(vectors))

#
# Code to simplify results
#

def secondary_uvs(uvs, plimit):
    """Single permutation of the basis"""
    assert uvs
    if len(uvs)==1:
        return uvs
    result = [uv for uv in uvs]
    for i, uv1 in enumerate(uvs):
        for uv2 in uvs[i+1:]:
            z = list(zip(uv1, uv2))
            result += [[x+y for (x, y) in z], [x-y for (x, y) in z]]
    return tenney_sort(result, plimit)

def tenney_sort(vectors, plimit):
    """
    Sort intervals by Tenney harmonic distance,
    and make positive
    """
    result = sorted(vectors, key=lambda uv: thd(uv, plimit))
    normalize_intervals(result, plimit)
    return result

def normalize_intervals(vectors, plimit):
    """Make all intervals positive -- destructive"""
    for i, row in enumerate(vectors):
        size = sum([v*p for (v,p) in zip(row, plimit)])
        if size < 0.0:
            vectors[i] = [-x for x in row]

def reverse_hermite(vectors):
    rev = [reversed(v) for v in vectors]
    hermite = regutils.lattice_reduction(rev)
    return [list(reversed(v)) for v in hermite]

def thd(vector, plimit):
    """Tenney Harmonic Distance"""
    return sum([abs(v*p) for (v,p) in zip(vector, plimit)])

def TLLL(vectors, limit, tc=2):
    """Tenney weighted LLL reduction of intervals.
    Use reciprocal weighting for intervals.
    """
    weights = [p*p for p in limit]
    def tenneyEuclid(u, v):
        assert len(u)==len(v) and len(u) <= len(weights)
        return sum([x*y*w for x, y, w in zip(u, v, weights)])
    return LLL(vectors, tenneyEuclid, tc=tc)

#
# Code to find a kernel
#

def kernel_basis(vectors):
    """Integer basis that may have contorsion"""
    # c.f. http://en.wikipedia.org/wiki/Null_space#Basis
    n_cols = len(vectors[0])
    if __debug__:
        for vector in vectors:
            assert len(vector)==n_cols

    I = [[int(i == j) for i in range(n_cols)] for j in range(n_cols)]
    BC = regutils.echelon_form(zip(*(vectors + I)))
    result = [row[-n_cols:] for row in BC
              if any(row) and not any(row[:-n_cols])]

    if __debug__:
        # check the null space is correct
        for row in result:
            assert regutils.tempersOut(vector, row)
    return result

#
# LLL code
#

def LLL(vectors, prod, tc=2):
    """
    LLL reduction, with a suitable Euclidean inner product prod

    tc is a termination contraint, the lower the closer
    vectors are to each other.  Or something.
    The book set it at 2.  I think it works from 2 to 4.
    (c.f. https://math.mit.edu/~apost/courses/18.204-2016/18.204_Xinyue_Deng_final_paper.pdf)
    """
    # from von zur Gathen & Gerhard p. 452
    g, n = [list(row) for row in vectors], len(vectors)
    gs, m = GSO(g, prod)
    i = 1
    while i < n:
        for j in reversed(range(i)):
            g[i] = [x - roundLLL(m[i][j]) * y for (x, y) in zip(g[i], g[j])]
            gs, m = GSO(g, prod) # indentation?
        if i > 0 and prod(gs[i-1], gs[i-1]) > tc*prod(gs[i], gs[i]):
            g[i-1], g[i] = g[i], g[i-1]
            gs, m = GSO(g, prod)
            i -= 1
        else:
            i += 1
    return g

def roundLLL(x):
    """The special rounding function that LLL reduction needs
    where 0.5 rounds down.
    c.f. von zur Gathen & Gerhard, p.458
    and the absence of a final rep(3,2)
    """
    return int(round(x - 1e-10))

def GSO(basis, prod):
    """
    Gram-Schmidt orthogonalization in the inner product space of prod.
    Returns the Gramian matrix and the other thing as a pair
    """
    # from von zur Gathen & Gerhard p. 449
    m, gramian, idrow = [], [], [1] + [0]*len(basis)
    for frow in basis:
        del idrow[-1]
        row = [prod(frow, g) / prod(g, g) for g in gramian] + idrow
        for mcell, grow in zip(row, gramian):
            frow = [x - mcell*g for (x, g) in zip(frow, grow)]
        gramian.append(frow)
        m.append(row)
    return gramian, m

#
# Code to deal with (con)torsion
#

def saturate(vectors):
    # c.f. http://www.wstein.org/papers/hnf/
    # pernet-stein-fast_computation_of_hnf_of_random_integer_matrices.pdf
    assert vectors
    hermite = regutils.lattice_reduction(vectors)
    transpose = zip(*hermite)
    double_hermite = regutils.lattice_reduction(transpose)
    if len(vectors)==1:
        gcd = double_hermite[0][0]
        return [[x//gcd for x in vectors[0]]]
    transformation = matfunc.Square(double_hermite[:len(vectors)]).inverse()
    saturated = transformation.tr().mmul(matfunc.Matrix(hermite))
    return integer_matrix(saturated)

def contorsion(mapping):
    hermite = regutils.lattice_reduction(mapping)
    transpose = zip(*hermite)
    double_hermite = regutils.lattice_reduction(transpose)
    total = 1
    for col in double_hermite:
        for row in col:
            if row:
                total *= row
                break
    return total

def integer_matrix(vectors):
    return [list(map(nint, v)) for v in vectors]

def nint(x):
    return int(round(x))

#
# Test code
#

if __name__=='__main__':
    # Example from Wikipedia
    wpex = [[1, 0, -3, 0, 2, -8],
            [0, 1, 5, 0, -1, 4],
            [0, 0, 0, 1, 7, -9],
            [0, 0, 0, 0, 0, 0]]
    plimit = [1.0, 1.5849625007211563, 2.3219280948873622,
            2.8073549220576042, 3.4594316186372978, 3.7004397181410922]
    wpuv = unison_vectors(wpex,  plimit)
    for vector in [[-3, 5, -1, 0, 0, 0],
                   [3, 2, -1, 2, 1, 1],
                   [-4, 2, 0, 5, -2, -1]]:
        assert vector in wpuv, vector
    assert temper_out(wpuv) == regutils.lattice_reduction(wpex)[:3]

    # Example from von zur Gathen & Gerhard p.458
    # that happens to use the same input matrix as Wikipedia
    # but gives a different result.
    ggmat = [[1,1,1],[-1,0,2],[3,5,6]]
    assert LLL(ggmat, regutils.dotprod) == [[0,1,0], [1,0,1], [-1, 0, 2]]
    # Example from von zur Gathen & Gerhard p.453
    assert LLL([[12,2],[13,4]], regutils.dotprod) == [[1, 2], [9, -4]]

    # Check something obvious
    assert list(map(regutils.getRatio, TLLL([(-4,4,-1)], plimit))) == [(81,80)]

    # Known temperaments that can cause problems
    mystery = [[29, 46, 67, 81, 100, 107], [58, 92, 135, 163, 201, 215]]
    mystuv = unison_vectors(mystery, plimit)
    mystery_commas = list(map(regutils.getRatio, mystuv))
    for comma in [(196,195), (364,363), (352,351)]:
        # 1575:1573 also comes out
        # but Pari gives 1001:1000 for this TLLL reduction
        assert comma in mystery_commas, (comma, mystery_commas)
    previous = 0.0
    for uv in tenney_sort(mystuv, plimit):
        n, d = regutils.getRatio(uv)
        prod = n * d
        assert prod > previous
        previous = prod
    assert temper_out(mystuv) == regutils.lattice_reduction(mystery)
    pajara = [[2, 3, 5, 6], [0, 1, -2, -2]]
    pajuv = unison_vectors(pajara, plimit) # spurious primes are fine
    assert list(map(regutils.getRatio, pajuv)) == [(50,49), (64,63)]
    assert temper_out(pajuv) == regutils.lattice_reduction(pajara)
    comma = regutils.factorizeRatio(50, 49)[:4]
    assert temper_out([comma]) == [[2,0,0,1], [0,1,0,0], [0,0,1,1]]
    uvs = unison_vectors(temper_out([comma]), plimit)
    assert len(uvs) == 1
    assert regutils.getRatio(uvs[0]) == (50, 49)
    nums = 2, 3, 5, 11
    sublimit = [1.0, 1.5849625007211563, 2.3219280948873622, 3.4594316186372978]
    magick = [(19,30,44,66),(3,5,7,11)]
    magickuv = unison_vectors(magick, sublimit)
    magick_commas = [regutils.getRatio(uv, nums) for uv in magickuv]
    for comma in [(45, 44)]: #, (3125, 3072)]:
        assert  comma in magick_commas
    assert temper_out(magickuv) == regutils.lattice_reduction(magick)
    miracle2 = [(72, 114, 167, 202, 249), (10, 16, 23, 28, 35)]
    assert contorsion(miracle2) == 2

