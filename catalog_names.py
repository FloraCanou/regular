import codecs, operator
import regutils, parametric, names, et, regular_html

out = codecs.open("catalog2.html", "w", encoding='utf-8')
out.write("""<html><head>
<title>Catalog of Regular Temperaments</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
</head>
<body>
""")

def lvae(x, y):
    """x is less then or very approximately equal to y"""
    return x <= y or 0 < x/y < 1.1

regutils.lae = lvae

catalogByNames = {}
for limit, lookup in names.namesByLimit.items():
    numbers, plimit = regutils.textToLimit('.'.join(map(str, limit)))
    for key, name in lookup.items():
        name = name.title()
        for rank in 2, 3, 4:
            try:
                mapping = regutils.mappingFromInvariant(key, rank)
            except IndexError:
                pass
            else:
                if len(mapping[0]) == len(numbers):
                    break
        else:
            assert name[-2:]=='Ji'
            continue
        if len(mapping[0])==rank:
            # JI or JI-like
            continue
        if key not in names.namesByRank[rank]:
            # Assume this is a canonical form or something
            continue
        rt = parametric.TemperamentClass(plimit, mapping)
        rt.betterMelody()
        if not rt.melody[1][0]:
            print("Can't find ETs for", name, plimit)
        assert rt.invariant() == key, name
        if len(mapping) < len(numbers):
            catalogByNames.setdefault(name, []).append((numbers, rt))

keys = sorted(catalogByNames.keys())
for name in keys:
    out.write('<a href="#%s">%s</a>\n'%(name, name))

for name in keys:
    out.write('<a name="%s"></a>' % name)
    rts = catalogByNames[name]
    def str_int_sorter(x):
        """Sort str and int similar to Python2"""
        return [(isinstance(y, str), y) for y in x[0]]
    rts.sort(key=str_int_sorter)
    for numbers, rt in rts:
        ets = rt.melody
        mapping = regutils.lattice_reduction(ets)
        assert rt.name()[1] == 0, name
        printer = regular_html.formatter(rt.plimit, numbers, mapping, ets)
        printer.uvs = printer.scala = ""
        # We want this to be Unicode in Python 2
        out.write(printer.__str__().replace("rt.cgi", "cgi-bin/rt.cgi"))

out.write("""
    </body>
</html>
""")

