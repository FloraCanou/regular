import re, codecs
import r2names, parametric, et, te, regutils, kernel

unidentified = r2names.nameLookup.copy()
r2lookup = {(1,1,0) : 'Pythagorean'}

r3lookup = {
        (1, 1, 0, 1, 0, 0) : 'Just Intonation',
        (1, 2, 1, 0, 2, 1, 0, 0, -5) : 'Marvel',
        # accurate 13-limit extension:
        (1, 2, -3, 4, 1, 0, 2, -1, -1, 1, 0, 0, -5, 12, -4): 'Marvel',
        # simpler 13-limit extension:
        (1, 2, -3, 3, 1, 0, 2, -1, 3, 1, 0, 0, -5, 12, -8) : 'Tripod',
        #17-limit
        (1, 2, -3, 3, -1, 1, 0, 2, -1, 3, -1,
            1, 0, 0, -5, 12, -8, 8) : 'Tripod',
        #17-limit
        (1, 2, -3, 4, 6, 1, 0, 2, -1, -1, 2,
            1, 0, 0, -5, 12, -4, -13) : 'Marvel',
        (1, 2, 3, 3, 1, 0, 2, 6, 3, 1, 0, 0, -5, -13, -8): 'Prodigy',
        (1, 3, 5, 5, 1, 0, -2, -2, -5, 1, 0, 0, -1, -5, 0): 'Thrush',
        (1, 2, 4, 3, 1, 0, 2, 2, 3, 1, 0, 0, -5, -9, -8): 'Minerva',
        (1, 2, 4, 4, 1, 0, 2, 2, -1, 1, 0, 0, -5, -9, -4): 'Athene',
        (1, 2, -3, -1, 1, 0, 2, -1, 0, 1, 0, 0, -5, 12, 6): 'Mercury',
        # Mixture of Harry and Mystery:
        (1, 1, 6, 0, -7, 1, 2, 0, 0): 'History',
        (1, 1, 1, 6, 0, -7, -2, 1, 2, 0, 0, 1): 'History',
        (1, 1, 1, 1, 6, 0, -7, -2, 9, 1, 2, 0, 0, 1, 2): 'History',
        (2, 1, 2, 1, 1, 1, 1, 1, 2) : 'breed',
        # orwell variant, 11-limit
        (4, 1, 2, 1, 3, 2, 2, 1, 0, 3, 1, 3) : 'Big Brother',
        # 13-limit
        (4, 1, 2, -2, 1, 3, 2, 2, 0, 1, 0, 3, 1, 3, 1) : 'Big Brother',
        # 17-limit
        (4, 1, 2, -2, 3, 1, 3, 2, 2, 0, 2, 1, 0, 3, 1, 3, 1, 5) : 'Big Brother',
        (3, -1, 5, 1, 0, 1, 1, 1, 0, 0, 2, -2) : 'Guanyin' ,
        (3, -1, 5, 5, 1, 0, 1, 1, -2, 1, 0, 0, 2, -2, 3): 'Guanyin',
        (3, -1, 5, -5, 1, 0, 1, 1, 1, 1, 0, 0, 2, -2, 6): 'Avalokita',
        # 13-limit
        (1, 2, 3, 4, 1, 0, -4, -6, -7, 2, 0, 0, 9, 12, 11): 'Varuna',
        # 11-limit
        (2, -1, 4, 1, 1, 2, 0, 1, 0, 0, 0, 2): 'Octarod',
        # dethrones Odin
        (42, 16, 7, 1, 7, 4, 3, 1, 0, 11, 4, 2): 'Siegfried',
        (1, 3, 1, 0, -2, 1, 0, 0, -1) : 'Starling',
        (1, 1, 1, 0, -6, 1, 0, 0, 10) : 'Hemifamity',
        (1, 0, 3, 0, -1, 1, 1, 0, 3) : 'Gamelan',
        (3, -1, 1, 0, 1, 1, 0, 0, 2) : 'Orwellian',
        (1, -4, 1, 0, 7, 1, 0, 0, 1) : 'Ragismic',
        (1, 2, 1, 0, -2, 3, 0, 0, 4) : 'Landscape',
        (1, 2, 0, 2, 0, 4, 5, 1, 1, 0, -3, 2) : 'Spectacle',
        # from tuning list
        (1, 0, 1, 3, 0, 5, -1, 2, 1, 0, 2, 3) : "Hades",
        # 7-limit
        (1, 0, 3, 0, 5, 2, 1, 0, 2) : "Hades",
        # 13-limit
        (1, 0, 1, 0, 3, 0, 5, -1, 13, 2, 1, 0, 2, 3, -2): "Hades",
        # 11-limit Marvel, close to Apollo
        (2, 4, 1, 1, 1, 4, 1, 1, 0, 1, -3, 2) : "Artemis",
        # 13-limit
        (2, 4, 1, 6, 1, 1, 4, 1, 6, 1, 0, 1, -3, 2, -5) : "Artemis",
        # variant
        (2, 4, 1, 1, 1, 1, 4, 1, -2, 1, 0, 1, -3, 2, 7) : "Diana",
        # 17-limit
        (2, 4, 1, 6, 5,
                1, 1, 4, 1, 6, 3,
                1, 0, 1, -3, 2, -5, 0) : "Artemis",
        # top of 225:224 page 4:
        # 19-limit
        (2, 4, 1, 6, 5, 5,
                1, 1, 4, 1, 6, 3, 5,
                1, 0, 1, -3, 2, -5, 0, -3) : "Artemis",
        # 23-limit
        (2, 4, 1, 6, 5, 5, 3,
                1, 1, 4, 1, 6, 3, 5, 5,
                1, 0, 1, -3, 2, -5, 0, -3, -3) : "Artemis",
        # top of 19-limit 225:224 pp2-3,  6 in the full 3 cent list:
        (2, 4, 3, 2, 3, 3,
                2, 1, 6, 4, 4, 1, 5,
                1, 0, 0, -5, -2, -1, 1, -2): "Hestia",
        # 17-limit
        (2, 4, 3, 2, 3,
                2, 1, 6, 4, 4, 1,
                1, 0, 0, -5, -2, -1, 1): "Hestia",
        # 13-limit
        (2, 4, 3, 2,
                2, 1, 6, 4, 4,
                1, 0, 0, -5, -2, -1): "Hestia",
        # 11-limit
        (2, 4, 3,
                2, 1, 6, 4,
                1, 0, 0, -5, -2): "Hestia",
        # Optimal in the 19-limit from 3 to 5 cents:
        (3, 2, 1, 1, 3, -1,
                1, 0, 1, 4, 1, 3, 4,
                1, 0, 2, 1, -3, 2, -1, -2): "Demeter",
        # 17-limit
        (3, 2, 1, 1, 3,
                1, 0, 1, 4, 1, 3,
                1, 0, 2, 1, -3, 2, -1): "Demeter",
        # 13-limit
        (3, 2, 1, 1,
                1, 0, 1, 4, 1,
                1, 0, 2, 1, -3, 2): "Demeter",
        # 11-limit
        (3, 2, 1,
                1, 0, 1, 4,
                1, 0, 2, 1, -3): "Demeter",
        # 7-limit
        (3, 2, 1, 0, 1, 1, 0, 2, 1): "Demeter",
        # 13-limit marvel
        (1, 2, -3, -2, 1, 0, 2, -1, 4, 1, 0, 0, -5, 12, 2) : "Hecate",
        # 13-limit Marvel, extends well, related to Minerva and Necromancy
        (1, 2, -3, -3, 1, 0, 2, -1, -4, 1, 0, 0, -5, 12, 17) : "Isis",
        # 17-limit
        (1, 2, -3, -3, -1, 1, 0, 2, -1, -4, -1,
                1, 0, 0, -5, 12, 17, 8) : "Isis",
        # 11-limit gap
        (4, 3, 2, 1, 1, 0, 1, 1, 0, 1, 3, 2) : "Aphrodite",
        # original 13-limit Zeus
        (2, -3, 1, -1, 1, 1, -1, 1, 1, 1, 0, 1, 4, 2, 2) : "Tinia",
        # from wiki somewhere
        (2, -3, 1, 1, 1, 1, -1, 1, -2, 1, 0, 1, 4, 2, 7) : "Zeus",
        # 11->13-limit): "Erato", # 11->13-limit
        (0, 0, 1, 0, 1, 4, 10, 0, 15, 1, 0, -4, -13, 0, -20) : "Erato",
        # 7->11-limit
        (2, 3, 4, 1, 0, -2, -2, 1, 0, 1, 4, 4) : "Octagari",
        # 13-limit
        (2, 3, 4, 5, 1, 0, -2, -2, -1, 1, 0, 1, 4, 4, 2) : "Octagari",
        # http://xenharmonic.wikispaces.com/Cataharry+family
        (1, -1, -3, 2, 0, 9, 22, 1, 0, 0, -2, -7): "Snape",
        (1, -1, -3, 1, 2, 0, 9, 22, 3, 1, 0, 0, -2, -7, -1): "Snape",
        (1, -1, 0, 2, 0, 9, 5, 2, 0, 0, -4, -1): "Hagrid",
        (1, -1, 0, 1, 2, 0, 9, 5, 3, 2, 0, 0, -4, -1, -2): "Hagrid",
        (1, -1, 1, 2, 0, 9, 9, 1, 0, 0, -2, -6): "Draco",
        (1, -1, 1, 1, 2, 0, 9, 9, 3, 1, 0, 0, -2, -6, -1): "Draco",
        # Magic variants
        # 11-limit
        (0, 0, 1, 5, 1, 12, 0, 1, 0, 2, -1, 0): "Supernatural",
        # 13-limit
        (0, 0, 1, 0, 5, 1, 12, 0, 18, 1, 0, 2, -1, 0, -2): "Supernatural",
        # Magic+
        (0, 0, 0, 1, 5, 1, 12, -8, 0, 1, 0, 2, -1, 6, 0): "Magician",
        # Praveen Venkataramana
        (3, 6, 4, 1, 2, 6, 5, 1, 0, 1, -3, -2): "Morfil",
        # Flora Canou 2020
        (4, -3, 1, 2, 2, 1, 0, 0, -1): "Canou",
        (4, -3, 1, 1, 2, 2, 2, 2, 0, 0, -2, 1): "Semicanou",
        (4, -3, 1, 1, 1, 2, 2, 2, -1, 2, 0, 0, -2, 1, 11): "Semicanou",
        (4, -3, 1, 5, 1, 2, 2, 2, 3, 2, 0, 0, -2, 1, 0): "Gentsemicanou",
        }

r4lookup = {
        (1, 1, 0, 1, 0, 0, 1, 0, 0, 0): 'Just Intonation',
        # Scott Dakota
        (1, 2, -2, -1,
            1, 0, -1, 2, 2,
            1, 0, 0, 2, -4, -3,
            1, 0, 0, 0, -3, 11, 7): 'Tannic',
        (1, 1, 1, 1,
            1, 0, 0, 0, 0,
            1, 0, 0, -4, -7, 9,
            1, 0, 0, 0, 7, 12, -13): 'Etypythia',  # "ia" added
        # Flora Canou 2020
        (1, 0,
            4, 0, 1,
            1, 2, 0, 2,
            1, 0, 2, 0, 1): "Semicanousmic",
        # Dawson "Aura" Berry 2020
        (1, 0, 
            1, 0, 0, 
            2, 0, 0, 1, 
            3, 0, 0, 0, 8): "Nexus",
        (1, 0, 
            1, 0, 0, 
            2, 0, 0, -1, 
            2, 1, 0, 0, 8): "Alpharabian", 
        (1, 0, 
            1, 0, 0, 
            2, 0, 0, -7, 
            1, 0, 0, 0, 9): "Betarabian",
        (1, -1, 
            1, 0, 0, 
            1, 0, 0, 9, 
            1, 0, 0, 0, -8): "Symbiotic", 
        (1, -1, 
            1, 0, 5, 
            1, 0, 0, -4, 
            1, 0, 0, 0, 1): "Liganellus",
        (5, 1, 
            1, 0, 0, 
            1, 0, 1, -1, 
            1, 0, 0, 1, 5): "Quartismic",
        }

new7 = open('7names.txt').read()
for n, d, name in re.findall('(?m)^(\d+)/(\d+),? "?(\w+)"?\s*$', new7):
    rt = parametric.fullSurvey([regutils.factorizeRatio(int(n), int(d))])[-1][0]
    r3lookup[rt.invariant()] = name.title()

planar11 = open('planar11.txt', 'rU').read()
pattern = r'(?m)(\w+)\n\[-?\d+(?:, -?\d+){9}\]\n(\[\[.*\]\])$'
matches = re.findall(pattern, planar11)
for name, mapping in matches:
    matrix = eval(mapping)
    invariant = regutils.lattice_invariant(matrix)
    if name=="Wonder": # avoid clash with Margo's temperament
        name = "Jove"
    r3lookup[invariant] = name

for limit, dimension in [(5,3),(7,4),(11,5),(13,6),(17,7)]:
    for ets, name in r2names.names[limit].items():
        lt = te.Temperament(regutils.primes[:dimension], *ets)
        key = lt.invariant()
        oekey = tuple(x*key[dimension-1] for x in key[:dimension-1])
        assert oekey in unidentified, (name, oekey)
        del unidentified[oekey]
        name = name.replace(r'\"u', u'\xfc')
        name = name.replace('~', ' ')
        r2lookup[lt.invariant()] = name

# Stuff that's changed in the wiki
oldKeys = [
(4, 21, -3, 1, 0, -6, 4), # Vulture -> Buzzard
(7, 9, 13, 31, 1, 6, 8, 11, 23), # Sensi -> Sensus
(1, -8, -20, 2, 0, 30, 69), # Hemischismic -> Bischismic
(9, 5, -3, 7, 11, 1, 1, 2, 3, 3, 3), # Valentine -> Lupercalia
(6, -7, -2, 15, -3, 1, 1, 3, 3, 2, 4), # Miracle -> Miraculous
(7, 9, 13, 4, 10, 1, 6, 8, 11, 6, 10), # Sensisept -> Sensis
(4, -3, 1, 2, 2), # Negripent -> Negri
(4, -3, 2, 1, 2, 2, 3), # Negrisept -> Negri
(7, 9, 1, 6, 8), # Sensipent -> Sensi
(7, 9, 13, 1, 6, 8, 11), # Sensisept -> Sensi
(1, 1, 4, 0, 3), # Dimipent -> Diminished
(1, 1, 1, 4, 0, 3, 5), # Dimisept -> Diminished
(1, -3, 5, 1, 0, 7, -5), # Pelogic -> Armodue
(4, 16, 9, 10, 29, 1, 3, 8, 6, 7, 14), # old Squares
(2, 8, 1, 1, 0, -4, 2), # Semaphore -> Godzilla
(5, 1, 12, -8, -23, 1, 0, 2, -1, 6, 11), # Magic -> Necromancy
(2, 8, -11, 1, 1, 0, 6), # Semififths -> Mohajira
(2, 8, -11, 5, 1, 1, 0, 6, 2), # Semififths -> Mohajira
(6, 5, 3, 13, 1, 0, 1, 2, 0), # Keemun -> Darjeeling
]
for going in oldKeys:
    if going in r2lookup:
        del r2lookup[going]

# Reverse engineer the above assuming consecutive prime limits
byLimit = {}
for rank, lookup in [(2, r2lookup), (3, r3lookup), (4, r4lookup)]:
    for key, name in lookup.items():
        mapping = regutils.mappingFromInvariant(key, rank)
        labels = regutils.primeNumbers[:len(mapping[0])]
        byLimit.setdefault(labels, {})[key] = name

lookups = {2:r2lookup, 3:r3lookup, 4:r4lookup}, byLimit

def addName(lookups, name, mapping, source, labels=None, recursed=False):
    byRank, byLimit = lookups
    name = name.split(',')[0].strip()
    oldname = name
    mapping = list(map(list, mapping))
    #name = name.replace(u'\xfc', 'ue')
    if u'\xc3\xbc' in name:
        print("bad encoding")
    name = name.replace(u'\xc3\xbc', u'\xfc') # utf-8
    names = [choice.strip().lower()
            for choice in re.split('[(,]', name) if choice.strip()]
    if not names:
        print(oldname, 'blank')
    try:
        lookup = byRank[len(mapping)]
    except KeyError:
        print("Unsupported rank", mapping, len(mapping))
        return
    key = regutils.lattice_invariant(mapping)
    if labels is None:
        labels = regutils.primeNumbers[:len(mapping[0])]
    else:
        labels = tuple(labels)
    newname = names[0].title()
    if key not in lookup and not recursed:
        lookup[key] = newname
    if key in byLimit.get(labels, {}):
        if lookup[key].lower() != name.lower():
            print('%r in %s as %r %r' %
                    (byLimit[labels][key], source, name.title(), key))
    else:
        byLimit.setdefault(labels, {})[key] = newname
    newMapping, newLabels = regutils.canonical_order(mapping, labels)
    if newLabels != labels:
        assert not recursed
        addName(lookups, name, newMapping, source, newLabels, True)

for key, name, limit in [
    [(2, 8, 1, 1, 0), "Vicentino", ()],
    [(2, 8, 1, 0, -4), "Godzilla", ()], # Semaphore is 2.3.7
    [(1, 4, 2, 0, -8), "Injera", ()], # already in 7-limit
    [(1, -6, 1, 0, 12), "Uncle", ()], # already in 7-limit

    # Petr's additions:
    [(8, 13, 1, 2, 3), "Unicorn", ()],
    [(4, 3, 2, 1, 3), "Doublewide", ()],
    [(2, 13, 1, 0, -8), "Immunity", ()], # Mike B's name
    [(9, 10, 1, 4, 5), "Shibboleth", ()],
    [(1, 2, 7, 0, -6), "Sevond", ()],
    [(5, 7, 2, 2, 3), "Fifive", ()],
    # 2011-10-7
    [(3, 2, 0, 1, 5, 1, 7, 14, 15), "Coblack", ()],
    [(3, 2, 0, 5, 1, 7, 14), "Coblack", ()],
    # also Trisedodge [(3, 2, 5, 1, 7), "Coblack", ()],
    [(34, 29, 23, 1, 27, 24, 20), "Quinmite", ()],
    [(34, 29, 1, 27, 24), "Quinmite", ()],
    [(27, 20, -9, 16, 1, 16, 13, -2, 12), "Tritriple", ()],
    [(27, 20, -9, 1, 16, 13, -2), "Tritriple", ()],
    [(27, 20, 1, 16, 13), "Tritriple", ()],
    [(9, -13, -3, 2, 5, 2, 5), "Lagaca", ()], # not found
    [(9, -13, 2, 5, 2), "Lagaca", ()],
    [(31, -12, 29, 14, 30, 1, 5, 1, 6, 5, 7), "Kastro", ()], # not found
    [(31, -12, 29, 14, 1, 5, 1, 6, 5), "Kastro", ()],
    [(33, 25, -21, 14, 1, 17, 14, -7, 10), "Whoops", ()],
    [(33, 25, 14, 1, 17, 14, 10), "Whoops", "2.3.5.11"], # 2012-03-30
    [(23, -13, 1, 11, -3), "Sesesix", ()],
    [(1, -5, 4, 0, 41), "Undim", ()],
    [(3, 2, -3, 1, 5, 1, 7, 21, 15), "Trisedodge", ()],
    [(3, 2, -3, 5, 1, 7, 21), "Trisedodge", ()],
    [(3, 2, 5, 1, 7), "Trisedodge", ()], # Or Trisedoge or Countdown?
    [(3, 2, 1, 5, 1, 7, 15), "Countdown", "2.3.5.11"], # 2012-03-30
    [(6, 17, -26, 15, -10, 1, 5, 12, -12, 12, -2), "Zarvo", ()],
    [(6, 17, -26, 15, 1, 5, 12, -12, 12), "Zarvo", ()],
    [(6, 17, -26, 1, 5, 12, -12), "Zarvo", ()],
    [(8, -7, 3, 7, 5), "Tertiosec", ()],
    [(2, -3, -2, 5, 1, 22, 21), "Qintosec", ()], # not found
    [(2, -3, 5, 1, 22), "Qintosec", ()],
    [(15, -14, 2, -26, -18, 1, 3, 1, 3, 1, 2), "Misneb", ()], # not found
    [(15, -14, -18, 1, 3, 1, 2), "Misneb", "2.3.5.13"], # 2012-03-30
    [(15, -14, 2, 1, 3, 1, 3), "Misneb", ()],
    [(15, -14, 1, 3, 1), "Misneb", ()],
    [(17, -6, 22, 10, 24, 1, 11, -1, 15, 9, 17), "Maquila", ()], # not found
    [(17, -6, 10, 1, 11, -1, 9), "Maquila", "2.3.5.11"], # 2012-03-30
    [(17, -6, 1, 11, -1), "Maquila", ()],
    [(9, -19, -20, -5, 1, 6, -7, -7, 1), "Untriton", ()], # not found
    [(9, -19, -20, 1, 6, -7, -7), "Untriton", ()], # not found
    [(9, -19, 1, 6, -7), "Untriton", ()], # clashes with Aufo
    [(3, 29, 11, 1, 0, -13, -3), "Trimot", ()],
    [(19, 7, -1, 13, 9, 1, 17, 8, 2, 14, 11), "Semaja", ()],
    [(19, 7, 9, 1, 17, 8, 11), "Semaja", "2.3.5.13"], # 2012-03-30
    [(19, 7, -1, 13, 1, 17, 8, 2, 14), "Semaja", ()],
    [(19, 7, -1, 1, 17, 8, 2), "Semaja", ()],
    [(19, 7, 1, 17, 8), "Semaja", ()],
    [(27, 8, 20, -1, 5, 1, 14, 6, 12, 3, 6), "Emka", ()],
    [(27, 8, -1, 5, 1, 14, 6, 3, 6), "Emka", "2.3.5.11.13"], # 2012-03-30
    [(8, -1, 5, 1, 6, 3, 6), "Emka", "2.5.11.13"],
    [(27, 8, 20, -1, 1, 14, 6, 12, 3), "Emka", ()],
    [(27, 8, 20, 1, 14, 6, 12), "Emka", ()],
    [(27, 8, 1, 14, 6), "Emka", ()],
    [(12, -31, 1, 11, -22), "Lafa", ()],
    [(19, 31, 9, 25, 14, 1, 2, 3, 3, 4, 4), "Sfourth", ()],
    [(19, 31, 9, 25, 1, 2, 3, 3, 4), "Sfourth", ()],
    [(19, 31, 9, 1, 2, 3, 3), "Sfourth", ()],
    [(19, 31, 1, 2, 3), "Sfourth", ()],
    [(10, -3, 14, 8, 12, 1, 6, 1, 9, 7, 9), "Amavil", ()],
    [(10, -3, 14, 8, 1, 6, 1, 9, 7), "Amavil", ()],
    [(10, -3, 14, 1, 6, 1, 9), "Amavil", ()],
    # Nessafof already recorded
    # 2012-03-30
    [(1, 0, -1, -1, 28, 0, 65, 123, 148), "Oquatonic", "2.3.5.7.13"],
    [(1, 0, -1, 28, 0, 65, 123), "Oquatonic", ()],
    [(1, 0, 28, 0, 65), "Oquatonic", ()],
    [(12, -19, -4, 35, -1, 1, 10, -11, 0, 28, 3), "Restles", ()],
    [(12, -19, -4, -1, 1, 10, -11, 0, 3), "Restles", "2.3.5.7.13"],
    [(12, -19, -1, 1, 10, -11, 3), "Restles", "2.3.5.13"], # 2012-03-30
    [(12, -19, -4, 35, 1, 10, -11, 0, 28), "Restles", ()],
    [(12, -19, -4, 1, 10, -11, 0), "Restles", ()],
    [(12, -19, 1, 10, -11), "Restles", ()],
    [(31, 24, -3, 15, 1, 27, 22, 1, 16), "Tremka", "2.3.5.11.13"],
    [(7, 3, -5, 9, 5, 2, 4, 5, 5, 8, 8), "Vishnean", ()], # not found
    [(5, 7, 4, 6, 2, 2, 3, 6, 6), "Fifives", "2.3.5.11.13"],
    [(5, 7, 4, 2, 2, 3, 6), "Fifives", "2.3.5.11"],
    [(5, 7, -6, 2, 2, 3, 7), "Fifives", ()],
    [(30, 13, 14, 3, 22, 1, 17, 9, 10, 5, 15), "Cotritone", ()],
    [(30, 13, 14, 3, 1, 17, 9, 10, 5), "Cotritone", ()],
    [(30, 13, 14, 1, 17, 9, 10), "Cotritone", ()],
    [(13, 2, -20, -19, -5, 1, 6, 3, -4, -3, 2), "Ditonic", ()], # not found
    [(13, 2, -19, -5, 1, 6, 3, -3, 2), "Ditonic", "2.3.5.11.13"], # 2012-03-30
    [(13, 2, 1, 6, 3), "Ditonic", ()],
    [(13, 2, 30, -19, -5, 1, 6, 3, 13, -3, 2), "Coditone", ()],
    [(13, 2, 30, -19, 1, 6, 3, 13, -3), "Coditone", ()],
    [(13, 2, 30, 1, 6, 3, 13,), "Coditone", ()],
    [(10, -27, -12, 1, 8, -15, -4), "Fasum", "2.3.5.13"], # 2012-03-30
    [(21, 32, -7, -4, 20, 1, 4, 6, 2, 3, 6), "Quartemka", ()],
    [(21, 32, -4, 20, 1, 4, 6, 3, 6), "Quartemka", "2.3.5.11.13"], # 2012-03-30
    [(21, 32, -7, -4, 1, 4, 6, 2, 3), "Quartemka", ()], # not found
    [(21, 32, 1, 4, 6), "Quartemka", ()],
    [(7, 1, -12, -11, -3, 3, 5, 7, 8, 10, 11), "Mutt", ()],
    [(7, 1, -12, -11, 3, 5, 7, 8, 10), "Mutt", ()],
    [(8, -5, 6, -4, 2, 1, 6, 4, 8), "Gwazy", ()], # not found
    [(8, -5, -4, 2, 1, 6, 8), "Gwazy", "2.3.5.11"], # 2012-03-30
    [(9, -19, 33, 1, 6, -7, 19), "Aufo", ()],
    [(9, -19, 1, 6, -7), "Aufo", ()],
    [(22, 7, 40, 1, 10, 5, 19), "Majvam", "2.3.5.13"], # 2012-03-30
    [(22, 7, 1, 10, 5), "Majvam", ()],
    [(35, 9, 1, 1, 12, 5, 4), "Dodifo", "2.3.5.13"], # 2012-03-30
    [(35, 9, 1, 12, 5,), "Dodifo", ()],
    [(17, 23, 1, 8, 11), "Maja", ()],
    [(17, 23, 22, 1, 8, 11, 12), "Maja", "2.3.5.13"],
    [(11, 30, 24, 1, 0, -2, 0), "Twentcufo", "2.3.5.11"], # 2012-03-30
    # 2012-03-30
    [(20, 23, 1, 10, 12), "Countermeantone", ()],
    [(1, 5, 5, 0, -28), "Pental", ()],
    [(25, 24, 1, 20, 20), "Counterhanson", ()],
    [(13, 8, 2, 7, 7), "Quatracot", ()],
    [(29, 4, 1, 21, 5), "Squarschmidt", ()],

    # Mike B's additions:
    [(6, 17, 1, 5, 12), "Gravity", ()],
    [(1, 3, 7, 0, -17), "Absurdity", ()],
    [(1, -1, -3, 4, 1, 0, 6, 13, -9), "Machine", "2.9.7.11.13"],

    #Magic variants:
    [(5, 1, 12, 33, 1, 0, 2, -1, -7), "Witchcraft", ()], # 11-limit
    [(5, 1, 12, 33, 18, 1, 0, 2, -1, -7, -2), "Witchcraft", ()], # 13-limit
    [(5, 1, 12, 11, 1, 0, 2, -1, 0), "Charisma", ()], # 11-limit
    [(5, 1, 12, 11, 18, 1, 0, 2, -1, 0, -2), "Charisma", ()], # 13-limit
    [(5, 1, 12, 11, -1, 1, 0, 2, -1, 0, 4), "Glamour", ()], # alternative
    [(5, 1, 12, 3, 2, 0, 4, -2, 5), "Divination", ()], # 11-limit, half-octave
    [(5, 1, 12, 2, 0, 4, -2), "Divination", ()], # 7-limit subset
    [(5, 1, 12, 3, 18, 2, 0, 4, -2, 5, -4), "Divination", ()], # 13-limit
    # 13-limit, Gene
    [(5, 1, 12, -8, -12, 2, 0, 4, -2, 12, 15), "Soothsaying", ()],
    [(5, 1, 1, 2, 0, 4, 5), "Astrology", ()], # 7-limit Divinationish
    [(5, 1, 2, 0, 4), "Astrology", ()], # 5-limit
    [(5, 1, 1, 3, 2, 0, 4, 5, 5), "Astrology", ()], # 11-limit
    [(10, 2, 24, 25, 1, 5, 3, 11, 12), "Hocus", ()], # 11-limit, half-generator
    [(10, 2, 24, 1, 5, 3, 11), "Hocus", ()], # 7-limit
    [(10, 2, 1, 5, 3), "Hocus", ()], # 5-limit
    [(10, 2, 24, 25, 36, 1, 5, 3, 11, 12, 16), "Hocus", ()], # 13-limit
    [(10, 2, 24, 1, 0, 2, -1), "Pocus", ()], # 7-limit
    [(10, 2, 5, 1, 0, 2, 2), "Spell", ()], # 7-limit Pocusish
    [(10, 2, 1, 0, 2), "Spell", ()], # 5-limit
    [(5, 1, 12, 14, 1, 0, 2, -1, -1), "Telepathy", ()], # 11-limit
    [(5, 1, 12, 14, -1, 1, 0, 2, -1, -1, 4), "Telepathy", ()], # 13-limit
    [(5, 1, 12, 14, 18, 1, 0, 2, -1, -1, -2), "Intuition", ()], # alternative
    [(5, 1, 12, -27, 1, 0, 2, -1, 12), "Horcrux", ()], # 11-limit, most evil
    [(5, 1, 12, -8, -1, 1, 0, 2, -1, 6, 4), "Sorcery", ()], # 13-limit
    # old 13-limit Magic
    [(5, 1, 12, -8, -23, 1, 0, 2, -1, 6, 11), "Necromancy", ()],
    [(3, -1, 1, 1, 3), "Slendric", "2.3.7"], # Margo's 2.3.7 temperament
    [(2, 1, 1, 0, 2), "Semaphore", "2.3.7"], # every other note of Negri
    [(2, 1, -2, 1, 0, 2, 5), "Semaphore", "2.3.7.11"],
    [(2, -4, 1, 1, 4), "Beatles", "2.3.7"],
    [(2, 13, 1, 1, -1), "Hemififths", "2.3.7"],
    [(2, 13, 5, 1, 1, -1, 2), "Hemififths", "2.3.7.11"],
    [(6, -2, 15, 1, 1, 3, 2), "Miracle", "2.3.7.11"],
    [(6, -2, 1, 1, 3), "Miracle", "2.3.7"],
    [(1, -2, 2, 0, 12), "Pajara", "2.3.7"],
    [(4, -3, 1, 0, 4), "Vulture", "2.3.7"],
    [(3, 11, 1, 0, -3), "Liese", "2.3.7"],
    [(4, 9, 1, 3, 6), "Squares", "2.3.7"],
    [(2, -4, 1, 0, 6), "Stones", "2.3.7"],
    [(5, 1, 12, 18, 1, 0, 2, -1, -2), "Magic", "2.3.5.7.13"],
    [(1, 4, 10, 15, 1, 0, -4, -13, -20), "Meanpop", "2.3.5.7.13"],
    [(7, 9, 13, 10, 1, 6, 8, 11, 10), "Sensis", "2.3.5.7.13"],
    [(7, 9, 13, 4, 1, 6, 8, 11, 6), "Sensis", ()], # 11-limit
    [(6, 5, 22, 14, 1, 0, 1, -3, 0), "Catakleismic", "2.3.5.7.13"],
    [(7, -3, 8, 2, 3, 1, 0, 3, 1, 3, 3), "Blair", ()], # Orwell
    [(7, -3, 2, 3, 1, 0, 3, 3, 3), "Blair", "2.3.5.11.13"],
    [(7, -3, 2, -19, 1, 0, 3, 3, 8), "Orwell", "2.3.5.11.13"],
    [(7, -3, 8, 12, 1, 0, 3, 1, 1), "Winston", "2.3.5.7.13"],
    [(7, 8, 2, 12, 1, 0, 1, 3, 1), "Winston", "2.3.7.11.13"],
    [(7, -3, 8, 33, 1, 0, 3, 1, -4), "Newspeak", ()], # Gene, from the Xenwiki
    # From the catalog, mapping 5-limit superpyth
    [(13, 14, 1, 5, 6), "Parakleismic", ()],
    [(2, -1, 1, 1, 2), "Lambda", "3.5.7"], # Bohlen-Pierce scale
    [(5, 4, 1, 3, 3), "Canopus", "3.5.7"], # Paul Erlich?
    [(3, 5, 1, 1, 1), "Sirius", "3.5.7"],  # Paul Erlich?
    [(3, 7, 1, 2, 3), "Toliman", "3.5.7"], # Igliashon Jones
    [(2, -3, 1, 0, 4), "Vega", "3.5.7"], # Igliashon Jones
    # add higher limits
    [(3, 17, -1, -13, -22, -20, 1, 1, -1, 3, 6, 8, 8), "Rodan", ()],
    [(3, 17, -1, -13, -22, 1, 1, -1, 3, 6, 8), "Rodan", ()],
    [(3, 17, 1, 1, -1), "Rodan", ()],
    [(19, 14, 4, 32, 1, 12, 10, 5, 21), "Marrakesh", ()], # From Herman
    [(1, 4, 4, 6, 9, 2, 0, -8, -7, -12, -21), 'Injera', ()], # 13-limit
    [(17, 6, 15, 27, 1, 12, 6, 12, 20), 'Semisept', ()], # 11-limit
    [(8, 1, 18, 20, 1, 7, 3, 15, 17), u'W\xfcrschmidt', ()], # 11-limit
    [(8, 18, 11, 20, 1, 1, 1, 2, 2), "Octacot", ()], # 11-limit
    [(1, 0, 2, 2, 3, 0, 7, -1, 1), "August", ()], # 11-limit
    [(0, 1, 0, -1, 5, 8, 0, 14, 29), "Blacksmith", ()], # 11-limit
    [(8, 13, 23, 1, 2, 3, 4), "Unicorn", ()], # 7-limit
    [(60, -8, 11, 1, 19, 0, 6), "Subneutral", ()],

    # http://xenharmonic.wikispaces.com/smallED2temperaments 2012-06-03
    [(6, 3, 7, 1, 1, 0, 2, 1, 4), "A-Team", "2.5.9.13.21"],
    [(6, 3, 7, 1, 0, 2, 1), "A-Team", "2.5.9.13"],
    [(6, 3, 1, 0, 2), "A-Team", "2.5.9"],

    # Facebook Xenharmonic Alliance, 2012-06-03
    [(5, 6, 1, 3, 4), "Sixix", ()],

    [(2, 8, 20, 1, 1, 0, -3), "Migration", ()], # 7-limit alternative
    [(2, 8, -11, 5, 1, 1, 0, 6, 2), "Mohajira", ()], # 11-limit
    [(2, 8, 20, 5, 1, 1, 0, -3, 2), "Migration", ()], # 11-limit
    [(2, 8, -11, 5, -1, 1, 1, 0, 6, 2, 4), "Mohajira", ()], # 13-limit
    [(2, 8, 20, 5, -1, 1, 1, 0, -3, 2, 4), "Migration", ()], # 13-limit
    [(2, 8, -11, 5, -1, -10, 1, 1, 0, 6, 2, 4, 7), "Mohajira", ()], # 17-limit
    # 19-limit
    [(2, 8, -11, 5, -1, -10, -6, 1, 1, 0, 6, 2, 4, 7, 6), "Mohajira", ()],
    [(2, -11, 5, 1, 1, 6, 2), "Hemif", "2.3.7.11"],
    [(2, -1, 1, 1, 4), "Hemif", "2.3.13"],
    [(2, 8, 20, 36, -1, 1, 1, 0, -3, -7, 4), "Leonhard", ()],  # Euler.pdf
    # Manuel Op de Coul, 2012-11-27:
    [(10, -8, 17, 1, -5, 1, 7, -2, 12, 4, 1), "Freivald", ()],
    # Gedankenwelt, Tuning list, 2013-06-09
    [(12, -14, -4, -1, 1, 7, -4, 1, 3), "Oracle", ()],
    # Xenharmonic Alliance - Mathematical Theory 2014-11-16
    [(2, 0, 1, 3, 0, 7, 8), "Triforce", "2.3.5.11"],
    # Gene Smith
    [(1, 15, 11, 8, 1, 0, -21, -14, -9), "Leapfrog", "2.3.7.11.13"],
    # Margo Schulter
    [(1, 15, 11, 8, 6, 1, 0, -21, -14, -9, -5), "Skidoo", "2.3.7.11.13.23"],

    # Xenharmonic Alliance - Mathematical Theory 2015-3-15
    # Scott Dakota and Gene Smith
    [(12, -14, -4, 30, 35, 12, 2, 2, 6, 6, 4, 4, 7), "Semihemisecordite", "17"],
    [(12, -14, -4, 30, 35, 43, 1, 1, 3, 3, 2, 2, 2), "Hemisecordite", "17"],
    [(6, -7, -2, 15, 2, 6, 2, 2, 6, 6, 4, 7, 7), "Semimiracle", "17"],
    # 2013-2-19 Scott Dakota
    [(12,-14,-4,30,35,12,5,2,2,6,6,4,4,7,8), "Semihemisecordite", "19"],
    [(12,-14,-4,30,35,12,5,21,2,2,6,6,4,4,7,8,7), "Semihemisecordite", "23"],
    # 2015-12-18 Scott Dakota
    [(18,-21,-6,45,1,13,1,7,-4,1,17,4,8), "Phicordial", "17"],
    [(18,-21,-6,45,1,1,7,-4,1,17,4), "Phicordial", "13"],
    [(18,-21,-6,45,1,7,-4,1,17), "Phicordial", "11"],  # contorted Miracle
    # 2015-12-22 Scott Dakota
    [(1,-20,-38,59,44,7,-3,30,34,53,1,0,34,63,-90,-66,-7,9,-43,-49,-79),
            "Gracecordial", "31"],
    [(1,-20,-38,59,44,7,-3,30,34,1,0,34,63,-90,-66,-7,9,-43,-49),
            "Gracecordial", "29"],
    [(1,-20,-38,59,44,7,-3,30,1,0,34,63,-90,-66,-7,9,-43),
            "Gracecordial", "23"],
    [(1,-20,-38,59,44,7,-3,1,0,34,63,-90,-66,-7,9), "Gracecordial", "19"],
    [(1,-20,-38,59,44,7,1,0,34,63,-90,-66,-7), "Gracecordial", "17"],
    [(1,-20,-38,59,44,1,0,34,63,-90,-66), "Gracecordial", "13"],
    [(1,-20,-38,59,1,0,34,63,-90), "Gracecordial", "11"],
    # 2016-01-13 Scott Dakota
    [(3,70,-42,69,-34,50,85,83,1,2,12,-3,13,-1,11,16,16), "Satin", "23"],
    [(3,70,-42,69,-34,50,85,1,2,12,-3,13,-1,11,16), "Satin", "19"],
    [(3,70,-42,69,-34,50,1,2,12,-3,13,-1,11), "Satin", "17"],
    [(3,70,-42,69,-34,1,2,12,-3,13,-1), "Satin", "13"],
    [(3,70,-42,69,1,2,12,-3,13), "Satin", "11"],
    [(3,70,-42,1,2,12,-3), "Satin", "7"],
    [(3,70,1,2,12), "Satin", "5"],
    # Joakim Bang Larsen 2016-03-28
    [(6,7,1,3,4), "DeLorean", "5"],
    [(15,22,1,0,0), "Mowgli", "5"],
    # Joakim Bang Larsen 2016-04-27
    [(4,-1,1,3,2), "Symbolic", "5"],
    [(5,3,1,1,2), "Lafayette", "5"],
    # 2016-05-28 David Stuetzel and Paul Erlich
    [(1, 4, -3, 1, 0, -4, 9), u"St\xfctzel", "2.3.5.19"],
    # Praveen Venkataramana 2016-06-24
    [(17, 18, 20, 1, 6, 7, 8), "Oolong", "7"],
    # Casey Hale 2016-10-02
    [(1, 1, 1, -1, 9, 0, 11, 19, 55), "Peregrine", "2.3.7.13.23"],
    # Kite Giedraitis 2017-01-14
    [(1, -1, 1, 0, 5), "Ilo", "2.3.11"],
    # Kite Giedraitis 2017-01-13
    [(1, 2, 1, 0, -1), "Yo", "2.3.5"],
    [(1, 3, 1, 0, -2), "Zo", "2.3.7"],
    [(1, 1, 1, 0, 2), "Lu", "2.3.11"],
    [(1, 1, 1, 0, 2), "Tho", "2.3.13"],
    [(1, 3, 1, 0, -1), "Thu", "2.3.13"],
    # Kite Giedraitis 2017-02-07
    # 50/49 vanishes (rryy-wT)
    [(1, 1, 2, 0, 1), "Biruyo Nowa", "2.5.7"],
    # Kite Giedraitis 2017-02-12
    # 2592/2401 = (5,4,0,-4) vanishes (r^4T)
    [(1, 1, 4, 0, 5), "Quadru", "2.3.7"],
    # 256/243 vanishes (5edo+z)
    [(0, 1, 5, 8, 0), "Sawa + Za", "2.3.7"],
    # 2187/2048 vanishes (7edo+z)
    [(0, 1, 7, 11, 0), "Lawa + Za", "2.3.7"],
    # 128/125 vanishes (g^3-w+zT)
    [(0, 1, 3, 7, 0), "Trigu Nowa + Za", "2.5.7"],
    # 26/25 vanishes (3ogg-wT)
    [(1, 2, 1, 0, -1), "Thogugu Nowa", "2.5.13"],
    # 25/24 and 1029/1024 (yy&Lz^3T)
    [(6, 3, -2, 1, 1, 2, 3), "Yoyo & Latrizo", "2.3.5.7"],
    # (-22,11,2) and 49/48 (LLyy&zzT)
    [(2, -11, 1, 1, 0, 11, 2), "Lala-yoyo & Zozo", "2.3.5.7"],
    # 2048/2025 and 256/245 (sgg&rrgT)
    [(1, -2, 1, 4, 0, 22, 5), "Sagugu & Rurugu", "2.3.5.7"],
    # 250/243 and 1029/1024 (wrong numeric index corrected 2018-02-19, 10 was 20)
    [(3, 5, -1, 3, 0, -1, 10), "Triyo & Latrizo", "2.3.5.7"],
    # 250/243 and 256/245 (y^3&rrgT)
    [(6, 10, -5, 1, 5, 8, 0), "Triyo & Rurugu", "2.3.5.7"],
    # 32805/32768 = (-15,8,1) and 50/49 (Ly&rryyT)
    [(1, -8, -8, 2, 0, 30, 31), "Layo & Biruyo", "2.3.5.7"],
    # 32805/32768 = (-15,8,1) and 49/48
    [(2, -16, 1, 1, 0, 15, 2), "Layo & Zozo", "2.3.5.7"],
    # 1331/1296 = (-4,-4,0,0,3) vanishes (1o^3T)
    [(3, 4, 1, 2, 4), "Trilo", "2.3.11"],
    # 2197/2187 = (0,-7,0,0,0,3) vanishes (s3o^3T)
    [(3, 7, 1, 0, 0), "Satritho", "2.3.13"],
    # (15,-16,0,0,3) and 352/351 (ss1o^3&3u1oT)
    [(3, 16, 7, 1, 0, -5, 0), "Sasa-trilo & Thulo", "2.3.11.13"],
    # 180224/177147 = (14,-1,0,0,1) vanishes (s1oT)
    [(1, 11, 1, 0, -14), "Salo", "2.3.11"],
    # 6656/6561 = (9,-8,0,0,0,1) vanishes (s3oT)
    [(1, 8, 1, 0, -9), "Satho", "2.3.13"],
    # 180224/177147 and 352/351 (s1o&3u1oT)
    [(1, 11, 8, 1, 0, -14, -9), "Salo & Thulo", "2.3.11.13"],
    # 2048/2025 and 49/48 (sgg&zzT)
    [(2, -4, 1, 2, 0, 11, 4), "Sagugu & Zozo", "2.3.5.7"],
    # 2048/2025 and 1029/1024 (sgg&Lz^3T)
    [(3, -6, -1, 2, 2, 7, 6), "Sagugu & Latrizo", "2.3.5.7"],
    # 2048/2025 and 2401/2400 (sgg&z^4ggT)
    [(4, -8, -3, 2, 0, 11, 8), "Sagugu & Bizozogu", "2.3.5.7"],
    # 2048/2025 and 36/35 (sgg&rgT)
    [(1, -2, 4, 2, 0, 11, -7), "Sagugu & Rugu", "2.3.5.7"],

    # From Xenharmonic Alliance Monthly Tunings
    [(2, -16, -13, -6, -1, 1, 0, 15, 14, 9, 6), "Dakota", "2.3.5.13.19.37"],
    [(2, -16, -13, -1, 1, 0, 15, 14, 6), "Dakota", "2.3.5.13.37"],
    [(2, -16, -13, 1, 0, 15, 14), "Taylor", "2.3.5.13"],
    # value add
    [(2, -16, -13, -6, 1, 0, 15, 14, 9), "Dakota", "2.3.5.13.19"],

    # Scott Dakota, 2017-04-03
    # The Xenharmonic Alliance - Mathematical Theory
    [(13, 30, 22, 16, 12, 1, 5, 9, 8, 7, 7), "Hemiskidoo", "2.5.7.11.13.23"],
    # Scott Dakota, August 2017
    [(23, 11, 18, 10, 1, 15, 9, 13, 9), "Chagall", "2.7.9.11.13"],

    # Kite Giedraitis, 2017-09-07
    # (-11,10,-2) vanishes (Lgg#2T)
    [(1, 5, 2, 0, -11), "Lagugubi", "2.3.5"],
    # 4096/3993 = (12,-1,0,0,-3) vanishes (s1u^3T)
    [(3, -1, 1, 0, 4), "Satrilu", "2.3.11"],
    # (-17,2,0,0,4) vanishes (L1o^4T)
    [(2, -1, 2, 1, 8), "Laquadlo", "2.3.11"],
    # (27,-17) vanishes (17edo+y)
    [(0, 1, 17, 27, 0), "17-edo + Ya", "2.3.5"],
    # (-19,12) vanishes (12edo+z)
    [(0, 1, 12, 19, 0), "12-edo + Za", "2.3.7"],
    # 256/243 vanishes (5edo+1a)
    [(0, 1, 5, 8, 0), "Sawa + La", "2.3.11"],

    # Casey Hale, 2017-09-24
    [(0, 3, 4, 3, 1, 0, 0), "Chartreuse", '3.13/9.17/9.7/3'],
    # Linearized
    [(4, 0, 3, 3, 3, 7, 6), "Chartreuse", '3.7.13.17'],

    # Scott Dakota, 2017-12-09
    [(2, -3, 1, 1, -1), "Archagall", '2.17/15.75/64'],
    [(3, 1, 1, 2, 5), "Archagall", '2.75.85'],

    # Scott Dakota, 2018-02-10
    [(5, -23, -36, -39, 6, -25, 1, 1, 5, 7, 8, 3, 7), "Quintannic", ()],

    # Kite Giedraitis, 2018-02-19
    # (-7,8,0,-2) vanishes (LrrT)
    [(1, 4, 2, 0, -7), "Laruru", "2.3.7"],
    # 2048/2025 and 243/242 vanishes (sgg&1uuT)
    [(2, -4, 5, 2, 0, 11, -1), "Sagugu & Lulu", "2.3.5.11"],

    # Scott Dakota (the middle one is a bonus)
    [(10, 12, -3, 26, 1, 1, 1, 4, -1), "Picasso", "2.7.9.11.13"],
    [(10, 12, -3, 26, 5, 1, 1, 1, 4, -1, 3), "Picasso", "2.7.9.11.13.15"],
    [(10, 12, -3, 26, 5, 6, 1, 1, 1, 4, -1, 3, 3),
            "Picasso", "2.7.9.11.13.15.17"],

    # Kite Giedraitis, 2018-03-23
    # (-15,11,-1) vanishes (LgT)
    [(1, 11, 1, 0, -15), "Lagu", "2.3.5"],
    # (-23,16,-1) vanishes (LLgT)
    [(1, 16, 1, 0, -23), "Lalagu", "2.3.5"],
    # (-26,15,1) vanishes (LLyT)
    [(1, -15, 1, 0, 26), "Lalayo", "2.3.5"],
    # (-22,11,2) vanishes (LLyyT)
    [(2, -11, 1, 0, 11), "Lala-yoyo", "2.3.5"],
    # (-18,7,3) vanishes (Ly^3T)
    [(3, -7, 1, 0, 6), "Latriyo", "2.3.5"],
    # (15,-5,-3) vanishes (sg^3T)
    [(3, -5, 1, 0, 5), "Satrigu", "2.3.5"],
    # (-17,2,6) vanishes (Ly^6T)
    [(3, -1, 2, 2, 5), "Latribiyo", "2.3.5"],
    # (9,9,-10) vanishes (g^10T)
    [(10, 9, 1, 9, 9), "Quinbigu", "2.3.5"],
    # (25,-14,0,-1) vanishes (ssrT)
    [(1, -14, 1, 0, 25), "Sasaru", "2.3.7"],
    # (-17,9,0,1) vanishes (LzT)
    [(1, -9, 1, 0, 17), "Lazo", "2.3.7"],
    # (-15,6,0,2) vanishes (LzzT)
    [(1, -3, 2, 0, 15), "Lazozo", "2.3.7"],
    # (22,-5,0,-5)  vanishes (sr^5T)
    [(1, -1, 5, 0, 22), "Saquinru", "2.3.7"],
    # (-14,0,0,5) vanishes (Lz^5T)
    [(1, 0, 5, 0, 14), "Laquinzo", "2.3.7"],
    # (-11,-9,0,9) vanishes (z^9T)
    [(1, 1, 9, 0, 11), "Tritrizo", "2.3.7"],
    # (-1,6,0,-3) vanishes (r^3T)
    [(1, 2, 3, 0, -1), "Triru", "2.3.7"],
    # (3,-16,0,8) vanishes (sz^8T)
    [(1, 2, 8, 0, -3), "Saquadbizo", "2.3.7"],
    # (-13,10,0,-1) vanishes (LrT)
    [(1, 10, 1, 0, -13), "Laru", "2.3.7"],
    # (21,-15,0,1) vanishes (sszT)
    [(1, 15, 1, 0, -21), "Sasazo", "2.3.7"],
    # 54/49 vanishes (rrT)
    [(2, 3, 1, 1, 2), "Ruru", "2.3.7"],
    # (7,-15,0,6) vanishes (sz^6T)
    [(2, 5, 3, 1, -1), "Satribizo", "2.3.7"],
    # (11,-14,0,4) vanishes (sz^4T)
    [(2, 7, 2, 1, -2), "Saquadzo", "2.3.7"],
    # 343/324 = (-2,-4,0,3) vanishes (z^3T)
    [(3, 4, 1, 1, 2), "Trizo", "2.3.7"],
    # (1,10,0,-6) vanishes (Lr^6T)
    [(3, 5, 2, 1, 2), "Latribiru", "2.3.7"],
    # (13,6,0,-8) vanishes (r^8T)
    [(4, 3, 2, 1, 4), "Quadbiru", "2.3.7"],
    # (3,7,0,-5) vanishes (r^5T)
    [(5, 7, 1, 1, 2), "Quinru", "2.3.7"],
    # (5,-12,0,5) vanishes (sz^5T)
    [(5, 12, 1, 0, -1), "Saquinzo", "2.3.7"],
    # (9,5,0,-6) vanishes (r^6T)
    [(6, 5, 1, 3, 4), "Tribiru", "2.3.7"],
    # (7,8,0,-7) vanishes (r^7T)
    [(7, 8, 1, 0, 1), "Sepru", "2.3.7"],
    # (1,-13,0,7) vanishes (sz^7T)
    [(7, 13, 1, 6, 11), "Sasepzo", "2.3.7"],
    # (-5,-11,0,8) vanishes (z^8T)
    [(8, 11, 1, 1, 2), "Quadbizo", "2.3.7"],
    # (17,7,0,-10) vanishes (r^10T)
    [(10, 7, 1, 9, 8), "Quinbiru", "2.3.7"],
    # (-15,-10,0,11) vanishes (z^11T)
    [(11, 10, 1, 4, 5), "Lezo", "2.3.7"],
    # (13,-6,0,0,-1) vanishes (s1uT)
    [(1, -6, 1, 0, 13), "Salu", "2.3.11"],
    # (-18,7,0,0,2) vanishes (L1ooT)
    [(2, -7, 1, 0, 9), "Lalolo", "2.3.11"],
    # (-16,-3,0,0,6) = 2 cents vanishes (1o^6T)
    [(2, 1, 3, 0, 8), "Tribilo", "2.3.11"],
    # (-27,4,0,0,6) vanishes (L1o^6T)
    [(3, -2, 2, 0, 9), "Latribilo", "2.3.11"],
    # 169/162 vanishes (3ooT)
    [(1, 2, 2, 0, 1), "Thotho", "2.3.13"],
    # (8,9,0,0,0,-6) vanishes (3u^6T)
    [(2, 3, 3, 0, 4), "Tribithu", "2.3.13"],
    # 256/245 vanishes (rrg-wT)
    [(2, -1, 1, 0, 4), "Rurugu Nowa", "2.5.7"],
    # (6,0,-5,2) vanishes (zzg^5-wT)
    [(2, 5, 1, 0, -3), "Zozoquingu Nowa", "2.5.7"],
    # (2,0,0,3,-3) vanishes (1u^3z^3-wT)
    [(1, 1, 3, 0, 2), "Triluzo Nowa", "2.7.11"],
    # 128/125 and 1029/1024 vanishes (g^3&Lz^3T)
    [(3, 0, -1, 3, 0, 7, 10), "Trigu & Latrizo", "2.3.5.7"],
    # 49/48 and 243/242 vanishes (zz&1uuT)
    [(2, 1, 5, 2, 0, 4, -1), "Zozo & Lulu", "2.3.7.11"],

    # Kite Giedraitis 2019-02-05
    # (19,-9,-2) vanishes (ssggT)
    [(2, -9, 1, 1, 5), "Sasa-gugu", "2.3.5"],

    # Kite Giedraitis 2019-03-04
    # (-6,6,0,0,-1) = 729/704 vanishes (L1uT)
    [(1, 6, 1, 0, -6), "Lalu", "2.3.11"],
    # (-10,4,0,0,0,1) vanishes (L3oT)
    [(1, -4, 1, 0, 10), "Latho", "2.3.13"],
    # (18,-9,0,0,0,-1) vanishes (s3uT)
    [(1, -9, 1, 0, 18), "Sathu", "2.3.13"],
    # (-12,5,0,0,0,0,1) vanishes (L17oT)
    [(1, -5, 1, 0, 12), "Laso", "2.3.17"],
    # (-7,7,0,0,0,0,-1) vanishes (L17uT)
    [(1, 7, 1, 0, -7), "Lasu", "2.3.17"],
    # 513/512 vanishes (L19oT)
    [(1, -3, 1, 0, 9), "Lano", "2.3.19"],
    # (-10,9,0,0,0,0,0,-1) vanishes (L19uT)
    [(1, 9, 1, 0, -10), "Lanu", "2.3.19"],
    # 289/288 vanishes (17ooT)
    [(1, 1, 2, 0, 5), "Soso", "2.3.17"],

    # Nicholas Sprenger 2019-07-30
    [(2, 18, -4, 32, -1, 1, 1, -3, 4, -6, 4), "Thomas", "13"],

    # Faras Almeer 2020-01-25
    [(1, -2, -8, -12, -15, 1, 2, 0, 11, 31, 45, 55, 5), 'Na"naa\'', "17"],
    [(1, -2, -8, -12, -15, 1, -17,
        2, 0, 11, 31, 45, 55, 5, 63), 'Na"naa\'', "2.3.5.7.11.13.17.23"],

    # Juhani Nuorvala 2020-02-06
    [(4, 9, 10, -2, 1, 3, 6, 7, 3), 'Skwairs', "2.3.7.11.13"],

    # Kite Giedraitis 2020-02-22
    # (23,-13,-1) vanishes (ssgT)
    [(1, -13, 1, 0, 23), "Sasagu", "2.3.5"],
    # (-34,23,-1) vanishes (L^3gT)
    [(1, 23, 1, 0, -34), "Trilagu", "2.3.5"],
    # (-17,8,2) vanishes (Lyy#2T)
    [(1, -4, 2, 0, 17), "Layoyobi", "2.3.5"],
    # (-19,15,-2) vanishes (LLggT)
    [(2, 15, 1, 1, -2), "Lala-gugu", "2.3.5"],
    # (9,-10,3) = 139c vanishes (sy^3T)
    [(3, 10, 1, 0, -3), "Satriyo", "2.3.5"],
    # (-29,14,3) = 186c  vanishes (LLy^3T)
    [(3, -14, 1, 1, 5), "Lala-triyo", "2.3.5"],
    # (-23,19,-3) = 178c vanishes (LLg^3T)
    [(3, 19, 1, 2, 5), "Lala-trigu", "2.3.5"],
    # (-8,11,-4) = 176c vanishes (Lg^4T)
    [(4, 11, 1, 0, -2), "Laquadgu", "2.3.5"],
    # (30,-13,-4) = 129c vanishes (ssg^4#2T)
    [(4, -13, 1, 2, 1), "Sasa-quadgubi", "2.3.5"],
    # (13,-14,4) = 118c vanishes (sy^4#2T)
    [(2, 7, 2, 1, -3), "Saquadyobi", "2.3.5"],
    # (-25,10,4) vanishes (LLy^4T)
    [(2, -5, 2, 1, 10), "Lala-quadyo", "2.3.5"],
    # (-29,11,5) = 53c vanishes (LLy^5T)
    [(5, -11, 1, 4, -3), "Lala-quinyo", "2.3.5"],
    # (11,2,-6) vanishes (sg^6#2T)
    [(3, 1, 2, 1, 4), "Satribigubi", "2.3.5"],
    # (-13,-2,7) = 100c vanishes (Ly^7#2T)
    [(7, 2, 1, 4, 3), "Lasepyobi", "2.3.5"],
    # (10,4,-7) vanishes (g^7#2T)
    [(7, 4, 1, 1, 2), "Sepgubi", "2.3.5"],
    # 243/224 = 141 cents  vanishes (Lr#2T)
    [(1, 5, 1, 0, -5), "Larubi", "2.3.7"],
    # (-23,11,0,2) vanishes (LLzzT) (za comma for Hemif)
    [(2, -11, 1, 1, 6), "Lala-zozo", "2.3.7"],
    # (-29,13,0,3) vanishes (LLz^3T)
    [(3, -13, 1, 2, 1), "Lala-trizo", "2.3.7"],
    # (17,-16,0,3) = 75c vanishes (ssz^3T)
    [(3, 16, 1, 2, 5), "Sasa-trizo", "2.3.7"],
    # (-27,10,0,4) vanishes (LLz^4T)
    [(2, -5, 2, 1, 11), "Lala-quadzo", "2.3.7"],
    # (-35,15,0,4) vanishes (L^3z^4T)
    [(4, -15, 1, 1, 5), "Trila-quadzo", "2.3.7"],
    # (-54,27,0,4) vanishes (L^4z^4T)
    [(4, -27, 1, 2, 0), "Quadla-quadzo", "2.3.7"],
    # (-39,14,0,6) vanishes (L^3z^6T)
    [(3, -7, 2, 0, 13), "Trila-tribizo", "2.3.7"],
    # (-4,15,0,-7) = 148c vanishes (Lr^7T)
    [(7, 15, 1, 4, 8), "Lasepru", "2.3.7"],
    # (26,-4,0,-7) = 10c vanishes (ssr^7T)
    [(7, -4, 1, 3, 2), "Sasa-sepru", "2.3.7"],
    # (-35,8,0,8) vanishes (LLz^8T)
    [(1, -1, 8, 0, 35), "Lala-quadbizo", "2.3.7"],
    # (36,-5,0,-10) = 2c vanishes (ssr^10T)
    [(2, -1, 5, 0, 18), "Sasa-quinbiru", "2.3.7"],
    # (-17,32,0,-12) vanishes (L^3r^12T)
    [(3, 8, 4, 1, -3), "Trila-quadtriru", "2.3.7"],
    # (9,-10,0,0,2) = 83c vanishes (s1ooT)
    [(1, 5, 2, 0, -9), "Salolo", "2.3.11"],
    # (-7,11,0,0,-3) = 68c vanishes (L1u^3T)
    [(3, 11, 1, 2, 5), "Latrilu", "2.3.11"],
    # (-9,-3,0,0,4) = 99c vanishes (1o^4T)
    [(4, 3, 1, 1, 3), "Quadlo", "2.3.11"],
    # (11,4,0,0,-5) = 51c vanishes (1u^5T)
    [(5, 4, 1, 1, 3), "Quinlu", "2.3.11"],
    # (-3,-9,0,0,5) = 39c vanishes (s1o^5T)
    [(5, 9, 1, 3, 6), "Saquinlo", "2.3.11"],
    # (-35,9,0,0,6) vanishes (LL1o^6T)
    [(2, -3, 3, 1, 16), "Lala-tribilo", "2.3.11"],
    # (-2,-14,0,0,7) vanishes (s1o^7T)  (2 * P8/7 = 11/9)
    [(1, 2, 7, 0, 2), "Saseplo", "2.3.11"],
    # (15,8,0,0,-8) =  vanishes (1u^8T) (P8/8 = 12/11)
    [(1, 1, 8, 0, 15), "Quadbilu", "2.3.11"],
    # (47,-10,0,0,-9) vanishes (s^31u^9T)
    [(9, -10, 1, 2, 3), "Trisa-tritrilu", "2.3.11"],
    # (-10,11,0,0,0,-2) = 40c vanishes (L3uuT)
    [(2, 11, 1, 0, -5), "Lathuthu", "2.3.13"],
    # (8,2,0,0,0,-3) = 82c vanishes (3u^3T)
    [(3, 2, 1, 2, 4), "Trithu", "2.3.13"],
    # (-19,5,0,0,0,3) = 31c vanishes (L3o^3T)
    [(3, -5, 1, 2, 3), "Latritho", "2.3.13"],
    # (-10,-3,0,0,0,4) = 56c vanishes (3o^4T)
    [(4, 3, 1, 2, 4), "Quadtho", "2.3.13"],
    # (-1,10,0,0,0,-4) = 57c vanishes (L3u^4T)
    [(2, 5, 2, 1, 2), "Laquadthu", "2.3.13"],
    # (9,6,0,0,0,-5) = 9c vanishes (3u^5T)
    [(5, 6, 1, 1, 3), "Quinthu", "2.3.13"],
    # (-28,6,0,0,0,5) = 14c vanishes (LL3o^5T)
    [(5, -6, 1, 3, 2), "Lala-quintho", "2.3.13"],
    # (-19,-2,0,0,0,6) vanishes (L3o^6T)
    [(3, 1, 2, 2, 7), "Latribitho", "2.3.13"],
    # (18,5,0,0,0,-7) = 26c vanishes (3u^7T)
    [(7, 5, 1, 2, 4), "Septhu", "2.3.13"],
    # (-37,0,0,0,0,10) vanishes (L3o^10T)
    [(1, 0, 10, 0, 37), "Laquinbitho", "2.3.13"],
    # 18/17 = 99c vanishes (17uT)
    [(1, 2, 1, 0, 1), "Su", "2.3.17"],
    # (-2,9,0,0,0,0,-3) = 3c vanishes (L17u^3T) (P8/3 = 34/27)
    [(1, 3, 3, 0, -2), "Latrisu", "2.3.17"],
    # (-17,3,0,0,0,0,3) = 21c vanishes (L17o^3T) (P8/3 = 64/51)
    [(1, -1, 3, 0, 17), "Latriso", "2.3.17"],
    # 19/18 = 94c vanishes (19oT)
    [(1, 2, 1, 0, 1), "Ino", "2.3.19"],
    # 729/722 = (-1,6,0,0,0,0,0,-2) = 17c vanishes (L19uuT) (P8/2 = 27/19)
    [(1, 3, 2, 0, -1), "Lanunu", "2.3.19"],
    # (8,3,0,0,0,0,0,-3) = 13c vanishes (19u^3T) (P8/3 = 24/19)
    [(1, 1, 3, 0, 8), "Trinu", "2.3.19"],
    # (17,0,0,0,0,0,0,-4) = 10c vanishes (s19u^4T) (P8/4 = 19/16)
    [(1, 0, 4, 0, 17), "Saquadnu", "2.3.19"],

    # Kite Giedraitis 2020-03-21
    # (31,-21,1) vanishes (s^3yT)
    [(1, 21, 1, 0, -31), "Trisayo", "2.3.5"],

    # Flora Canou 2020
    [(13, 14, 35, 23, -16, 2, 10, 12, 24, 19, -1), "Semiparakleismic", ()],
    [(13, 14, 35, 23, 24, 2, 10, 12, 24, 19, 20), "Gentsemiparakleismic", ()],
    [(15, -2, 54, 2, 8, 4, 23), "Semiluna", ()],
    [(7, 38, -4, 20, 2, 6, 20, 4, 15), "Semiseptiquarter", ()],
    
    # Dawson "Aura" Berry 2021
    [(0, 0, -1, 53, 84, 123, 0), "Schismerc", ()], # 7-limit 53 & 159
    [(0, 0, 1, -1, 53, 84, 123, 0, 332), "Cartography", ()],  # 11-limit 53 & 159
    [(0, 0, 1, -1, 0, 53, 84, 123, 0, 332, 196), "Cartography", ()], # 13-limit 53 & 159
    [(0, 0, 1, 1, 106, 168, 246, 0, 69), "Boiler", ()],  # 11-limit 106 & 212
    [(0, 0, 0, 1, 53, 84, 123, 149, 0), "Joliet", ()],  # 11-limit 53 & 106
    [(0, 0, 0, 1, 0, 53, 84, 123, 149, 0, 196), "Joliet", ()],  # 13-limit 53 & 106
    
    # "Xenllium" 2020
    [(0, 0, 1, -2, 53, 84, 123, 0, 481), "Pentacontatritonic", ()], #11-limit 53 & 265
    [(0, 0, 1, -2, 1, 53, 84, 123, 0, 481, 345), "Pentacontatritonic", ()], #13-limit 53 & 265
    
]:
    mapping = regutils.mappingFromInvariant(key, 2)
    if limit:
        labels = tuple(regutils.textToLimit(limit)[0])
    else:
        labels = regutils.primeNumbers[:len(mapping[0])]
    addName(lookups, name, mapping, 'make_names', labels)

for key, name, limit in [
    # Margo Schulter, tuning list, 2012-10-23
    [(1, 1, 1, 1, 0, -4, -7, 1, 0, 0, 7, 12), "Parapyth", "2.3.7.11.13"],

    # Kite Giedraitis 2017-02-07
    # 45/44 vanishes (1uyT)
    [(1, 1, 1, 0, 2, 1, 0, 0, -2), "Luyo", "2.3.5.11"],
    # 64/63 vanishes (r+yT)
    [(1, 0, 1, 0, -2, 1, 0, 0, 6), "Ru + Ya", "2.3.5.7"],
    # 49/48 vanishes (zz+yT)
    [(1, 0, 2, 0, 1, 1, 0, 0, 2), "Zozo + Ya", "2.3.5.7"],
    # 729/715 = (0,6,-1,-1,-1) vanishes (3u1ug-cT)
    [(1, -1, 1, 0, -1, 1, 0, 0, 6), "Thulugu Noca", "3.5.11.13"],

    # Kite Giedraitis 2017-02-12
    # 50/49 vanishes (rryyT)
    [(1, 1, 1, 0, 0, 2, 0, 0, 1), "Biruyo", "2.3.5.7"],
    # 3645/3584 = (-9,6,1,-1) vanishes (LryT)
    [(1, 1, 1, 0, 6, 1, 0, 0, -9), "Laruyo", "2.3.5.7"],
    # 8748/8575 = (2,7,-2,-3) vanishes (rrrggT)
    [(3, -2, 1, 2, 1, 1, 0, 1, 0), "Triru-agugu", "2.3.5.7"],
    # 9216/8575 = (10,2,-2,-3) vanishes (rrrgg#2T)
    [(3, -2, 1, 1, 0, 1, 0, 2, 2), "Triru-agugubi", "2.3.5.7"],
    # 2592/2401 = (5,4,0,-4) vanishes (r^4+yT)
    [(1, 0, 1, 0, 1, 4, 0, 0, 5), "Quadru + Ya", "2.3.5.7"],
    # 77/75 vanishes (1ozgg-cT)
    [(1, -1, 1, 0, 2, 1, 0, 0, 1), "Lozogugu Noca", "3.5.7.11"],
    # 26/25 vanishes (3oggT)
    [(1, 2, 1, 0, 0, 1, 0, 0, -1), "Thogugu", "2.3.5.13"],
    # 352/351 vanishes (3u1oT)
    [(1, 1, 1, 0, -3, 1, 0, 0, 5), "Thulo", "2.3.11.13"],
    # 81/80 vanishes
    [(0, 1, 1, 4, 0, 1, 0, -4, 0), "Meantone + Za", "2.3.5.7"],
    # 81/80 vanishes
    [(0, 1, 1, 4, 0, 1, 0, -4, 0), "Meantone + La", "2.3.5.11"],
    # 81/80 vanishes
    [(0, 1, 1, 4, 0, 1, 0, -4, 0), "Meantone + Tha", "2.3.5.13"],
    # 81/80 and 77/75 (1ozggT)
    [(0, 1, -1, 1, 4, 0, 9, 1, 0, -4, 0, -8), "Meantone & Lozogugu", "2.3.5.7.11"],

    # Scott Dakota, 2017-03-25
    # Facebook Xenharmonic Alliance - Mathematical Theory
    [(4, 3, 2, 2, 1, 1, 0, 1, -2, 1, 0, 1, 3, 2, 7), "Eros", "13"],
    [(4, 3, 2, 2, 5,
        1, 1, 0, 1, -2, -1,
        1, 0, 1, 3, 2, 7, 6), "Eros", "17"],
    [(4, 3, 2, 2, 5, 0,
        1, 1, 0, 1, -2, -1, -3,
        1, 0, 1, 3, 2, 7, 6, 9), "Eros", "19"],
    [(4, 3, 2, 2, 5, 0, 1,
        1, 1, 0, 1, -2, -1, -3, 1,
        1, 0, 1, 3, 2, 7, 6, 9, 3), "Eros", "23"],

    # More Aphrodite extensions from the discussion
    [(4, 3, 2, 11, 1, 1, 0, 1, -1, 1, 0, 1, 3, 2, 6), "Aphrodite", "13"],
    [(4, 3, 2, 7, 1, 1, 0, 1, 2, 1, 0, 1, 3, 2, 1), "Inanna", "13"],
    [(4, 3, 2, 1, 1, 1, 0, 1, 3, 1, 0, 1, 3, 2, -1), "Ishtar", "13"],

    # Petr Parizek, 2017-04-10
    # Facebook Xenharmonic Alliance - Mathematical Theory
    [(1, 1, 3, 0, -1, 1, 2, 0, 1), "Homalic", "2.3.5.11"],

    # Kite Giedraitis, 2017-09-07
    # 128/125 vanishes (g^3+zT)
    [(0, 1, 1, 0, 0, 3, 0, 7, 0), "Trigu + Za", "2.3.5.7"],
    # 128/125 vanishes (g^3+1aT)
    [(0, 1, 1, 0, 0, 3, 0, 7, 0), "Trigu + La", "2.3.5.11"],
    # 128/125 vanishes (g^3+3aT)
    [(0, 1, 1, 0, 0, 3, 0, 7, 0), "Trigu + Tha", "2.3.5.13"],
    # 1029/1000 vanishes (z^3g^3T)
    [(1, 1, 3, 0, -1, 1, 0, 0, 1), "Trizogu", "2.3.5.7"],
    # 28/27 vanishes (z+yT)
    [(1, 0, 1, 0, 3, 1, 0, 0, -2), "Zo + Ya", "2.3.5.7"],
    # (-9,11,0,-3) vanishes (Lr^3+yT)
    [(1, 0, 3, 0, 11, 1, 0, 0, -3), "Latriru + Ya", "2.3.5.7"],

    # Petr Parizek 2017-12-29
    [(3, 1, 1, 1, 1, 1, 0, 0, 1), "Petredecu", "2.5.11.13"],

    # Scott Dakota 2018-01-12
    [(9, -14, -16, -17, -10, 1, 3, -3, -3, -4, -2, 1, 0, 5, -4, -5, -4, -1),
            "Galaxy", "17"],
    # 2018-02-16
    [(1, 2, 3, -2, 0, 1, 0, 2, 6, -8, -5, 1, 0, 0, -5, -13, 21, 12),
            "Protannic", ()],

    # Kite Giedraitis 2018-02-19
    # 99/98 vanishes (1orrT)
    [(1, 2, 1, 0, -2, 1, 0, 0, 1), "Loruru", "2.3.7.11"],

    # Scott Dakota
    [(0, 1, 1, 1, 1, 1, 21, 0, -4, -7, 9, 1, 0, -31, 0, 7, 12, -13),
            "Terrapyth", ()],
    [(1, 1, 1, 1, 1, 0, -4, -7, 9, 1, 0, 0, 7, 12, -13),
            "Etypyth", '2.3.7.11.13.17'],

    # Kite Giedraitis 2018-03-23
    # (-15,8,1,0) vanishes (Ly+zT)
    [(0, 1, 1, -8, 0, 1, 0, 15, 0), "Layo + Za", "2.3.5.7"],
    # 525/512 vanishes (LzyyT)
    [(1, -2, 1, 0, -1, 1, 0, 0, 9), "Lazoyoyo", "2.3.5.7"],
    # (7,5,-4,-2) vanishes (rrg^4T)
    [(1, -2, 2, 0, 5, 1, 1, 0, 6), "Birugugu", "2.3.5.7"],
    # 36/35 vanishes (rgT)
    [(1, -1, 1, 0, 2, 1, 0, 0, 2), "Rugu", "2.3.5.7"],
    # (6,-7,1,1) vanishes (szyT)
    [(1, -1, 1, 0, 7, 1, 0, 0, -6), "Sazoyo", "2.3.5.7"],
    # (-15,3,2,2) vanishes (LzzyyT)
    [(1, -1, 2, 0, -3, 1, 1, 0, 6), "Labizoyo", "2.3.5.7"],
    # (-13,10,0,-1) vanishes (Lr+yT)
    [(1, 0, 1, 0, 10, 1, 0, 0, -13), "Laru + Ya", "2.3.5.7"],
    # 15/14 vanishes (ryT)
    [(1, 1, 1, 0, 1, 1, 0, 0, -1), "Ruyo", "2.3.5.7"],
    # (6,-5,-4,4) vanishes (sz^4g^4T)
    [(1, 1, 4, 0, 5, 1, 2, 0, 1), "Saquadzogu", "2.3.5.7"],
    # (-4,1,-5,5) vanishes (z^5g^5T)
    [(1, 1, 5, 0, -1, 1, 4, 0, 0), "Quinzogu", "2.3.5.7"],
    # 200/189 = (3,-3,2,-1) vanishes (ryy#2T)
    [(1, 2, 1, 0, -3, 1, 0, 0, 3), "Ruyoyobi", "2.3.5.7"],
    # 256/245 vanishes (rrgT)
    [(2, -1, 1, 0, 0, 1, 0, 0, 4), "Rurugu", "2.3.5.7"],
    # (16,-8,1,-2) vanishes (srryT)
    [(2, 1, 1, 0, -4, 1, 0, 0, 8), "Saruruyo", "2.3.5.7"],
    # 392/375 vanishes (zzgggT)
    [(2, 3, 1, 1, 2, 1, 0, 1, 0), "Zozotrigu", "2.3.5.7"],
    # (0,-2,5,-3) vanishes (r^3y^5T)
    [(3, 5, 1, 1, 1, 1, 0, 0, 0), "Triru-aquinyo", "2.3.5.7"],
    # 2430/2401 = (1,5,1,-4) vanishes (r^4yT)
    [(4, 1, 1, 3, 2, 1, 0, 3, 1), "Quadru-ayo", "2.3.5.7"],
    # (-9,3,-3,4) vanishes (Lz^4g^3T)
    [(4, 3, 1, 1, 0, 1, 0, 1, 3), "Laquadzo-atrigu", "2.3.5.7"],
    # (-5,0,7,-4) vanishes (r^4y^7T)
    [(4, 7, 1, 0, 0, 1, 0, 3, 4), "Quadru-asepyo", "2.3.5.7"],
    # 55/54 vanishes (1oyT)
    [(1, -1, 1, 0, 3, 1, 0, 0, 1), "Loyo", "2.3.5.11"],
    # 243/242 vanishes (1uu+yT)
    [(1, 0, 2, 0, 5, 1, 1, 0, 2), "Lulu + Ya", "2.3.5.11"],
    # 100/99 vanishes (1uyyT)
    [(1, 2, 1, 0, -2, 1, 0, 0, 2), "Luyoyo", "2.3.5.11"],
    # 121/120 vanishes (1oogT)
    [(2, 1, 1, 1, 1, 1, 0, 1, 2), "Lologu", "2.3.5.11"],
    # (17,-5,0,-2,-1) vanishes (s1urrT)
    [(1, -2, 1, 0, -5, 1, 0, 0, 17), "Salururu", "2.3.7.11"],
    # 896/891 = (7,-4,0,1,-1) vanishes (s1uzT)
    [(1, 1, 1, 0, -4, 1, 0, 0, 7), "Saluzo", "2.3.7.11"],
    # (2,0,0,3,-3) vanishes (1u^3z^3T)
    [(1, 1, 1, 0, 0, 3, 0, 0, 2), "Triluzo", "2.3.7.11"],

    # Kite Giedraitis 2019-03-04
    # 65/64 vanishes (3oyT)
    [(1, -1, 1, 0, 0, 1, 0, 0, 6), "Thoyo", "2.3.5.13"],
    # 40/39 vanishes (3uyT)
    [(1, 1, 1, 0, -1, 1, 0, 0, 3), "Thuyo", "2.3.5.13"],
    # 144/143 vanishes (3u1uT)
    [(1, -1, 1, 0, 2, 1, 0, 0, 4), "Thulu", "2.3.11.13"],
    # 136/135 vanishes (17ogT)
    [(1, 1, 1, 0, 3, 1, 0, 0, -3), "Sogu", "2.3.5.17"],
    # 256/255 vanishes (17ugT)
    [(1, -1, 1, 0, -1, 1, 0, 0, 8), "Sugu", "2.3.5.17"],
    # 153/152 vanishes (19u17oT)
    [(1, 1, 1, 0, 2, 1, 0, 0, -3), "Nuso", "2.3.17.19"],
    # 324/323 vanishes (19u17uT)
    [(1, -1, 1, 0, 4, 1, 0, 0, 2), "Nusu", "2.3.17.19"],

    # Kite Giedraitis 2020-02-22
    # (5,-1,7,-7) vanishes (r^7y^7T)
    [(1, 1, 7, 0, -1, 1, 5, 0, 0), "Sepruyo", "2.3.5.7"],
    # (3,-7,2,0,1) vanishes (s1oyyT)
    [(1, -2, 1, 0, 7, 1, 0, 0, -3), "Saloyoyo", "2.3.5.11"],
    # (-12,1,3,0,1) vanishes (L1oy^3T)
    [(1, -3, 1, 0, -1, 1, 0, 0, 12), "Lalotriyo", "2.3.5.11"],
    # (14,-3,-1,0,-2) vanishes (s1uugT)
    [(2, -1, 1, 1, -2, 1, 0, 0, 7), "Salulugu", "2.3.5.11"],
    # (1,3,2,0,-3) vanishes (1u^3yyT)
    [(3, 2, 1, 0, 1, 1, 0, 1, 1), "Trilu-ayoyo", "2.3.5.11"],
    # (3,1,4,0,-4) vanishes (1u^4y^4T)
    [(1, 1, 4, 0, 1, 1, 1, 0, 1), "Quadluyo", "2.3.5.11"],
    # (6,1,0,1,-3) vanishes (1u^3zT)
    [(3, 1, 1, 2, 1, 1, 0, 0, 2), "Trilu-azo", "2.3.7.11"],
    # (-3,-3,0,4,-1) vanishes (1uz^4T)
    [(1, 4, 1, 0, -3, 1, 0, 0, -3), "Luquadzo", "2.3.7.11"],
    # (-2,-4,2,0,0,1) = 325/324 vanishes (3oyyT)
    [(1, -2, 1, 0, 4, 1, 0, 0, 2), "Thoyoyo", "2.3.5.13"],
    # 169/168 vanishes (3oorT)
    [(2, 1, 1, 1, 1, 1, 0, 1, 2), "Thothoru", "2.3.7.13"],

    # Subgroup 2.3.5.13, Comma 676/675
    [(1, 1, 2, 0, 3, 1, 0, 0, -1), "Parizekmic", "2.3.5.13"],
]:
    mapping = regutils.mappingFromInvariant(key, 3)
    if limit:
        labels = tuple(regutils.textToLimit(limit)[0])
    else:
        labels = regutils.primeNumbers[:len(mapping[0])]
    addName(lookups, name, mapping, 'make_names', labels)

for ek in 1/12e2, 0.3/12e2, 3/12e2, 10/12e2:
    for r2t in parametric.survey2(regutils.primes[:4], ek=ek, nResults=170):
        key = r2t.invariant()
        oekey = tuple(x*key[3] for x in key[:3])
        if oekey in unidentified and kernel.contorsion(r2t.melody)==1:
            name = unidentified[oekey]
            del unidentified[oekey]
            name = name.replace(r'\"u', u'\xfc')
            name = name.replace('~', ' ')
            key = r2t.invariant()
            if key not in oldKeys:
                byLimit[2,3,5,7][key] = r2lookup[key] = name

for key, name in [
    [(2, 8, -11, 1, 1, 0, 6), "Mohajira"], # overwrites Semififths
    [(6, 5, 3, 13, 1, 0, 1, 2, 0), "Darjeeling"], # Overrides Keemun
]:
    mapping = regutils.mappingFromInvariant(key, 2)
    labels = regutils.primeNumbers[:len(mapping[0])]
    addName(lookups, name, mapping, 'make_names', labels)

def simplifyhtml(html):
    nolinks = re.sub('<a[^>]*>([^<]*)</a>', r'\1', html)
    nocomments = re.sub('<!--[^>]+-->', '', nolinks)
    return nocomments

wikis = codecs.open('proposed+names+for+rank+2+temperaments',encoding='utf-8').read()
wikis = simplifyhtml(wikis)
wikiPattern = r"([^]>*+\n]+) \[&lt;([^]|]+)[]|], &lt;([^]|]+)[]|]&gt;"
for name, map1, map2 in re.findall(wikiPattern, wikis):
    def decodeMapping(s):
        return list(map(int, re.split('[, ]+', s)))
    mapping = decodeMapping(map1), decodeMapping(map2)
    addName(lookups, name, mapping, 'proposed')

optimal = codecs.open('optimal+patent+val',encoding='utf-8')
sectionMatch = '<h1[^>]+>(\d+)-limit rank (\w+)</h1>'
mapMatch = r"^([^:]+): \d+edo \[(&lt;[^]|]+[]|](?:, &lt;[^]|]+[]|])+)]"
braMatch = r"&lt;([^]|]+)[]|]"
etsMatch = r"^(?:\[\[[^|]+\|)?([^]:]+)]?]?: \d+edo (\d+[a-i]*(?:&amp;\d+[a-i]*)+)"
def processSection(headline):
    limit, rank = re.findall(sectionMatch, headline)[0]
    print("%s-limit rank %s" % (limit, rank))
    labels, plimit = regutils.makeLimit([int(limit)])
    # Hopefully the rank's redundant
    # and only consecutive prime limits, for now ...
    for line in optimal:
        line = simplifyhtml(line)
        if re.search(sectionMatch, line):
            processSection(line)
            return
        etres = re.search(etsMatch, line)
        if etres:
            name, ets = etres.groups()
            if '|' in name:
                print("Broken name:", name)
                name = name.split('|')[-1]
            mapping = [PrimishET(odiv, plimit) for odiv in ets.split('&amp;')]
            addName(lookups, name, mapping, 'optimal', labels)
        else:
            mapres = re.search(mapMatch, line)
            if mapres:
                name, maps = mapres.groups()
                mapping = [map(int, res.replace(',',' ').split())
                        for res in re.findall(braMatch, line)]
                addName(lookups, name, mapping, 'optimal', labels)

def PrimishET(name, limit):
    """Nearest-prime mapping if no warts, explicitly specified otherwise"""
    try:
        return list(et.PrimeET(int(name), limit))
    except ValueError:
        return list(et.NamedET(name.strip(), limit))

for line in optimal:
    line = simplifyhtml(line)
    if re.search(sectionMatch, line):
        processSection(line)

optimal.close()

smalls = codecs.open('smallED2temperaments', encoding='utf-8').read()
smalls = simplifyhtml(smalls)
codecs.open('simplesmalls', 'w', encoding='utf-8').write(smalls)
smallsMatch = r"""(?s)<h3[^>]*>(?:<strong>)?([^<]*?) Temperament(?:</strong>)?</h3>
.*?
<strong>JI Basis:</strong>\s*([^<]+)\s*<br />
<strong>Commas(?: Tempered Out)?:</strong>\s*([^<]+)\s*<br />"""
for name, limit, ratios in re.findall(smallsMatch, smalls):
    name = name.title()
    plimit = re.findall(r'(\d+)-limit', limit)
    if plimit:
        assert len(plimit) == 1
        labels, primes = regutils.makeLimit([int(plimit[0])])
    else:
        labels, primes = regutils.makeLimit(list(map(int, limit.split('.'))))
    try:
        uvs = [regutils.factorizeRatio(int(n), int(d), labels)
            for n, d in re.findall(regutils.ratioPattern, ratios)]
    except OverflowError:
        print("Can't factorize all ratios in", name)
        continue
    try:
        mapping = kernel.temper_out(uvs)
        if len(mapping) == 2:
            addName(lookups, name, mapping, 'smalls', labels)
        else:
            print(name, "defined as rank", len(mapping))
    except AssertionError:
        print("Failed with", name)

chromatic = codecs.open('chromatic+pairs',encoding='utf-8')
nameMatch = "<h[12][^>]*>([^<]+)</h[12]>"
cmapMatch = r"Map: +\[(&lt;[^]|]+[]|](?:, &lt;[^]|]+[]|])+)]"
groupMatch = r'group: ([0-9/.]+)'
headings = ["music", "definitions", "resources", "connect", "practice",
            "theory", "chromatic pairs"]
for line in chromatic:
    line = simplifyhtml(line)
    if re.search(cmapMatch, line):
        print("spurious map found", line)
    nameres = re.search(nameMatch, line)
    if nameres:
        name = nameres.groups()[0]
        if name.lower() in headings:
            continue
        groupline = next(chromatic).lower()
        for line in chromatic:
            line = simplifyhtml(line)
            assert re.search(nameMatch, line) is None, ("No map for " + name)
            mapres = re.search(cmapMatch, line)
            if mapres:
                limit = re.findall(groupMatch, groupline)[0]
                labels = regutils.textToLimit(limit)[0]
                maps = mapres.groups()[0]
                mapping = [map(int, res.replace(',',' ').split())
                        for res in re.findall(braMatch, line)]
                try:
                    addName(lookups, name, mapping, 'chromatic', labels)
                except AssertionError:
                    print("Failed with", name)
                break

assert not unidentified, str(list(unidentified.values()))

# add or replace JIs
for limit in byLimit:
    key = []
    for i in range(len(limit)):
        key += [1] + [0]*i
    limitString = "%s-limit JI" % regutils.formatPrimeLabels(limit, '.')
    byLimit[limit][tuple(key)] = limitString
byLimit[2,3] = {(1,1,0): "Pythagorean"}

# Now for a name without a unique equivalence-equivalent mapping
byLimit[3,5,7][1, 6, 1, 0, -7] = u'Arcturus'

output = open('names.py', "w")
output.write('# encoding: utf-8\n')
output.write(
    "namesByRank = {2:%r,\n3:%r,\n4:%r}\n" %
        (r2lookup, r3lookup, r4lookup))

output.write("namesByLimit = {\n")
for limit in byLimit:
    output.write("    %r:{\n" % (limit,))
    for key, name in byLimit[limit].items():
        output.write("        %r:%r,\n" % (key, name))
    output.write("    },\n")
output.write("}\n")
output.close()
