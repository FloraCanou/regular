# Equal temperament finding library
# used by the regular temperament libraries

###############################################################################
#
#  definitions
#
###############################################################################

from math import log, sqrt
import operator
from regutils import (formatMapping, primes, primeNumbers, prime_mapping,
        consecutiveWarts)

# redundant here, but check for includes
def log2(f):
  return log(f)/log(2)

# Friendly aliases for the prime limits.
# In general, the first entry in a list of primes is taken to be
# the equivalence interval.
# Here, that's always 2:1.
limit5 = primes[:3]
limit7 = primes[:4]
limit11 = primes[:5]
limit13 = primes[:6]
limit17 = primes[:7]
limit19 = primes[:8]
limit23 = primes[:9]
limit29 = primes[:10]
limit31 = primes[:11]

# Sometimes it's useful to limit the number of operations
# to stop bogging down the computer with a useless search.
# This class is a placeholder to allow the machinery for restrictions
# without actually restricting anything.
# It has to be defined at the top because it's a default argument
# to sum functions.
class InfiniteResource:
    def consume(self):
        pass

###############################################################################
#
#  Error Functions
#
###############################################################################

# These functions take a weighted primes sequence as input.
# Usually, Tenney weighting is assumed.

def optimalRMSError(w):
    """The optimum root mean square error of the weighted primes

    independent of the octave stretch
    """
    mean, meanSquare = means(w)
    return sqrt(1.0 - (mean**2 / meanSquare))

def rmsError(w):
    """The actual RMS of the weighted primes,
    specific to the octave stretching
    """
    total = 0.0
    for x in w:
        total += (x-1)**2
    return sqrt(total/len(w))

def stdError(w):
    """The standard deviation of the weighted primes

    approximates the RMS error,
    but not really that much simpler
    """
    mean, meanSquare = means(w)
    return sqrt(meanSquare - mean**2)

def rmsStretch(w):
    """The optimum octave stretch for the RMS of the weighted primes
    """
    mean, meanSquare = means(w)
    return mean/meanSquare

def topError(w):
    """TOP-max error of the weighted primes

    independent of octave stretching"""
    lo, hi = min(w), max(w)
    return (hi-lo)/(hi+lo)

def topStretch(w):
    """TOP-max octave stretch"""
    return 2/(min(w)+max(w))

def errorRange(w):
    """Difference between lowest and highest weighted primes
    (approximates double the TOP error)
    """
    return max(w)-min(w)

def worstError(w):
    """The worst weighted error
    (the thing the TOP error is the optimum of)
    """
    return max([abs(x-1) for x in w])

def means(w):
    """Used by optimalRMSError() and rmsStretch().
    The ends justify the means!
    """
    mean = meanSquare = 0.0
    for stretch in w:
        mean += stretch
        meanSquare += stretch**2
    return mean/len(w), meanSquare/len(w)

###############################################################################
#
#  Equal Temperaments
#
###############################################################################

def getEqualTemperaments(
        primes, nETs=30, lowest=1, cutoff=None, error=rmsError, Emax=0):
    """Get nETs EqualTemperaments

    The primes are supplied as a list of logarithms of frequencies
    The cutoff is the largest allowable error multiplied by the step size
    The error as a function of the weighted primes can be supplied
    Emax is the maximum error in the rank 2 temperament we're
        trying to find.
        All part of a hair-brained scheme to make it all predictable
    """
    # if the cutoff wasn't supplied, guess one that might work
    if cutoff is None:
        cutoff = 0.15/sqrt(len(primes)) + 0.01
    else:
        # avoid integer division
        cutoff = float(cutoff)
    consist = []
    # Use a for loop over an arbitrarily large range
    # instead of an infinite loop
    # so that if the cutoff is too strict the program doesn't hang
    for nNotes in range(lowest, 1000000):
        if not len(consist)<nETs:
            # always return exactly the right number of results
            return consist[:nETs] ###
        if Emax:
            maxError = sqrt(Emax**2 + cutoff**2/nNotes**4)
        else:
            maxError = cutoff/nNotes
        if error in etFinders:
            # this is a kind of error we know about
            # so use an efficient way of producing the ETs
            finder = etFinders[error](nNotes, primes)
            consist += [
                EqualTemperament(mapping, primes)
                for mapping in finder.limitedSupersets(finder.convert(maxError))
                if hcf(mapping)==1]
        else:
            # For an arbitrary error function.
            # Produce a big list of equal temperaments
            # and filter it for those with small enough badness.
            pet = PrimeET(nNotes, primes)
            for et in alternativeMappings(pet):
                if error(et.weightedPrimes()) < maxError and hcf(et.mapping)==1:
                    consist.append(et)
    raise TemperamentException("not enough ETs found")

class PrimeWeighted:
    """Abstract class that does calculations for weighted errors.

    Requires a tuningMap() method and a primes attribute
    """
    def weightedPrimes(self):
        """Return the weighted prime mapping
        Used as the input to error functions
        """
        return [t/p for t, p in zip(self.tuningMap(), self.primes)]
    def errors(self):
        """Return the unweighted errors"""
        return [t-p for t, p in zip(self.tuningMap(), self.primes)]
    def weightedErrors(self):
        return [w-1.0 for w in self.weightedPrimes()]
    def rmsError(self):
        return rmsError(self.weightedPrimes())
    def worstError(self):
        return worstError(self.weightedPrimes())

class EqualTemperament(PrimeWeighted):
    def __init__(self, mapping, primes):
        self.mapping = mapping
        self.primes = primes
    def tuningMap(self):
        for i in range(len(self.primes)):
            if self.mapping[i]:
                stepSize = self.primes[i]/self.mapping[i]
                break
        else:
            raise ZeroDivisionError("Null mapping")
        return [steps*stepSize for steps in self.mapping]

    # convenience methods
    def optimalRMSError(self):
        return optimalRMSError(self.weightedPrimes())
    def topError(self):
        return topError(self.weightedPrimes())

    def name(self, warts = consecutiveWarts):
        """Standard name, based on Herman Miller's suggestion.

        The basic name is the number of steps to the octave.
        If this is unambiguous, that's all you get.
        If it is ambiguous, but has the best approximation to each prime
        (it is the patent val) a 'p' is appended (not in Herman's suggestion).
        Otherwise, a letter is appended for each prime that differs from
        its best approximation.
        The letters are supplied as the 'warts' argument, and default to
        the beginning of the alphabet (correct for consecutive prime limits).
        """
        for i, p in enumerate(self.primes):
            if 0.999 < p < 1.001:
                # Call it an octave
                nNotes = self.mapping[i]
                break
        else:
            # Guessing the best period is important
            period = self.primes[0] * rmsStretch(self.weightedPrimes())
            nNotes = int(round(self.mapping[0] / period)) # steps per octave
        reference = PrimeET(nNotes, self.primes).mapping
        name = str(nNotes)
        if not self.ambiguous():
            return name
        suffix = ""
        for map1, refmap, prime, letter in zip(
                self.mapping, reference, self.primes, warts):
            delta = abs(map1-refmap)
            sign = 1 if refmap < prime*nNotes else -1
            if map1 == refmap - sign * delta:
                freq = 2*delta
            else:
                freq = 2*delta - 1 # c.f. NamedET
            suffix += letter * freq
        return name + (suffix or "p")

    def periodicName(self, warts = consecutiveWarts):
        """
        Name relative to the period (first element) size
        with a wart to clarify this.
        """
        if self.primes[0] == 1.0:
            # Already periodic
            return self.name(warts)
        pe_primes = [p/self.primes[0] for p in self.primes]
        pe_et = EqualTemperament(self.mapping, pe_primes)
        return warts[0] + pe_et.name(warts)

    def ambiguous(self, tolerance=1.2):
        """Decide if the mapping is ambiguous.

        That could be because another equal temperament
        with the same number of steps to the octave:

        1) Has a lower TOP-RMS error
        2) Has a lower TOP error
        3) Has the best approximations to prime intervals (patent val)
        4) Has a TOP-RMS error within "tolerance" of the TOP-RMS optimum
        """
        n, plimit = self.mapping[0], self.primes
        mapping = list(self.mapping)
        if PrimeET(n, plimit).mapping != mapping:
            return True
        resource = LimitedResource(1000)
        try:
            if BestET(n, plimit, optimalRMSError, resource).mapping != mapping:
                return True
            if BestET(n, plimit, topError, resource).mapping != mapping:
                return True
            finder = CumulativeWeightedSumSquares(n, plimit)
            cap = finder.convert(self.optimalRMSError()*tolerance)
            assert len(finder.limitedSupersets(cap, resource)) > 0
            return len(finder.limitedSupersets(cap, resource)) > 1
        except ResourceExhausted:
            # If it's that difficult to find, there must be ambiguity
            return True

    # make these objects look like lists
    def __getitem__(self, index):
        return self.mapping[index]
    def __len__(self):
        return len(self.mapping)
    def __repr__(self):
        # stringify an ET as a ket vector
        return formatMapping(self.mapping)

def BestET(nNotes, primes, error=optimalRMSError, res=InfiniteResource()):
    """Return the best equal temperament with nNotes notes
    in the given prime limit and according to the given error function.
    Note 1: nNotes is always per octave (1.0) not per equivalence interval.
    Note 2: nNotes need note be an integer.
    """
    seed = PrimeET(nNotes, primes)
    candidates = [
        (error(et.weightedPrimes()), et)
        for et in [seed] + betterMappings(seed, error, res)]
    candidates.sort(key=operator.itemgetter(0))
    return candidates[0][1]

def PrimeET(nNotes, primes):
    """Return the EqualTemperament with the right number of notes to the
    octave (1.0) and the nearest approximation to each prime interval.
    """
    return EqualTemperament(prime_mapping(nNotes, primes), primes)

def NamedET(name, primes, warts = consecutiveWarts):
    """
    Reverse my version of Herman's name (no suffix means nearest-primes).
    """
    if name[0] in warts:
        divider = primes[warts.index(name[0])]
        pe_primes = [p / divider for p in primes]
        mapping = NamedET(name[1:], pe_primes, warts).mapping
        return EqualTemperament(mapping, primes)

    n = int(''.join([s for s in name if s.isdigit()]))
    suffix = [s for s in name if s in warts]
    assert name in (str(n) + ''.join(suffix), str(n) + "p"
            ), name + " doesn't match"
    result = PrimeET(n, primes)
    for i in range(len(primes)):
        freq = suffix.count(warts[i])
        if result.mapping[i] < primes[i]*n:
            sign = 1
        else:
            sign = -1
        if freq % 2:
            # odd, better direction
            result.mapping[i] += sign * ((freq+1)//2)
        else:
            result.mapping[i] -= sign * (freq//2)
    return result

def NamedETs(name, primes,
             warts=consecutiveWarts,
             tolerance=1.2,
             maxResults=100):
    """Reverse my version of Herman's name.
    Returns many options for ambiguously specified ambiguous cases.
    Also does stretched octaves specified as decimal fractions.
    """
    if '.' in name:
        return [PrimeET(float(name), primes)]
    try:
        n = int(name)
    except ValueError:
        # Warts make it unambiguous
        return [NamedET(name, primes, warts)]
    prime = PrimeET(n, primes)
    if prime.ambiguous():
        try:
            finder = CumulativeWeightedSumSquares(n, primes)
            cap = finder.convert(prime.optimalRMSError()*tolerance)
            resource = LimitedResource(1000)
            mappings = set(map(tuple, finder.limitedSupersets(cap, resource)))
            if mappings:
                ets = [EqualTemperament(list(mapping), primes)
                        for mapping in mappings]
                def etGoodness(etobj):
                    return etobj.optimalRMSError()
                ets.sort(key=etGoodness)
                return ets[:maxResults]
            return [prime]
        except ResourceExhausted:
            return NamedETs(name, primes, warts, tolerance*0.9, maxResults)
    else:
        return [prime]


# Subclasses of ETFinder are used to find equal temperaments
# with badness below a given limit.
# Each subclass uses a different error measure for the badness.

# The objects recursively create new instances of themselves
# for each new prime interval they look at.
# This all gets quite complicated, but doing efficient searches
# for different error measures without duplicating code is
# a fundamentally tricky problem.

class ETFinder:
    """Base class for a limited Equal Temperament finder"""

    def __init__(self, newMap, primes, mapping=[], data=None):
        """newMap: the mapping of the next prime
        primes: the prime intervals
        mapping: the val for the ET
        data: specific to the implementation
        """
        self.mapping = mapping+[newMap]
        self.primes = primes
        self.stepSize = primes[0]/self.mapping[0]
        self.data = self.setData(data, newMap/self.primes[len(mapping)]*self.stepSize)

    def addEntry(self, newMap):
        """Return a new object updated with the mapping of a new prime"""
        # has to be a new object because the algorithm branches
        # self.data is specific to the subclass
        return self.__class__(newMap, self.primes, self.mapping, self.data)

    def convert(self, value):
        """Convert the thing we're minimizing to the thing we prefer to work with"""
        # do a null conversion as the simplest case
        # subclasses can override it
        return value

    def limitedSupersets(self, limit, res=InfiniteResource()):
        """Return all mappings that start like this one,
        with the error less than the limit.

        Supply a finite resource to stop the search getting
        bogged down with endless recursions.
        That can happen if the limit is set too high.
        """
        res.consume()

        if self.value() > limit:
            return [] ###

        if len(self.mapping)==len(self.primes):
            return [self.mapping] ###

        # First check the nearest mapping of the prime.
        bestGuess =  int(round(self.primes[len(self.mapping)]/self.stepSize))
        results = self.addEntry(bestGuess).limitedSupersets(limit, res)

        # Then try the mappings above and below the nearest one.
        # Carry on in each direction until the limit's broken.
        # This is guaranteed to find all mappings within the limit
        # for the TOP-max error
        # but may not be complete for other errors.

        guess = bestGuess
        while 1:
            guess += 1
            moreResults = self.addEntry(guess).limitedSupersets(limit, res)
            if moreResults==[]:
                break
            results += moreResults

        guess = bestGuess
        while 1:
            guess -= 1
            moreResults = self.addEntry(guess).limitedSupersets(limit, res)
            if moreResults==[]:
                break
            results += moreResults

        return results

class CumulativeWeightedSumSquares(ETFinder):
    """Calculate the optimal sum of weighted primes for an equal temperament
    on a prime-by-prime basis
    """
    def setData(self, data, newW):
        # Remembers the sum of weighted primes,
        # and the sum squared
        if data is None:
            return newW, newW**2 ###
        tot, tot2 = data
        return tot + newW, tot2 + newW**2

    def value(self):
        """Return the current value of the weighted sum squares"""
        # not the sum of squares of weighted primes
        # but the optimal sum of square errors of the equal temperament
        tot, tot2 = self.data
        return len(self.mapping) - tot**2/tot2

    def convert(self, rms):
        """Turn a root mean squared into a sum of squares"""
        return len(self.primes)*rms**2

class CumulativeVariance(CumulativeWeightedSumSquares):
    """Optimize for the variance of the error
    instead of the optimal sum of square errors.
    This leads to a standard deviation limit, instead
    of an optimal RMS one.
    """
    def value(self):
        tot, tot2 = self.data
        return tot2 - tot**2/len(self.mapping)

class CumulativeTOP(ETFinder):
    """Calculate the TOP-max error of an equal temperament
    on a prime-by-prime basis
    """
    def setData(self, data, newW):
        """Remember the highest and lowest weighted primes"""
        if data is None:
            return newW, newW ###
        lo, hi = data
        return min(lo, newW), max(hi, newW)

    def value(self):
        """Return the current TOP value"""
        lo, hi = self.data
        return (hi-lo)/(hi+lo)

class CumulativeErrorRange(CumulativeTOP):
    def value(self):
        lo, hi = self.data
        return hi-lo


# dictionary to look up which alternative ET mapping finder to use
# for which error (assuming the stretch can be optimized)
etFinders = {
    optimalRMSError: CumulativeWeightedSumSquares,
    rmsError: CumulativeWeightedSumSquares,
    stdError: CumulativeVariance,
    topError: CumulativeTOP,
    worstError: CumulativeTOP,
    errorRange: CumulativeErrorRange,
}


def seedETs(primes, complexity, maxError, minBadness=0.1,
        resource=InfiniteResource()):
    """
    Return a list of equal temperaments that is guaranteed to include
    at least one special case of each rank 2 temperament
    with error and complexity not exceeding the specified values.

    minBadness is roughly the error threshold for 1-equal.
    Complexity is the standard deviation, roughly half the max-Kees.
    Error is the Tenney weighted prime RMS.
    The resource is so you can cut off pointless searches.
    """
    maxNotes = int(complexity/minBadness)
    effectiveMinBadness = complexity/float(maxNotes+1)
    results = []
    for nNotes in range(1, maxNotes+1):
        maxETError = sqrt((effectiveMinBadness/nNotes)**2 + maxError**2)
        finder = CumulativeWeightedSumSquares(nNotes, primes)
        for mapping in finder.limitedSupersets(finder.convert(maxETError),
                resource):
            if hcf(mapping)==1:
                results.append(EqualTemperament(mapping, primes))
    return results

def betterMappings(et, error=optimalRMSError, resource=InfiniteResource()):
    """Return all equal temperaments with lower error than the example
    and the same number of notes to the octave.
    May or may not include the original example in the results.
    """
    resource = LimitedResource(1000)
    if error in etFinders:
        # this is a kind of error we know about
        # so use an efficient way of producing the ETs
        finder = etFinders[error](et.mapping[0], et.primes)
        maxError = finder.convert(error(et.weightedPrimes()))
        return [
            EqualTemperament(mapping, primes)
            for mapping in finder.limitedSupersets(maxError, resource)]
    return alternativeMappings(et, resource)

def alternativeMappings(et, resource=InfiniteResource(), index=1):
    """get all equal temperaments that might be consistent"""

    resource.consume()

    if index == len(et.mapping):
        return [et] ###
    result = alternativeMappings(et, resource, index+1)

    target = et.primes[index]*et.mapping[0]
    approximation = float(et.mapping[index])
    error = approximation-target
    mapping = et.mapping[:]
    # round each prime up and down
    if error < 0:
        mapping[index] += 1
    else:
        mapping[index] -= 1
    result += alternativeMappings(
        EqualTemperament(mapping, et.primes), resource, index+1)

    return result

###############################################################################
#
#  Utilities
#
###############################################################################

class TemperamentException(Exception):
    pass

class DegenerateException(TemperamentException):
    pass

class ResourceExhausted(TemperamentException):
    pass

def hcf(numbers):
    """Highest common factor by Euclid"""
    m = 0
    for n in numbers:
        if m==1 or m==-1:
            return 1
        while n:
            m, n = n, m%n
    return abs(m)

class LimitedResource:
    """Only allows itself to be consumed a certain number of times"""
    def __init__(self, calls):
        self.timeToLive = calls
    def consume(self):
        self.timeToLive -= 1
        if (self.timeToLive < 0):
            raise ResourceExhausted("Set number of calls exceeded")

def nint(x):
    """Nearest integer to a float, as an int"""
    return int(round(x))


if __name__ == '__main__':
    assert BestET(12, primes[:6]).name() == '12f'
    for name in '12f', '31', '22p', '19p':
        assert NamedET(name, primes[:6]).name() == name
        assert NamedET(name, primes[:6]).periodicName() == name
    assert BestET(12, primes[:6]).periodicName() == '12f'

    # This is how it optimizes without octaves
    warts = 'bcdef'
    plimit = primes[1:6]
    assert BestET(12, plimit).name(warts) == '12p'
    b19e = BestET(12, plimit)
    assert BestET(12, plimit).periodicName(warts) == 'b19e'
    assert NamedET('b19p', plimit, warts).periodicName(warts) == 'b19p'
    assert NamedET('b19e', plimit, warts).periodicName(warts) == 'b19e'
