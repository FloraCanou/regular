"""Various utilities for temperament searches"""

from __future__ import division

import math, operator

#
# Defining the prime intervals
#

# Intervals are measured in octaves.
# In general, the first entry in a list of primes is taken to be
# the equivalence interval.
# Tenney weighting is implied.
def log2(f):
    return math.log(f)/math.log(2)
primeNumbers = 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53
primes = [log2(f) for f in primeNumbers]

def makeLimit(limit):
    """Returns the labels and primes for the limit given as a list.
    If it's a list of one number, that specifies the prime limit.
    If more, they specify the prime intervals.
    An integer means a harmonic.
    A pair of integers means a rational partial.
    A float means a partial specified in cents.
    The labels are strings when they aren't integers.
    All kinds of exceptions may be thrown for invalid input.
    """
    labels, plimit = [], []
    for partial in limit:
        if isinstance(partial, int):
            if partial == 1:
                raise TypeError(
                        "Partial should not be 1. "
                        "You can probably omit this as 1/1 is implied "
                        "(or set it to 2 for an octave-based timbre.")
            elif partial < 1:
                raise TypeError("Partials should be positive integers.")
            labels.append(partial)
            plimit.append(log2(partial))
        elif isinstance(partial, float):
            labels.append('%.3f'%partial)
            plimit.append(partial/12e2)
        else:
            if len(partial)==1:
                # assume this should be a harmonic
                partial = partial[0], 1
            n, d = partial
            if d==1:
                labels.append(n)
                plimit.append(log2(n))
            else:
                labels.append('%i/%i' % (n, d))
                plimit.append(log2(n) - log2(d))
    if len(labels)==1:
        try:
            highest, = labels[0]
        except TypeError:
            highest, = labels
        for i, prime in enumerate(primeNumbers):
            if prime > highest:
                return primeNumbers[:i], primes[:i]
        return primeNumbers, primes
    return labels, plimit

def arePrimeNumbers(numbers):
    return tuple(numbers) == primeNumbers[:len(numbers)]

def formatPrimeLabels(labels, separator='.'):
    if arePrimeNumbers(labels):
        return str(labels[-1])
    if separator == '.':
        for label in labels:
            if isinstance(label, str) and '.' in label:
                separator = ', '
                break
    return separator.join(map(str, labels))

consecutiveWarts = "abcdefghijklmno"
standardWarts = dict(zip(primeNumbers, consecutiveWarts))
otherWarts = "qrstuvwxyzabcdefghijklmnop"
def wartsFromPrimeLabels(labels):
    """Return some letters to be used in suffixes for a
    variant of Herman's ET naming scheme.
    Letters from the start of the alphabet are used for primes.
    Letters starting from q are used otherwise.
    """
    wartStore = iter(otherWarts)
    result = []
    for label in labels:
        if label in standardWarts:
            result.append(standardWarts[label])
        else:
            result.append(next(wartStore))
    return ''.join(result)

def textToLimit(text):
    partials = []
    if '_' in text:
        # written by CGI
        for partial in text.split('_'):
            if '.' in partial:
                partials.append(float(partial))
            else:
                partials.append(list(map(int, partial.split('/'))))
    elif ' ' in text:
        # a list of values in cents
        for partial in text.split():
            if partial.endswith(','):
                partial = partial[:-1]
            partials.append(float(partial))
    elif ':' in text:
        # partials specified as a chord
        freqs = text.split(':')
        root = int(freqs[0])
        for partial in freqs[1:]:
            if '.' in partial:
                # convert to cents
                partials.append(log2(float(partial)/root)*1200)
            elif '/' in partial:
                n, d = map(int, partial.split('/'))
                d *= root
                if n < d:
                    d, n = n, d
                partials.append((n, d)) # don't bother to reduce
            else:
                n, d = int(partial), root
                if n < d:
                    d, n = n, d
                partials.append((n, d)) # don't bother to reduce
    else:
        # . separated harmonics or ratios
        for partial in text.split('.'):
            if '/' in partial:
                n, d = map(int, partial.split('/'))
                if n < d:
                    d, n = n, d
                partials.append((n, d)) # don't bother to reduce
            else:
                partials.append(int(partial))
    return makeLimit(partials)

#
# The TemperamentList class
#

class TemperamentList:
    """Used for the containers temperaments are stored in during searches.
    Stores up to the given number of Rank2Temperaments ordered by badness.
    This is optimized for efficiency of large searches.
    It can reject temperaments that are too bad to make the list.
    It can also reject temperaments that are no better than equivalent
    ones it's seen before.
    """
    def __init__(self, capacity):
        self.capacity = capacity
        self.data = {} # list of the things we've seen before
        self.queue = [] # the best ETs sorted by badness

    def goodEnough(self, badness):
        """Say of a temperament with this badness would make the list"""
        return not self.full() or badness < self.badnessCap()

    def full(self):
        return len(self.queue) == self.capacity

    def badnessCap(self):
        return self.queue[-1][0]

    def add(self, temperament, badness):
        """Add a temperament to the list, if it's good and novel enough.
        Trust the caller to supply the correct badness to save
        re-calculating it.
        """
        key = temperament.invariant()
        newItem = badness, temperament
        if key in self.data:
            oldItem = self.data[key]
            if newItem[0] >= oldItem[0]:
                return ###
            try:
                self.queue.remove(oldItem)
            except ValueError:
                pass
        self.data[key] = newItem
        self.queue.append(newItem)
        self.queue.sort()
        del self.queue[self.capacity:]

    def retrieve(self):
        """Return all the stored temperaments"""
        return [lt for badness, lt in self.queue]

class ConservativeList(TemperamentList):
    """Always keeps the oldest"""
    def add(self, temperament, badness):
        """Add a temperament to the list, if it's good and novel enough.
        Trust the caller to supply the correct badness to save
        re-calculating it.
        """
        key = temperament.invariant()
        if key not in self.data:
            newItem = badness, temperament
            self.data[key] = newItem
            self.queue.append(newItem)
            self.queue.sort(key=operator.itemgetter(0))
            del self.queue[self.capacity:]

#
# Statistical functions
#

def mean(x):
    """The arithmetic mean of a sequence"""
    return sum(x)/len(x)

def rms(x):
    """The root mean squared of a sequence"""
    return math.sqrt(mean([y*y for y in x]))

def lae(x, y):
    """x is less then or approximately equal to y"""
    return x <= y or 0 < x/y < 1.001

#
# invariants as reduced lattices
#

def nameThatMapping(mapping, labels=None):
    """Returns a pair of the name of a temperament class
    and the number of primes you need to remove from the mapping
    to get it.
    Raises a KeyError if no name can be found.
    """
    import names
    r = len(mapping)
    dimension = len(mapping[0])
    if __debug__:
        for row in mapping:
            assert len(row) == dimension
    if labels is not None:
        assert len(labels) == dimension
        limit = tuple(labels)
        if limit in names.namesByLimit:
            key = lattice_invariant(mapping)
            if key in names.namesByLimit[limit]:
                # An exact non-canonical name takes priority
                return names.namesByLimit[limit][key], 0
        # check the canonical order next
        newMapping, newLimit = canonical_order(mapping, limit)
        if newLimit != limit:
            return nameThatMapping(newMapping, newLimit)
    for ignoredDimensions in range(dimension-2) or [0]:
        if ignoredDimensions == 0:
            submapping = mapping
        else:
            submapping = [row[:-ignoredDimensions] for row in mapping]
        if rank(submapping) < r:
            r = rank(submapping)
        invariant = lattice_invariant(submapping, r)
        if labels is None:
            lookup = names.namesByRank.get(r, {})
        else:
            lookup = names.namesByLimit.get(tuple(labels[:-ignoredDimensions]), {})
        if invariant in lookup:
            if not (ignoredDimensions and r==(dimension-ignoredDimensions)): # JI
                return lookup[invariant], ignoredDimensions
    if labels is None:
        raise KeyError("no name found")
    name, ignored = nameThatMapping(mapping)
    return name+'?', ignored

def canonical_order(mapping, labels):
    def str_int_sorter(x):
        return isinstance(x[0], str), x[0]
    decorated = sorted(zip(labels, *mapping), key=str_int_sorter)
    transpose = list(zip(*decorated))
    return transpose[1:], transpose[0]

def lattice_invariant(mapping, rank=None):
    if rank is None:
        rank = len(mapping)
    reduced = lattice_reduction(mapping)
    for entry in reduced[rank:]:
        assert not any(entry)
    del reduced[rank:]
    for i in range(len(reduced)):
        assert not any(reduced[i][:i])
        reduced[i] = reduced[i][i:]
    reduced.reverse()
    result = []
    for entry in reduced:
        result.extend(entry)
    return tuple(result)

def mappingFromInvariant(invariant, rank):
    length = len(invariant)
    mapping = []
    remainder = tuple(invariant)
    padding = rank - 1
    total_padding = (padding*(padding+1))>>1
    n_dimensions, check = divmod(len(invariant) + total_padding, rank)
    if check:
        raise IndexError("Invariant of wrong length for rank")
    for r in range(rank):
        mapping.append((0,)*padding + remainder[:(n_dimensions-padding)])
        remainder = remainder[(n_dimensions-padding):]
        padding -= 1
    mapping.reverse()
    return mapping

def reduced_mapping(ets):
    """Reduced echelon form with a special case for rank 2"""
    if len(ets) != 2:
        return lattice_reduction(ets)

    m, n = ets[0][0], ets[1][0]

    mGen, nGen, octave = extendedEuclid(m, n)
    if mGen < 0:
        # forget negative generators
        mGen += m//octave
        nGen += n//octave
    if 2*mGen > m//octave:
        # and put the generator between a unison and a period
        # at least as far as we can tell from that one
        m, n = n, m
        melody = list(zip(ets[1], ets[0]))
        mGen, nGen = m//octave - nGen, n//octave - mGen
    else:
        melody = list(zip(ets[0], ets[1]))

    assert m*nGen-n*mGen == octave

    genConv1, genConv2 = -n//octave, m//octave
    generatorMapping = [
        (genConv1*axis1 + genConv2*axis2)
        for axis1, axis2 in melody]

    periodMapping = [
        (nGen*axis1 - mGen*axis2)
        for axis1, axis2 in melody]

    return periodMapping, generatorMapping

def lattice_reduction(ets):
    """Reduced echelon form without removing common factors
    and allowing positive numbers to appear in place of zeros
    instead of introducing torsion.
    (These positive numbers should be the smallest possible.)
    """
    echelon = echelon_form(ets)
    n_columns = len(echelon[0])
    column = 1
    for row in range(1, len(echelon)):
        nrow = echelon[row]
        try:
            while nrow[column]==0:
                column += 1
        except IndexError:
            # lower rank
            break
        n = nrow[column]
        for srow in echelon[:row]:
            m = srow[column] // n
            for j in range(n_columns):
                srow[j] -= m*nrow[j]
    return echelon

def rank(mapping):
    result = 0
    for row in echelon_form(mapping):
        for element in row:
            if element:
                break
        else:
            return result
        result += 1
    return result

def echelon_form(ets, col=0):
    working = [normalize_sign(et) for et in map(list, ets)]
    if not working:
        return []
    ncols = len(working[0])
    if col == ncols:
        return working
    assert col < ncols

    reduced = []
    while True:
        reduced += [row for row in working if row[col] == 0]
        working = [row for row in working if row[col] != 0]

        if len(working) < 2:
            return working + echelon_form(reduced, col + 1)

        working.sort()
        pivot = working[0]
        for row in working[1:]:
            n = row[col] // pivot[col]
            for i in range(col, ncols):
                row[i] -= pivot[i] * n

def real_echelon_reduce(matrix):
    """Echelon form of a real (float) matrix in place"""
    size = len(matrix)
    if size < 2:
        return
    width = len(matrix[0])
    for row in matrix[1:]:
        assert width == len(row)
    first_row = col = 0
    for col in range(width):
        # Reduce the ith column from the ith row
        for row in range(first_row, size):
            if matrix[row][col] != 0.0:
                break
        else:
            # column all zeros
            continue
        # swap rows
        matrix[first_row], matrix[row] = matrix[row], matrix[first_row]
        # zero the rest of the column
        for row in range(first_row+1, size):
            mul = float(matrix[row][col])/matrix[first_row][col]
            for sub_col in range(col, width):
                matrix[row][sub_col] -= mul * matrix[first_row][sub_col]
        first_row += 1
        if first_row == size - 1:
            return

def normalize_sign(mapping):
    nonzeros = [m for m in mapping if m]
    if nonzeros==[] or nonzeros[0]>0:
        return mapping
    return [-m for m in mapping]

def size_of_matrix(m):
    """
    The determinant of m multiplied by its transpose.
    But uses mean instead of sum to confuse matters.
    """
    size = len(m)
    n_primes = len(m[0])
    assert size <= n_primes
    ms_rows = []
    for row in m:
        assert len(row) == n_primes
        ms_rows.append(mean([x*x for x in row]))
    if size==1:
        result = ms_rows[0]
    elif size==2:
        cross_term = mean([x*y for x, y in zip(m[0], m[1])])
        result = ms_rows[0]*ms_rows[1] - cross_term**2
    else:
        mtm = [[0]*size for i in range(size)]
        for i in range(size):
            mtm[i][i] = ms_rows[i]
            for j in range(i+1, size):
                mtm[i][j] = mtm[j][i] = mean([x*y for x, y in zip(m[i], m[j])])
        real_echelon_reduce(mtm)
        result = 1.0
        for i in range(size):
            result *= mtm[i][i]
    if result < 0:
        return 0.0
    return math.sqrt(result)

def extendedEuclid(f, g):
    """Return two generator sizes and the octave division
    from the sizes of two equal temperaments
    """
    # Uses the extended Euclidian algorithm.
    # From von zur Gathen & Gerhard, p.47

    assert f>0 and g>0
    lastR, nextR = f, g
    lastS, lastT, nextS, nextT = 1, 0, 0, -1

    while nextR:
        q, r = divmod(lastR, nextR)
        nextR, lastR = r, nextR
        nextS, lastS = lastS - q*nextS, nextS
        nextT, lastT = lastT - q*nextT, nextT

    return lastT, lastS, lastR

#
# Interval vectors and mappings
#

def tempersOut(mapping, vector):
    for element in vector[len(mapping):]:
        if element != 0:
            return False
    return sum([m*v for m, v in zip(mapping, vector)])==0

def prime_mapping(nNotes, primes):
    """
    Mapping with the nearest approximation to each prime
    """
    assert nNotes>0
    return [int(round(prime*nNotes)) for prime in primes]

def formatMapping(mapping):
    return '<' + repr(list(mapping))[1:]

def formatInterval(interval):
    return repr(list(interval))[:-1] + '>'

def formatMappingMatrix(m):
    return '[%s>' % ',\n '.join(map(formatMapping, m))

def inherentError(uv, plimit):
    """Returns the TOP-RMS error for the best case of
    tempering out the given unison vector in the given
    prime limit.
    The number of dimensions does matter.
    """
    # See http://x31eq.com/primerr.pdf p.22 equation 107
    q = [x*p for x, p in zip(fitToPrimeLimit(uv, plimit), plimit)]
    return abs(mean(q))/rms(q)

def fitToPrimeLimit(uv, plimit):
    d = len(plimit)
    assert not any(uv[d:])
    return (list(uv) + [0]*(d - len(uv)))[:d]

def setBestLimit(intervals):
    """Returns the smallest consecutive prime limit
    that works with the given list of intervals
    and also returns those intervals all of the right dimension.
    Assumes harmonic timbres.
    """
    size = max(map(last_non_zero, intervals)) + 1
    result = []
    for interval in intervals:
        if len(interval) >= size:
            result.append(tuple(interval[:size]))
        else:
            result.append(tuple(interval) + (0,)*(size - len(interval)))
    return result, primes[:size]

def last_non_zero(interval):
    result = 0
    for i in range(len(interval)):
        if interval[i] != 0:
            result = i
    return result

#
# Ratios
#

ratioPattern = '(\d+)[/:](\d+)'
vectorPattern = '[[|(]\s*(-?\d+(?:[, ]\s*-?\d+)*)\s*[])>]'

def factorizeRatio(n, d, labels=primeNumbers):
    """return n/d as a ratio space vector"""
    assert n > 0 and d > 0
    for basis in labels, tuple(reversed(labels)):
        for i in range(1,10):
            try:
                result = tuple([ni-di for ni, di in
                    zip(factorize(n*i, basis), factorize(d*i, basis))])
                if basis==labels:
                    return result
                return tuple(reversed(result))
            except OverflowError:
                pass
    raise OverflowError("Inappropriate prime limit")

def factorize(n, labels=primeNumbers):
    if __debug__:
        for num in primeNumbers:
            assert type(num) == int
    assert n>0
    result = []
    residue = n
    for prime in labels:
        for factors in range(1000): # make the range bigger for stupid ratios
            newResidue, remainder = divmod(residue, prime)
            if remainder != 0:
                break
            residue = newResidue
        else:
            raise OverflowError("Number too large to factorize")
        result.append(factors)
    if residue != 1:
        raise OverflowError("Inappropriate prime limit")
    return tuple(result)

def getRatio(interval, primes=primeNumbers):
    """convert the octave-specific ratio space vector
    to a ratio n/d

    return (n,d)

    """
    n = d = 1
    for r, partial in zip(interval, primes):
        if r == 0:
            continue
        if isinstance(partial, int):
            nx, dx = partial, 1
        elif isinstance(partial, str):
            nx, dx = map(int, partial.split("/"))
        else:
            raise ValueError("Bad basis for rationalization")
        if r > 0:
            n *= nx**r
            d *= dx**r
        else:
            n *= dx**(-r)
            d *= nx**(-r)
    assert type(n) == int or type(n) == long, (interval, primes, n)
    assert type(d) == int or type(n) == long, (interval, primes, d)
    return n, d

def textToIntervals(s, labels=primeNumbers):
    """Used by the CGI"""
    import re
    vectors = []
    for n, d in re.findall(ratioPattern, s):
        try:
            vectors.append(factorizeRatio(int(n), int(d), labels))
        except TypeError:
            raise TypeError("You can only factorize ratios with integer harmonics")
    for result in re.findall(vectorPattern, s):
        numbers = tuple(map(int, result.replace(',', ' ').split()))
        vectors.append(numbers + (0,)*(len(labels)-len(numbers)))
    return vectors

def dotprod(u, v):
    assert len(u) == len(v)
    return sum(x*y for (x, y) in zip(u, v))

if __name__=='__main__':

    orwell = [[31, 49, 72, 87, 107], [22, 35, 51, 62, 76]]
    assert nameThatMapping(orwell) == ("Orwell", 0)
    orwell[0][3], orwell[0][4] = orwell[0][4], orwell[0][3]
    orwell[1][3], orwell[1][4] = orwell[1][4], orwell[1][3]
    assert nameThatMapping(orwell, (2, 3, 5, 11, 7)) == ("Orwell", 0)
    del orwell[0][4]
    del orwell[1][4]
    assert nameThatMapping(orwell, (2,3,5,11)) == ("Orson", 1)
    supra = [(5, 8, 14, 17), (17, 27, 48, 59)]
    assert nameThatMapping(supra, (2,3,7,11)) == ("Supra", 0)
