# Tenney-Euclidean regular temperaments

from __future__ import division

import regutils, parametric
import matfunc # http://users.rcn.com/python/download/python.htm

class TuningMixin:
    """
    Methods for a TempramentClass with a tuning
    (a tuple of floats called "basis")
    """

    def unstretch(self):
        stretch = self.tuningMap()[0]/self.plimit[0]
        self.basis = tuple(x/stretch for x in self.basis)

    def tuningMap(self):
        """The size of each tempered prime interval in octaves."""
        assert hasattr(self, "basis"), "Must have a tuning"
        map_tuning = list(zip(self.melody, self.basis))
        return [sum(m[prime]*tuning for m, tuning in map_tuning)
                for prime in range(len(self.plimit))]

    def errors(self):
        """Return the unweighted errors"""
        return [t-p for t, p in zip(self.tuningMap(), self.plimit)]

    def weightedPrimes(self):
        """Return the weighted prime mapping
        Used as the input to error functions
        """
        return [t/p for t, p in zip(self.tuningMap(), self.plimit)]

    def tunedBlock(self, units=12e2):
        octaves = [m[0] for m in self.melody]
        return list(self.tuneScale(fokkerBlock(*octaves), units))

    def tuneScale(self, notes, units=12e2):
        for note in notes:
            pitch = 0.0
            for i, tuning in zip(note, self.basis):
                pitch += i*tuning *units
            yield pitch

    def getscale(self, d, units=12e2):
        """Return a list of cents (by default) values for a generated scale
        with d notes to the octave.
        """
        evenScales = [[maximallyEven(d, self.melody[0][0],)]]
        for et in self.melody[1:]:
            # try to minimize the number of distinct scale steps
            candidates = []
            for rot in range(d):
                scale = maximallyEven(d, et[0], rot)
                for oldScale in evenScales:
                    newScale = oldScale + [scale]
                    candidates += [(nStepSizes(newScale), newScale)]
            candidates.sort()
            nDistinctSteps = candidates[0][0]
            evenScales = []
            for dist, scale in candidates:
                if dist > nDistinctSteps:
                    break
                evenScales.append(scale)

        tunings = self.basis
        result = []
        for degrees in zip(*evenScales[0]):
            result.append(sum(d*t for d, t in zip(degrees, tunings))*units)
        return result

    def __str__(self):
        """The standard way of converting the temperament to a string.
        This is supposed to be readable, but still brief.
        """
        name, extras = self.name()
        if " & " in name:
            # no friendly name
            name = ""
        else:
            name += "+"*extras + "\n\n"
        name = name.replace(u"\xfc", "ue")
        if not hasattr(self, 'basis'):
            self.optimize()
        params = dict(
            name=name,
            melody=regutils.formatMappingMatrix(self.melody),
            reduced=regutils.formatMappingMatrix(
                regutils.reduced_mapping(self.melody)),
            complexity=self.complexity(),
            error=self.error()*1200,
            tuning=formatIntervals(self.basis),
            tuningMap=formatIntervals(self.tuningMap()),
        )
        return regularFormat % params

regularFormat = """%(name)smapping by steps:
%(melody)s

step tunings:
<%(tuning)s] cents

reduced mapping:
%(reduced)s

tuning map:
<%(tuningMap)s] cents

complexity: %(complexity).3f
error: %(error).3f cents/octave"""

class RegularTemperament(parametric.TemperamentClass, TuningMixin):
    def optimize(self):
        if len(self.melody)==1:
            # special case for equal temperaments:
            m = self.weightedMapping()[0]
            self.basis = regutils.mean(m)/regutils.mean([x**2 for x in m]),
        else:
            M = matfunc.Matrix(self.weightedMapping()).tr()
            V = matfunc.Vec([1.0]*M.rows)
            self.basis = tuple(M.solve(V))

    def error(self):
        return regutils.rms((x-1) for x in self.weightedPrimes())

def formatIntervals(intervals):
    return ', '.join(['%.3f'%(x*1200) for x in intervals])

def Temperament(plimit, *octaves):
    if isinstance(plimit, str):
        nums, plimit = regutils.textToLimit(plimit)
    rt = RegularTemperament(plimit, [parametric.bestET(plimit, n)
                                     for n in octaves])
    rt.optimize()
    return rt
temperament = Temperament

def fokkerBlock(*octaves):
    return zip(*[maximallyEven(octaves[0], j, -1) for j in octaves])

def maximallyEven(d, n, rotation=0):
    """A maximally even d from n scale.
    Rotations from 1 to d should cover all possibilities.
    """
    return [(i*n + n + rotation%d)//d for i in range(d)]


if __name__ == '__main__':
    cents = 123.4, 5432.0, 223.4
    plimit = [x/1200 for x in cents]
    random_example = RegularTemperament(plimit, [[2, 108, 4]])
    random_example.optimize()
    example_tuning = random_example.tuningMap()
    tuning_string = ', '.join(map('{:.6f}'.format, example_tuning))
    assert tuning_string == '0.091961, 4.965919, 0.183923'

    marvel = Temperament(regutils.primes[:5], 19, 22, 31)
    marvel.optimize()
    marvel_tuning = marvel.tuningMap()
    tuning_string = ', '.join(map('{:.6f}'.format, marvel_tuning))
    assert tuning_string == '1.000534, 1.584502, 2.320855, 2.808046, 3.459337'
